<h1>Backend con Node.js: Autenticación con Passport.js y JWT 🚀</h1>

<h3>Nicolas Molina</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Cómo autenticar usuarios con Node.js](#cómo-autenticar-usuarios-con-nodejs)
  - [Autenticación vs. autorización](#autenticación-vs-autorización)
  - [Tienda en línea: instalación del proyecto](#tienda-en-línea-instalación-del-proyecto)
- [2. Protección de contraseñas](#2-protección-de-contraseñas)
  - [Middleware de verificación](#middleware-de-verificación)
  - [Hashing de contraseñas con bcryptjs](#hashing-de-contraseñas-con-bcryptjs)
  - [Implementando hashing para usuarios](#implementando-hashing-para-usuarios)
- [3. Passport y JSON Web Tokens](#3-passport-y-json-web-tokens)
  - [Implemetando login con Passport.js](#implemetando-login-con-passportjs)
  - [¿Qué es un JWT?](#qué-es-un-jwt)
  - [Firmar y verificar tokens](#firmar-y-verificar-tokens)
  - [Generar JWT en el servicio](#generar-jwt-en-el-servicio)
  - [Protección de rutas](#protección-de-rutas)
  - [Control de roles](#control-de-roles)
  - [Obteniendo órdenes del perfil](#obteniendo-órdenes-del-perfil)
  - [Manejo de la autenticación desde el cliente](#manejo-de-la-autenticación-desde-el-cliente)
- [4. Envío de emails con Node.js](#4-envío-de-emails-con-nodejs)
  - [Cómo enviar emails con Node.js](#cómo-enviar-emails-con-nodejs)
  - [Implementando el envío de emails](#implementando-el-envío-de-emails)
- [5. Recuperación de contraseñas](#5-recuperación-de-contraseñas)
  - [Generando links de recuperación](#generando-links-de-recuperación)
  - [Validando tokens para cambio de contraseña](#validando-tokens-para-cambio-de-contraseña)
- [6. Despliegue a producción](#6-despliegue-a-producción)
  - [Deploy en Heroku](#deploy-en-heroku)
- [7. Próximos pasos](#7-próximos-pasos)
  - [Cómo seguir aprendiendo backend](#cómo-seguir-aprendiendo-backend)

# 1. Introducción

## Cómo autenticar usuarios con Node.js

Bienvenidos al curso

## Autenticación vs. autorización

![img](https://www.redeszone.net/app/uploads-redeszone.net/2020/06/diferencias-autenticacion-autorizacion-3.jpg)

**Autenticación:** Quien eres (ususario, contraseña)
**Authorizacion:** Gestion de permisos (roles y privilegios).

### **Autenticación**

Este proceso es para determinar si la combinación de usuario y contraseña son las correctas.

- Si es correcta:

  - Nos dan una llave para acceder, (en este curso esa llave será un token generado por la librería **jsonwebtoken** )

- Si

   

  NO

   

  es correcta:

  - El servidor nos prohibe continuar y lo normal es que nos responda con un código http 401 que significa

     

    Unauthorized

     

    (veamoslo con gatos xD)

     

### **Autorización**

Es cuando el servidor ya verificó que la contraseña y usuario son correctas y se le devolvió correctamente al usuario un token (la llave) pero se quiere usar esa llave para entrar a una parte del sitio **prohibido** para ese usuario, esto puede suceder cuando:

- El usuario no es administrador y quiere acceder a una página sólo para admistradores (petición get) .
- El usuario quiere realizar una petición tipo delete/post/put a un recurso pero sólo tiene permisos de lectura ese usuario.

Lo normal es que el servidor responda con status code 403 cuando esto sucede
![403 forbidden](https://http.cat/403)

En lo personal siento que la imagen del 401 debería estar en la del código 403 porque representa mejor que el gato tiene prohibido entrar a la tienda.

Quizá en la imagen de 401 lo que debería ir es un gato sosteniendo una identificación con su nombre y foto entregandola a otro gato para entrar a un lugar donde la puerta tenga un letrero que diga “only local cats”. Jaja no sé, si alguna vez un diseñador ve este mensaje, traten de enviar un diseño con una solicitud a la página por favor xD.

> Autenticación: Quien eres
> Authorizacion: Los permisos que tienes

## Tienda en línea: instalación del proyecto

 **link del proyecto** [curso-nodejs-auth](https://github.com/platzi/curso-nodejs-auth/tree/1-init)

```sh
git clone https://github.com/platzi/curso-nodejs-auth.git
```

Les dejo mi archivo docker-compose.yml que utiliza este metodo y ademas usa variables dntorno (las que usamos en el archivo .env del proyecto)

Adicionalmente estoy utilizando phppgadmin como gestor para postgress pero pueden sustituirlo por pgadmin sin problema

```dockerfile
version: '3.3'

services:
  db:
    container_name: db
    image: 'postgres:${TAG}'
    restart: always
    environment:
      POSTGRES_DB: ${DB_NAME}
      POSTGRES_USER: ${DB_USER}
      POSTGRES_PASSWORD: ${DB_PASSWORD}
    ports:
      - 5432:5432
    volumes:
      - dbdata:/var/lib/postgresql/data
    networks:
      - app-network

  dbmanager:
    image: bitnami/phppgadmin:7
    restart: always
    environment:
      DATABASE_HOST: db
      DATABASE_PORT_NUMBER: ${DB_PORT}
      DATABASE_SSL_MODE: allow
      ALLOW_EMPTY_PASSWORD: yes
    ports:
      - 8081:8080
    depends_on:
      - db
    networks:
      - app-network

#Docker Networks
networks:
  app-network:
    driver: bridge
#Volumes
volumes:
  dbdata:
    driver: local
```

mi archivo .env es este

```env
NODE_ENV="development"
TAG=13
PORT=3000
DB_NAME="my_store"
DB_USER="admin123"
DB_PASSWORD="admin123"
DB_HOST="localhost"
DB_PORT="5432"
```

Sigo usando las variables separadas porque son necesarias para usarlas en el archivo decker-compose.yml

Crear variables de entorno en insomia.

#### Insomnia

**ACTIVATE ENVIRONMENT**

Manage Environments

`prod` `color: red`

```json
{
	"API_URL": "https://api.escuelajs.co"
}
```

`dev` `color: green`

```json
{
	"API_URL": "http://localhost:3000/api/v1/"
}
```

# 2. Protección de contraseñas

## Middleware de verificación

```javascript
if (req.headers['api'] === '123') {
	next();
} else {
	next(boom.unauthorized());
}
```

`auth.handler.js`

```javascript
const boom = require('@hapi/boom');

const { config } = require('./../config/config');

function checkApiKey(req, res, next) {
    const apikey = req.headers['api'];
    if (apikey === config.apikey) {
        next();
    } else {
        next(boom.unauthorized());
    }
}

module.exports = {}
```

`.env`

```env
API_KEY=789544
```

## Hashing de contraseñas con bcryptjs

Esta libreria es muy util para muchas otras cosas mas, porque hay ciertas legislaciones que sancionan si se tiene información sencible de los usuarios sin estar protegida como por ejemplo, nombre completo, dirección de email, direccion postal entre otras

con esta libreria se puede implementar una rutina que al momento de guardar información sencible la encripte y luego la guarde en la base de datos y cuando deba ser consultada por usuarios esta infirmación sea desencriptada para luego ser mostrada. Asi la infirmación guardada si llega a ser comprometida no podrá ser desencriptada a menos que se dispongan de las claves de encriptación y desencriptación

**encriptar password**

**Instalar `bcrypt`**

```sh
npm install bcrypt
```

`pass-hash.js`

```javascript
const bcrypt = require('bcrypt');

async function hashPassword() {
	const myPassword = 'admin 123 .202';
    const hashPassword = await bcrypt.hash(myPassword, 10);
    console.log(hash);
}

hashPasword();
```

`pass-verify.js`

```javascript
const bcrypt = require('bcrypt');

async function hashPassword() {
	const myPassword = 'admin 123 .202';
    const hash = 'hash..............................hash';
    const isMatch = await bcrypt.compare(myPassword, hash);
    console.log(isMatch);
}

hashPasword();
```

## Implementando hashing para usuarios

Pueden utlizar los hooks con sequelize para que realice el hash de la contraseña antes de guardar los datos. Solo tienes que agregar la opción `hooks` en el método config de la clase `User` que se encuentra en `user.model.js`

```js
static config(sequelize) {
    return {
      sequelize,
      tableName: USER_TABLE,
      modelName: 'User',
      timestamps: false,
      hooks: {
        beforeCreate: async (user, options) => {
          const password = await bcrypt.hash(user.password, 10);
          user.password = password;
        },
      }
    };
  }
```

De esta forma puedes evitar realizar el hash en los servicios `user` y `customer` y dejarlos como estaban anteriormente.

Para eliminar el password de la respuesta, simplemente colocar esto antes del `return`:

```javascript
  delete newCustomer.user.dataValues.password;
  return newCustomer;
}
```

To encrypt a password before being send to the database, just add a hook in `model.config`

```javascript
static config(sequelize) {
        return {
            sequelize,
            tableName: USER_TABLE,
            modelName: 'User',
            timestamps: false,
            **hooks: {
                beforeCreate: async(user) => {
                    const password = await bcrypt.hash(user.password, 10);
                    user.password = password;
                },
            }**
        }
    }
```

(This part of the code was made by @Carlos Alberto Angel Angel, some comments above this)

This is good! But when we create a password, user will receive the hash too, so we need to delete it from the info that will be sent.

In `user.service:` We delete password from `dataValues` when object is created

```javascript
async create(data) {

        const newUser = await models.User.create(data);
        //Delete password to don't send it when created
        **delete newUser.dataValues.password;**
        return newUser;
    }
```

#### Resolviendo Reto

Para poder resolver este reto lo que debemos de hacer entrar al objeto de dataValues luego al objeto de usuario y por ultimo al objeto de usuario. Y ya podemos acceder a la propiedad de contraseña para proceder a borrarla.

**Codigo**

```javascript
delete newCustomer.dataValues.user.dataValues.password;
return newCustomer;
```

# 3. Passport y JSON Web Tokens

## Implemetando login con Passport.js

- **Install**:

  ```sh
  npm install passport
  ```

  You may install all strategies as you want, some of the available strategies are:

  ```sh
  npm i passport-local
  ```

  

  ```sh
  npm ipassport-facebook
  ```

  

  ```sh
  npm i passport-twitter
  ```

  **`passport-jwt`**

- **Configure environment:**

  Following a good fold structure, we can follow the next structure

  - utils/
    `index.js`
    - auth/
      - strategies
  - Utils will contain all utilities in our program, one of these is **Authentication** tools
  - Inside **auth**, strategies stores all strategies that will be used in our application.
  - Finally to configure our strategies, create an`index.js` file.

- Programming strategies:

  - Step 1: Config Strategy

    Each strategy, have its own way to be programmed, in this case, local strategy is configured:

    ```jsx
    //Destructure passport-local strategy and get just Strategy
    const { Strategy } = require('passport-local');
    //Import service which will be used for authentication
    const Service = require("./../../../services/user.service.js")
    const userService = new Service();
    const boom = require('@hapi/boom');
    const bcrypt = require('bcrypt');
    //Create a new strategy and configure it
    const localStrategy = new Strategy({
            usernameField: 'email'
        },
        async(email, password, done) => {
            try {
                const user = await userService.findByMail(email);
    
                if (!user) {
                    //return error and it wont be authenticated
                    done(boom.unauthorized(), false);
                    return;
                }
                //Using bcrypt to compare the password with the hash
                const isMatch = await bcrypt.compare(password, user.password);
                if (!isMatch) {
                    done(boom.unauthorized(), isMatch);
                    return;
                }
                delete user.dataValues.password;
                //Null errors and return user
                done(null, user);
            } catch (err) {
                //Return error and false, it wont be authenticated
                done(err, false);
            }
        });
    
    //Export the strategy
    module.exports = localStrategy;
    ```

  - Step 2: Config `index.js`

    Just need to configure in this way all strategies which will be used in our application

    ```jsx
    //Require passport
    const passport = require('passport');
    //Require strategy used
    const localStrategy = require('./strategies/local.strategy');
    //Tell to passport we'll use localStrategy 
    passport.use(localStrategy);
    ```

  - Step 3: Config a route: `routes/auth.router.js`

    ```jsx
    const express = require('express');
    
    const passport = require('passport');
    const router = express.Router();
    
    router.post('/login',
        //We are using the local strategy and not using sessions
        passport.authenticate('local', { session: false }),
        async(req, res, next) => {
            try {
                res.json(req.user);
            } catch (err) {
                next(err);
            }
        }
    );
    
    module.exports = router;
    ```

  - Step 4: Make our application work with passport

    In file `index.js` of root applications, just configure passport before initializing routes.

    ```jsx
    const passport = require('passport');
    app.use(passport.initialize({ session: false }));
    ```

    - Final `index.js` example

      ```jsx
      /*Server basics*/
      const express = require('express');
      const routerApi = require('./routes');
      /*Middlewares*/
      const { logErrors, errorHandler, boomErrorHandler, queryErrorHandler } = require('./middlewares/error.handler');
      const checkApiKey = require('./middlewares/auth.handler');
      const app = express();
      const port = process.env.PORT || 3000;
      const passport = require('passport');
      app.use(passport.initialize({ session: false }));
      
      app.use(express.json());
      
      app.get('/', checkApiKey, (req, res) => {
          res.send('<h1 style="text-align: center;font-family: Roboto sans-serif;">Hello Everybody (:</h1>');
      });
      /*Routes config*/
      
      routerApi(app);
      
      require("./utils/auth");
      /*Middlewares Config */
      app.use(logErrors);
      app.use(boomErrorHandler);
      app.use(errorHandler);
      app.use(queryErrorHandler);
      /*Serve*/
      app.listen(port, () => {
          // eslint-disable-next-line no-console
          console.log(`Server running in port ${port}`);
      });
      ```

#### Solución de error con Passport.Js

#### En el momento de nosotros loguearnos nos lanza el siguiente error 👇🏽

```js
message": "req.logIn is not a function",
```

#### para poder solucionar este error sin tener que bajar nuestra version de passport podemos hacer lo siguiente 👨‍💻

#### Index.js

```javascript
const passport = require('passport')
// Antes de nuestras rutas debemos de colocar esto
app.use(passport.initialize());
// Rutas
routerApi(app);
```

#### Esto sucede ya que debemos de inicializar el middleware de passport antes de nuestras rutas y ahora todo deberia de funcionar con normalidad 🥳

## ¿Qué es un JWT?

Traduccion resumida de sobre JWT.

- **JSON Web Token (JWT)**

Es un estándar abierto (RFC 7519) que define una forma compacta y autónoma de transmitir información de forma segura entre partes como un objeto JSON. Esta información se puede verificar y confiar porque está firmada digitalmente. Los JWT se pueden firmar usando una palabra secreta (con el algoritmo HMAC) o un par de claves públicas / privadas usando RSA o ECDSA.

- **¿Cuándo deberíamos utilizar JSON Web Tokens?**

**Autorización**: este es el escenario más común para usar JWT. Una vez que el usuario haya iniciado sesión, cada solicitud posterior incluirá el JWT, lo que le permitirá acceder a rutas, servicios y recursos que están autorizados con ese token. El inicio de sesión único es una función que se utiliza ampliamente con JWT en la actualidad, debido a su pequeña sobrecarga y su capacidad para usarse fácilmente en diferentes dominios o servidores distribuidos.

**Intercambio de información**: los JWT son una buena forma de transmitir información de forma segura entre varias partes. Debido a que los JWT se pueden firmar, por ejemplo, utilizando pares de claves públicas / privadas, se puede estar seguro de que los remitentes son quienes dicen ser. Además, como la firma se calcula utilizando las cabeceras y el payload, también se puede verificar que el contenido no haya sido manipulado.

Btw, si te has preguntado qué es *payload* o no lo tienes muy claro te explicaré brevemente lo que es:
En CS, *payload* (o carga útil) significa al menos dos cosas.

1. La parte de los datos que lleva el mensaje. Esto deja de lado la metadata que puede venir de la cabecera (headers).
2. En seguridad informática o pentesting, *payload* también hace referencia a datos; sin embargo, éstos son la parte del programa malware que explota las vulnerabilidades de un sistema (*exploits*).

[Referencia](https://es.wikipedia.org/wiki/Carga_útil_(informática)) de seguimiento.

[![img](https://www.google.com/s2/favicons?domain=https://jwt.io//img/favicon/apple-icon-57x57.png)JSON Web Tokens - jwt.io](https://jwt.io/)

## Firmar y verificar tokens

Para los refresh tokens hay que definir un tiempo de expiración, eso se puede lograr pasando un tercer argumento de configuración a la función sign.

Para hacer que expire el token después de un cierto tiempo sería:

```javascript
const jwt = require('jsonwebtoken')

const jwtConfig = {
  expiresIn: '7d',
};
const payload = {
  sub: user.id,
  role: "customer"
}

const token = jwt.sign(payload, process.env.JWTSECRET, jwtConfig)
```

Observaciones:

- user es la instancia del usuario obtenido del modelo que tenga la propiedad Id del usuario.
- Se utiliza sub por conveniencia porque así lo maneja el standar de JWT pero puede usarse el nombre que uno quiera [mas info sobre los claims disponibles aquí](https://datatracker.ietf.org/doc/html/rfc7519#section-4)

si en **expiresIn** se pone sólo número entonces lo considera en segundo, pero si es un string entonces deberá llevar la unidad para definir el tiempo de expiración, ejemplo:

60 * 60 === '1h’
60 * 60 * 24 === ‘1d’

pero si por accidente se pone un **string sin unidad de tiempo** entonces lo tomará como **milisegundos**:
“60” === “60ms”

**Javascript Web Tockens:**

Allows to create continuous and secure connections in server-client

- Brief introduction of JWT

  - Install:

    `npm install jsonwebtoken`

  - Signing tokens

    To sign, create an env var or any secure variable to store a secret, which will allow you access to token information

    Example:

    `const secret = 'Run';`

    Then create payload, it contains all information you want to send

    ```jsx
    const payload = {
        sub: 1,
        role: 'customer',
    };
    ```

    Finally, use token functions to sign it:

    ```jsx
    //First argument is payload and second is secret key
    const token = jtw.sign(payload, secret);
    console.log(token);
    ```

  - Verify tokens

    ```jsx
    const jwt = require('jsonwebtoken');
    
    const secret = 'Runrun';
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInJvbGUiOiJjdXN0b21lciIsImlhdCI6MTYzNTQ0ODU0MX0.xnUAX-1jN2Gnp25LbYw7q0ezmm4Ya-T0FAZ7ePe7thg'
    
    const payload = jwt.verify(token, secret);
    console.log(payload);
    ```

[![img](https://www.google.com/s2/favicons?domain=https://jwt.io//img/favicon/apple-icon-57x57.png)JSON Web Tokens - jwt.io](https://jwt.io/)

[![img](https://www.google.com/s2/favicons?domain=//cdn2.auth0.com/styleguide/latest/lib/logos/img/favicon.png)Configure Silent Authentication](https://auth0.com/docs/login/configure-silent-authentication)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.auth0.com/styleguide/components/1.0.8/media/logos/img/favicon.png)What Are Refresh Tokens and How to Use Them Securely](https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/)

## Generar JWT en el servicio

- First of all, configure `.env` to provide a secret and it won’t be accessible from code.

  `jwtSecret="A SECRET REALLY SECRET"`

  Page to generate greats passwords:

  

- In our `auth.routes` configure secret such as we did in the last lesson

  ```jsx
  router.post('/login',
      //We are using the local strategy and not using sessions
      passport.authenticate('local', { session: false }),
      async(req, res, next) => {
          try {
              const user = req.user;
              **const payload = {
                  sub: user.id,
                  role: user.role
              }**
  
              **const token = jwt.sign(payload, config.auth.jwtSecret);**
  
              res.json({ user, token });
  
          } catch (err) {
              next(err);
          }
      }
  );
  ```

- Finally,![Authh.png](https://static.platzi.com/media/user_upload/Authh-8628de4a-bfc9-4451-ab31-0e9794675f44.jpg) try it in insomnia/postman!

> Al implementar JWT ya no es necesario enviar los datos del usuario en la petición, ya que por medio del payload del token podemos enviarla, ademas recordar que por ningún motivo se debe enviar informacion sensible del usuario.

## Protección de rutas

instalar 

```sh
npm install passport-jwt
```

`jwt.strategy.js`

```javascript
const {Strategy, ExtractJwt } = require('passport-jwt');

const { config } = require('../../../config/config');

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtSecret
}

const JwtStrategy = new Strategy(options, (payload, done) => {
    return done(null, payload);
});

module.exports = JwtStrategy;
```

[![img](https://www.google.com/s2/favicons?domain=https://www.passportjs.org/packages/passport-jwt//images/favicon/favicon.ico)passport-jwt](https://www.passportjs.org/packages/passport-jwt/)

## Control de roles

> Un 403, no tiene permisos de acceso. 401 yo lo uso solo para si es sesion o no.

To create a **role control**, a **middleware** will be our best friend

First of all, let’s think about what we need, we need a **middleware** able to check what kind of users is authenticated, for this, let’s create a single function, which will receive a list of **roles** and return a **middleware.**

```jsx
//@param roles array of roles that can access the route
function checkRole(roles) {

    return (req, res, next) => {
        const user = req.user;
        //Check if rol is allowed to access
        if (!roles.includes(user.role)) {
            next(boom.unauthorized("Unauthorized!You cannot do this, Admins have been notified "));
        } else {
            //If everything is right, go to next middleware
            next();
        }
    }
}
```

Finally just use it wherever you need to check role, por example:

```jsx
router.post("/",
    passport.authenticate('jwt', { session: false }),
    checkRole(['admin', 'Captain']),
    validatorHandler(getOrderSchema, "body"),
    async(req, res, next) => {
        try {
            const body = req.body;
            res.status(201).json(await service.create(body));
        } catch (error) {
            next(error);
        }
    }
);
```

**Recommendation**

[Acces Control NPM](https://www.npmjs.com/package/accesscontrol)

[![img](https://www.google.com/s2/favicons?domain=https://static.npmjs.com/58a19602036db1daee0d7863c94673a4.png)accesscontrol - npm](https://www.npmjs.com/package/accesscontrol)

## Obteniendo órdenes del perfil

Para resolver el reto de crear la order sin neesidad de enviar el customerId en el body de la petición hariamos lo siguente:

En order.router.js

```javascript
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  //validatorHandler(createOrderSchema, 'body'),
  async (req, res, next) => {
    try {
      const body = {
        userId: req.user.sub
      };
      const order = await service.create(body);
      res.status(201).json(order);
    } catch (error) {
      next(error);
    }
  }
);
```

Hay que eliminar el validator porque ahora no es necesario pasar en el body el customer id, pero se agrega el jwt validator

Luego en order.service.js

```javascript
  async create(data) {
    // Accedemos al modelo Customer y usando where encadenamos hacia user
    const customer = await models.Customer.findAll({
      where: {
        '$user.id$': data.userId
      },
      include: ['user']
    });
    // Validamos que exista el customer
    if (!customer) {
      throw boom.notFound('Customer not found');
    }
    // Creamos un objeto con el customerId obtenido de la consulta
    const dataOrder = {
      customerId: customer[0].id
    };
    const newOrder = await models.Order.create(dataOrder);
    return newOrder;
  }
```

TRas un poco de depuración con este metodo vi que los resultados de la consulta llegan en un Array, si se canbia el FinAll por findOne en ese caso solo retorna un unico objeto y no haria falta usar el customer[0] sino directamente customer.

Para el reto, creamos una nueva ruta, `post`:

```javascript
router.post("/my-orders",
    passport.authenticate('jwt', { session: false }),
    async(req, res, next) => {
        const data = { id: req.user.sub }

        try {
            res.status(201).json(await service.createFromProfile(data));
        } catch (error) {
            next(error);
        }
    }
);
```

Para que esto funcione, necesitamos que el servicio tenga una nueva función:

```JavaScript
   async createFromProfile(data) {
        const user = await service.findOne(data.id);
        const customerId = user.customer.id;
        return models.Order.create({ customerId });
    }
```

Y listo, con eso debe estar funcionando (:

## Manejo de la autenticación desde el cliente

`token`

```json
{
	"user": {
		"id": 4,
		"email": "superadmin@mail.com",
		"role": "admin",
		"createAt": "2021-07-32T16:25:12"
	},
	"token": "....."
}
```

#### Client Sesion (Browser)

- Un estado de login.
- Cookies o LocalStorage.
- Enviar en el header.
- Refresh token.
- Validar Permisos.



> Porque es mejor practica poner el token en las cookies en lugar del localStorage?
>
> Puedes acceder a través de JavaScript al LocalStorage, por lo que un atacante podría acceder a esa información, por otro lado, no se puede acceder a las cookies, por eso LocalStorage no se utiliza para almacenar información sensible. Otra razón es porque las cookies sólo almacenan hasta 5Kb y LocalStorage hasta 5Mb y las cookies expiran, LocalStorage no.

# 4. Envío de emails con Node.js

## Cómo enviar emails con Node.js

🚀 Aca encontraran la documentacion de [nodemailer](https://nodemailer.com/about/)

```sh
npm install nodemailer
```

`nodemailer.js`

```javascript
const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function sendMail() {

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    secure: true, // true for 465, false for other ports
    port: 465,
    auth: {
      user: 'nicobytes.demo@gmail.com',
      pass: 'dmlikrjugujjlugl'
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: 'nicobytes.demo@gmail.com', // sender address
    to: "santibytes.demo@gmail.com", // list of receivers
    subject: "Este es un nuevo correo", // Subject line
    text: "Hola santi", // plain text body
    html: "<b>Hola santi</b>", // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

sendMail();
```

[![img](https://www.google.com/s2/favicons?domain=http://nodemailer.com/about//favicon-32x32.png)Nodemailer :: Nodemailer](http://nodemailer.com/about/)

## Implementando el envío de emails

```js
const express = require('express');
const passport = require('passport');

const AuthService = require('./../services/auth.service');

const router = express.Router();
const service = new AuthService();

router.post('/login',
  passport.authenticate('local', {session: false}),
  async (req, res, next) => {
    try {
      const user = req.user;
      res.json(service.signToken(user));
    } catch (error) {
      next(error);
    }
  }
);

router.post('/recovery',
  async (req, res, next) => {
    try {
      const { email } = req.body;
      const rta = await service.sendMail(email);
      res.json(rta);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;
```

`auth.service`

```javascript
const boom = require('@hapi/boom');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const { config } = require('./../config/config');
const UserService = require('./user.service');
const service = new UserService();

class AuthService {

  async getUser(email, password) {
    const user = await service.findByEmail(email);
    if (!user) {
      throw boom.unauthorized();
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      throw boom.unauthorized();;
    }
    delete user.dataValues.password;
    return user;
  }

  signToken(user) {
    const payload = {
      sub: user.id,
      role: user.role
    }
    const token = jwt.sign(payload, config.jwtSecret);
    return {
      user,
      token
    };
  }

  async sendMail(email) {
    const user = await service.findByEmail(email);
    if (!user) {
      throw boom.unauthorized();
    }
    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      secure: true, // true for 465, false for other ports
      port: 465,
      auth: {
        user: 'nicobytes.demo@gmail.com',
        pass: 'dmlikrjugujjlugl'
      }
    });
    await transporter.sendMail({
      from: 'nicobytes.demo@gmail.com', // sender address
      to: `${user.email}`, // list of receivers
      subject: "Este es un nuevo correo", // Subject line
      text: "Hola santi", // plain text body
      html: "<b>Hola santi</b>", // html body
    });
    return { message: 'mail sent' };
  }
}

module.exports = AuthService;
```

# 5. Recuperación de contraseñas

## Generando links de recuperación

Cuando generas el token de recuperación y te llega al correo, podrías copiar ese token de recuperación y usarlo para acceder a cualquier endpoint, ya que al estar firmado con el mismo secreto que el access token sería valido

## Validando tokens para cambio de contraseña

Podría ser válido sólo por unos minutos y sólo para un endpoint específico (el de recovery) por eso es recomendable tener 3 firmas diferentes para los tokens:

- 1 para acceso a recursos
- 1 para refrescar la sesión ([refresh token](https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/))
- 1 para recovery

de esta forma se agrega mas seguridad a los endpoint.

Es verdad que con este token uno podría enviar los datos al endpoint para cambiar contraseña y obtener un token de acceso para los demás endpoint, pero solo sería en casos en que el email quede desprotegido o estén monitoreando la red del usuario y esten justo activos en el proceso de esos 10 o 15 minutos en el que el token de recuperación de contraseña sea válido para cambiar la contraseña. En general no creo que haya problema, si se necesita mas seguridad, siempre está la opción de two factor authenticator.

```javascript
const express = require('express');
const passport = require('passport');

const AuthService = require('./../services/auth.service');

const router = express.Router();
const service = new AuthService();

router.post('/login',
  passport.authenticate('local', {session: false}),
  async (req, res, next) => {
    try {
      const user = req.user;
      res.json(service.signToken(user));
    } catch (error) {
      next(error);
    }
  }
);

router.post('/recovery',
  async (req, res, next) => {
    try {
      const { email } = req.body;
      const rta = await service.sendRecovery(email);
      res.json(rta);
    } catch (error) {
      next(error);
    }
  }
);

router.post('/change-password',
  async (req, res, next) => {
    try {
      const { token, newPassword } = req.body;
      const rta = await service.changePassword(token, newPassword);
      res.json(rta);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;
```

# 6. Despliegue a producción

## Deploy en Heroku

Si queremos crear un usuario admin por defecto solo debemos agregar a nuestra migración luego de crear la tabla usuario el siguiente codigo

```js
    const hash = await bcrypt.hash('123456', 10);
    await queryInterface.bulkInsert(USER_TABLE, [
      {
        username: 'admin',
        email: 'admin@domain.com',
        password: hash,
        role: 'admin,
        created_at: new Date()
      }
    ]);
```

de esta forma al momento de ejecutar la migración nos creara un usuario con rol admin y las credenciales y asi nuestro endpoint de creación de uuarios estará protegido.

[![img](https://www.google.com/s2/favicons?domain=https://www.herokucdn.com/favicon.ico)Cloud Application Platform | Heroku](https://www.heroku.com/)

# 7. Próximos pasos

## Cómo seguir aprendiendo backend

