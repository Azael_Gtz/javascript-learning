<h1>Backend con Node.js: API REST con Express.js</h1>

<h3>Nicolas Molina</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [¿Qué es Express.js?](#qué-es-expressjs)
  - [Configuración del entorno de desarrollo](#configuración-del-entorno-de-desarrollo)
  - [Instalación de Express.js y tu primer servidor HTTP](#instalación-de-expressjs-y-tu-primer-servidor-http)
  - [Routing con Express.js](#routing-con-expressjs)
- [2. CRUD](#2-crud)
  - [¿Qué es una RESTful API?](#qué-es-una-restful-api)
  - [GET: recibir parámetros](#get-recibir-parámetros)
  - [GET: parámetros query](#get-parámetros-query)
  - [Separación de resposabilidades con express.Router](#separación-de-resposabilidades-con-expressrouter)
  - [Instalación de Postman o Insomia](#instalación-de-postman-o-insomia)
  - [POST: método para crear](#post-método-para-crear)
  - [PUT, PATCH y DELETE](#put-patch-y-delete)
  - [Códigos de estado o HTTP response status codes](#códigos-de-estado-o-http-response-status-codes)
- [3. Servicios](#3-servicios)
  - [Introducción a servicios: crea tu primer servicio](#introducción-a-servicios-crea-tu-primer-servicio)
  - [Crear, editar y eliminar](#crear-editar-y-eliminar)
  - [Async await y captura de errores](#async-await-y-captura-de-errores)
- [4. Middlewares](#4-middlewares)
  - [¿Qué son los Middlewares?](#qué-son-los-middlewares)
  - [Middleware para HttpErrors](#middleware-para-httperrors)
  - [Manejo de errores con Boom](#manejo-de-errores-con-boom)
  - [Validacion de datos con Joi](#validacion-de-datos-con-joi)
  - [Probando nuestros endpoints](#probando-nuestros-endpoints)
  - [Middlewares populares en Express.js](#middlewares-populares-en-expressjs)
- [5. Deployment](#5-deployment)
  - [Consideraciones para producción](#consideraciones-para-producción)
  - [Problema de CORS](#problema-de-cors)
  - [Deployment a Heroku](#deployment-a-heroku)
- [6. Próximos pasos](#6-próximos-pasos)
  - [Continúa en el Curso de Node.js con PostgreSQL](#continúa-en-el-curso-de-nodejs-con-postgresql)


# 1. Introducción

## ¿Qué es Express.js?

[BackendConNodeJS-Express.pdf](https://drive.google.com/file/d/1wMaI6O-DfPZDFQILGBvvy3wDzpHPV7xB/view?usp=sharing)

## Configuración del entorno de desarrollo

La instalación de paquetes : 

```bash
npm i nodemon eslint eslint-config-prettier eslint-plugin-prettier prettier -D
```

1. gitignore para las configuraciones

2. En VS instalar el Pluying EditorConfig
   3.En Terminal ejecutar: 

   ```sh
   npm i nodemon eslint eslint-config-prettier eslint-plugin-prettier prettier -D
   ```

   

3. ```sh
   npm run dev
   ```

   

4. ```sh
   npm run start
   ```

   

Desarrollo. 😁
⠀⠀⠀⠀
**Git Ignore:**
⠀⠀⠀⠀
Son archivos ignorados que suelen ser artefactos de compilación y archivos generados por el equipo que pueden derivarse de tu fuente de repositorios o que no deberían confirmarse por algún otro motivo. Ejemplo: node_modules.

**Linters:**
⠀⠀⠀⠀
Herramientas que realizan la lectura del código fuente

- Detectan errores/warnings de código
- Variables sin usar o no definida, llave sin cerrar…

⠀⠀⠀⠀
**Nodemon:**
⠀⠀⠀⠀
Es una herramienta que ayuda a desarrollar aplicaciones en Node.js al reiniciar la aplicación autómaticamente cuando hay cambios en el archivo.
⠀⠀⠀⠀
[**gitignore.io:**](https://www.toptal.com/developers/gitignore)
⠀⠀⠀⠀
Contiene la configuración del entorno que específiques
⠀⠀⠀⠀
**Editor Config:**
⠀⠀⠀⠀
Todos los desarrolladores tendrán la misma configuración del proyecto.

<img src="https://www.toptal.com/developers/gitignore/img/gitignore-logo-horizontal.png" alt="gitignore.io" style="zoom:25%;" />[gitignore.io](https://www.toptal.com/developers/gitignore)

El archivo package.json es el punto de entrada de las aplicaciones JavaScript. En él tendremos los scripts para levantar nuestra app, hacer el build, ejecutar los tests, etc. También quedarán registradas las dependencias de nuestra aplicación. De todo esto se encarga npm, Node Package Manager, que se nos instalará junto a Node.

Si empezamos una aplicación de 0, podemos crear el archivo con npm init. Nos hará una serie de preguntas, normalmente si estamos desarrollando una aplicación propia y no un package de npm, nos puede ser más rápido usar npm init -y para crearlo directamente con las opciones por defecto.

También en el caso de una app propia, nos interesará añadir “private”: true para evitar publicarla accidentalmente en el registry de npm.

Tienes todas las opciones de configuración del package.json en la documentación de npm.

Si tenemos nuestro proyecto en GitHub podemos mantener nuestras dependencias actualizadas activando Dependabot en la configuración de nuestro repositorio.

Este repositorio starter para aplicaciones vanilla JS cuenta con lo mínimo para tener una experiencia de desarrollo agradable:

Webpack (v5) para procesar nuestros archivos JavaScript y assets
Babel con preset-env para dar soporte a navegadores antiguos (puedes editar la lista en el package.json)
ESLint con Prettier
Jest con DOM Testing Library para tests unitarios
Cypress con Testing Library para tests de aceptación
GitHub Action workflows para ejecutar eslint y tests en cada push

Descarguen el repositorio acá -> [javascript skeleton](https://github.com/CodelyTV/javascript-basic-skeleton)

## Instalación de Express.js y tu primer servidor HTTP

😄: Instalar `express` 

```sh
npm i express
```

Código:

```js
const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) =>{
  res.send("Hola mi server en Express");
});

app.listen(port, () =>{
  console.log("My port: " + port);
});
```

El primera paso para crear un servidor http con express es requerir dicho modulo

```jsx
const express = require('express');
```

Seguidamente creamos una aplicación de express

```jsx
const app = express();
```

Indicamos el puerto donde queremos que corra nuestra aplicacion (normalmente es en el puerto 3000)

```jsx
const port = 3000;
```

Con esto creamos la aplicacion pero aún no se esta utilizando, para empezar a utilizarla podemos crear una ruta de tipo get, esta funcion recibe dos parametros, el primero sera indicar la ruta y el segundo una funcion callback, la funcion callback a su vez recibe dos parametros que son los request(req) y la response(res).

```jsx
app.get('/', (req, res) => {
	res.send('Hola mi server en express')
}
```

En este caso la ruta sera la principal ‘/’, es decir, que cuando se acceda a esta ruta el sevidor enviara un mensaje.

Finalmente tenemos que hacer que la aplicacion sea escuchada en un puerto, lo hacemos con el metodo listen, este metodo recibe dos parametros que son el puerto donde sera escuchado y una funcion callback

```jsx
app.listen(port, () => {
	console.log('Mi port' + port)
}
```

Así se vería el código completo

```jsx
const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hola mi server en express');
});

app.listen(port, () => {
  console.log('Mi port ' + port);
});
```

[![img](https://www.google.com/s2/favicons?domain=http://expressjs.com//images/favicon.png)Express - Node.js web application framework](http://expressjs.com/)

## Routing con Express.js

Express ocupa el metodo get, para recibir las rutas en el servidor

Se puede responder con un mensaje

```
res.send('Contenido de respuesta');
```

O una respuestas en formato JSON

```json
res.json({
    name: 'Producto 1',
    price: 1000
  });
```

Se recomienda instalar el plugin
**JSON Viewer -> Google-chrome**

![plugin.PNG](https://static.platzi.com/media/user_upload/plugin-c5785107-4cb5-4bde-9d3d-d5cb53c11ff9.jpg)

### req y res

`req` es un objeto que contiene información sobre la solicitud **HTTP** que provocó el evento. En respuesta a`req`, usa `res`para devolver la respuesta **HTTP** deseada.

[![img](https://www.google.com/s2/favicons?domain=https://www.google.com/images/icons/product/chrome_web_store-32.png)JSON Viewer - Chrome Web Store](https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh)

# 2. CRUD

## ¿Qué es una RESTful API?

#### REST: Representational State Transfer

Es una conveccion que se refiere a servicios web por protocolo HTTP

Metodos:

- **Get**: Obtener
- **Put**: Modificar/Actualizar
- **Patch**: Modificar/Actualizar
- **Post**: Crear
- **Delete**: Eliminar

#### Patch

El **método de solicitud HTTP `PATCH`** aplica modificaciones parciales a un recurso.

PATCH es algo análogo al concepto de “actualización” que se encuentra en [CRUD](https://developer.mozilla.org/en-US/docs/Glossary/CRUD), Una solicitud se considera un conjunto de instrucciones sobre cómo modificar un recurso. Contrasta esto con [PUT](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT); que es una representación completa de un recurso.`PATCH

Mo es necesariamente idempotente, aunque puede serlo. Contrasta esto con [PUT](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT); que siempre es idempotente.

La palabra “idempotente” significa que cualquier número de solicitudes repetidas e idénticas dejará el recurso en el mismo estado.

Por ejemplo, si un campo de contador de incremento automático es una parte integral del recurso, entonces un [PUT](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT) lo sobrescribirá naturalmente (ya que sobrescribe todo), pero no necesariamente para .`PATCH`

`PATCH` (como [POST](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST)) *puede* tener efectos secundarios sobre otros recursos.

[PATCH - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH)

![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-d336ab2e-8e2d-40a4-808a-ee3da1fbdaef.jpg)

REST: Representational State Transfer
⠀⠀
El diseño REST o RESTful API (Representational State Transfer) está diseñado para aprovechar los protocolos existentes.⠀⠀
⠀⠀
Si bien REST se puede usar en casi cualquier protocolo, generalmente aprovecha HTTP cuando se usa para API web.⠀⠀
⠀⠀⠀⠀
Esto significa que los desarrolladores no necesitan instalar bibliotecas o software adicional para aprovechar un diseño de API REST. REST API Design fue definido por el Dr. Roy Fielding en su disertación de doctorado de 2000. Destaca por su increíble capa de flexibilidad. Dado que los datos no están vinculados a métodos y recursos, REST tiene la capacidad de manejar múltiples tipos de llamadas, devolver diferentes formatos de datos e incluso cambiar estructuralmente con la implementación correcta de hipermedia.

https://www.mulesoft.com/resources/api/what-is-rest-api-design#:~:text=REST or RESTful API design,when used for Web APIs.

![Screenshot 2021-10-06 080951.png](https://static.platzi.com/media/user_upload/Screenshot%202021-10-06%20080951-029b6b2a-60c1-494a-bec8-df44d3d20e3e.jpg)

## GET: recibir parámetros

>  *Los endpoints son las URLs de un API o un backend que responden a una petición. Los mismos entrypoints tienen que calzar con un endpoint para existir. Algo debe responder para que se renderice un sitio con sentido para el visitante.*

Agregando productos, categorías y clientes

```js
app.get('/products/:id' , (req,res)=>{
  const {id} = req.params
  res.json({
    id,
    name:'Jabon',
    price:2000,
    available:true
  })
} )

app.get('/categories/:id' , (req,res)=>{
  const {id} = req.params
  res.json({
    id,
    name:'Limpieza',
    products:13
  })
} )

app.get('/clients/:id' , (req,res)=>{
  const {id} = req.params
  res.json({
    id,
    firstName:'Nicolas',
    lastName:'Molina'
  })
} )
```

## GET: parámetros query

> Los endpoints especificos deben declararsen antes de los endpoints dinamicos. Uno de los mandamientos.

`faker`

```sh
npm i faker -D
```

users.js

```javascript
const faker = require('faker');

const init = () => {
	const result = [];
	for (let i = 0; i < 200; i++) {
		result.push({
			id: i,
			firstName: faker.name.firstName(),
			lastName: faker.name.lastName(),
			gender: faker.name.gender(),
		});
	}

	return result;
};

module.exports = { init };
```

luego, en el index inicializo el objeto con los usuarios

```javascript
app.listen(port, () => {
	initialDB = {
		users: users.init(),
	};
	console.log(`Server running at port ${port}`);
});
```

y finalmente filtro los usuarios en la operación get

```javascript
app.get('/users', (req, res) => {
	const { offset, limit } = req.query;
	const result = [];
	if (offset && limit) {
		/* Los parámetros requperados de query, 
		vienen como string, es necesario pasarlos a int*/
		for (let i = parseInt(offset); i < parseInt(limit); i++) {
			result.push(initialDB.users[i]);
		}
		res.json(result);
	} else {
		res.send('No data found');
	}
});
```

## Separación de resposabilidades con express.Router

Los tan respetados principios [SOLID](https://profile.es/blog/principios-solid-desarrollo-software-calidad/) entendiendolos poco a poco, nos hace ser mejores desarrolladores.

Ya maneje la organización de manera distinta usando clases en typescript, y dentro de las clases manejo los middlewares, routes o listen del servidor mediante métodos.

Mi estructura de directorios es algo así
![img](https://i.postimg.cc/V6NcJS9q/directorio.png)

el index.ts ubicado en el directorio raíz inicialice la clase y posteriormente ejecuta el método listen

```ts
import {Server} from "./app/server"

const server = new Server();
server.listen()
```

y el index.ts dentro de app/server/ se encuentra la clase donde se ejecuta express

```ts
import express from "express";
import products from "../routes/products";
export class Server {
  port: string | number;
  app: express.Application;
  paths: { [index: string]: string };
  constructor() {
    this.port = 8000,
    this.app = express(),
    this.paths = {
      products: "/api/products",
      users: "/api/users",
      categories: "/api/categories"
    };
    this.routes();
  }

  routes(): void {
    this.app.use(this.paths.products, products)
  }

  listen(): void {
    this.app.listen(this.port, () => {
      console.log(`Corriendo en ${this.port}`);
    });
  }
}
```

y para las rutas es muy similar a como lo realiza el instructor, ejemplo para products.ts

```ts
import { Router } from "express";
import faker from "faker";
const router = Router();

router.get("/", (req, res) => {
  const { size } = req.query;
  const products = [];
  const limit = size || 100;
  for (let index = 0; index < limit; index++) {
    products.push({
      name: faker.commerce.productName(),
      price: parseInt(faker.commerce.price(), 10),
      image: faker.image.imageUrl(),
    });
  }
  res.send(products);
});

router.get("/filter", (req, res) => {
  res.json({ msg: "Soy un filtro" });
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  res.send({ id, name: "product1", price: "1000" });
});

export default router;
```

## Instalación de Postman o Insomia

Para poder probar nuestra API de forma más productiva vamos a tener que usar algún cliente de APIs que nos permita hacer las pruebas de funcionamiento de lo que estás construyendo, los dos clientes más famosos son **Insomnia** y **Postman** y vas a necesitar de alguno de ellos para ir probando cada característica que vamos a ir construyendo en nuestro servicio de Platzi Store.

## Insomnia

Como cliente para probar APIs tiene grandes características, destaca principalmente por una interfaz limpia y sencilla, pero a la vez potente, en donde puede configurar ambientes, exportar e importar, gran soporte con GraphQL, etc. **Insomnia** será el que vamos a usar en este curso 🙂

![img](https://raw.githubusercontent.com/Kong/insomnia/develop/screenshots/main.png)

### Instalación

La instalación es sencilla, solo tienes que ingresar a https://insomnia.rest/download y descargar el instalador. Una vez esté descargado lo ejecutas y sigues los pasos de la instalación; en caso de Windows descarga un archivo `.exe`, en caso de Mac descarga un `.dmg`, y finalmente, en caso de **Ubuntu** descargas el `.deb`.

## Postman

![img](https://www.postman.com/assets/response-screenshot.svg)

Es uno de los más usados y legendariamente nos ha acompañado por mucho tiempo, además cuenta con características similares a Insomnia como: exportar e importar, ambientes, entornos, y provee una API para hacer testing muy potente.

### Instalación Windows y Mac

La instalación es sencilla solo tienes que ingresar a https://www.postman.com/downloads/
y descargar el instalador, una vez esté descargado lo ejecutas y sigues los pasos de la instalación, en caso de Windows descarga un archivo `.exe` y en caso de Mac descargas un archivo comprimido lo descomprimes y ahí sigues el proceso.

### Instalación Linux

```bash
# Debian, Ubuntu y derivados
sudo apt install postman
# Arh, Manjaro
sudo pacman -S postman
# Fedora
sudo yam install postman
```

[![Insomnia](https://insomnia.rest/images/insomnia-logo.svg)](https://insomnia.rest/download)

## POST: método para crear

**Middleware**:
⠀⠀
El término middleware se refiere a un sistema de software que ofrece servicios y funciones comunes para las aplicaciones. En general, el middleware se encarga de las tareas de gestión de datos, servicios de aplicaciones, mensajería, autenticación y gestión de API.
⠀⠀⠀
Ayuda a los desarrolladores a diseñar aplicaciones con mayor eficiencia. **Además, actúa como hilo conductor entre las aplicaciones, los datos** y los usuarios.

[Red Hat Middleware](https://www.redhat.com/es/topics/middleware/what-is-middleware)

## PUT, PATCH y DELETE

Cree el de usuarios y categorías siguiendo el mismo ejemplo que productos dejo mi ejemplo de usuarios

```js
const express = require('express');
const faker = require('faker');
const router = express.Router();
```

READ users

```js
router.get('/', (req, res) => {
  const users = [];
  const {size} = req.query;
  const limit = size || 10;
  for (let i = 0; i < limit; i++) {
    users.push({
      name: faker.name.firstName(),
      lastName: faker.name.lastName(),
      image: faker.image.imageUrl(),
    });
  }
  res.json(users);
});
```

READ user

```js
router.get('/:id', (req, res) => {
  const { id } = req.params;

  res.json({
    id,
    name: faker.name.firstName(),
    lastName: faker.name.lastName(),
    image: faker.image.imageUrl(),
  });
});
```

CREATE

```js
router.post('/', (req, res) => {
  const body = req.body;
  res.json({
    message: 'Creation',
    data: body
  });
});
```

UPDATE

```js
router.patch('/:id', (req, res) => {
  const { id } = req.params;
  const body = req.body;
  res.json({
    message: 'Update',
    data: body,
    id
  });
});
```

DELETE

```js
router.delete('/:id', (req, res) => {
  const { id } = req.params;
  res.json({
    message: 'deleted element',
    id
  });
});

module.exports = router;
```

![insomnia.PNG](https://static.platzi.com/media/user_upload/insomnia-0d2796b3-d831-44e6-9053-65d7b6323b74.jpg)

## Códigos de estado o HTTP response status codes

`código HTTP`

### [httpstatuses](https://httpstatuses.com/)

[![img](https://www.google.com/s2/favicons?domain=https://http.cat//favicon.png)HTTP Cats](https://http.cat/)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/HTTP/favicon-48x48.97046865.png)HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP)

# 3. Servicios

## Introducción a servicios: crea tu primer servicio

![img](https://groovy-stocking-90c.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Fca2ad85d-b763-424c-8de5-6dc2bd0cabff%2FUntitled.png?table=block&id=a722cb02-0f23-4944-9471-de9cb751aeaa&spaceId=f48fe2f0-baec-4a02-9cd8-95e7e5dc99df&width=1320&userId=&cache=v2)

![img](https://groovy-stocking-90c.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F2448a7dd-c8fe-4b1d-8917-0c158cf71239%2FUntitled.png?table=block&id=bb345abb-9c92-43d2-94ed-0b53ea916451&spaceId=f48fe2f0-baec-4a02-9cd8-95e7e5dc99df&width=1330&userId=&cache=v2)

Creamos nuestra estructura

### products.services.js

```jsx
const faker = require('faker');

class ProductsService {
  constructor() {
    this.products = [];

    this.generate(); // Iniciamos nuestros servicios de usuarios
  }

  generate() {}

  // Funciones para los servicios
  create() {}

  find() {}

  findOne(id) { // Devolvemos un solo dato}

  update() {}

  delete() {}
}

module.exports = ProductsService;
```

Nuestro metodo generate se va a encargar de hacer el llamado a faker y setear los datos

```jsx
generate() {
    const limit = 10;

    // Push de productos
    for (let i = 0; i < limit; i++) {
      this.products.push({
        id: faker.datatype.uuid(),
        nombre: faker.commerce.productName(),
        precio: parseInt(faker.commerce.price(), 10), // Numero en base 10
        descripcion: faker.lorem.sentence(),
        imagen: faker.image.imageUrl(),
      });
    }
  }
```

El metodo find se encarga de retornar todo los datos y findOne se encarga de retornar un solo dato

```jsx
find() {
    // Devolvemos los datos
    return this.products;
  }

  findOne(id) { // Devolvemos un solo dato
    return this.products.find((item) => item.id == id);
  }
```

Para usar los servicios en nuestros router se importa y setea en los lugares requeridos

```jsx
const ProductsService = require('../services/products.services');

//...

// Servicio de productos
const service = new ProductsService();
router.get('/', (req, res) => {
  // Traemos los datos desde nuestros servicios
  const products = service.find();

  res.json(products); // Respuesta
});

//...

router.get('/:id', (req, res) => {
  const { id } = req.params; // Destructuracion del parametro
  const product = service.findOne(id); // Lo trae del servicio

  res.json(product); // Respuesta con el parametro
});
```

**Products.routes**

```javascript
const express = require('express');
const router = express.Router();
const productService = require('../services/product.service');
const product = new productService();

router.get('/', (req, res) => {
    let {limit, offset} = req.query;
    res.json(product.find({offset, limit}));
});

router.get('/:id', (req, res) => {
    const {id} = req.params;
    res.json(product.findOne(id));
});


router.post("/", (req, res) => {
    const {name, price, image} = req.body;

    const newProduct = product.create({name, price, image});
    if (newProduct) {
        res.status(201).json({
            message: "Product added",
            data: newProduct
        });

    } else
        res.status(501).json({message: "internal error"});
});


router.patch("/:id", ((req, res) => {
    const id = parseInt(req.params.id);
    const {name, price, image} = req.body;
    const updateProduct = product.update(id, {name, price, image});
    if (updateProduct) {
        res.json({
            message: "Product updated",
            data: req.body
        });
    } else {
        res.status(501).json({message: "Internal error"})
    }


}));

router.delete("/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const currentProduct = product.delete(id);
    if (currentProduct) {
        res.status(201).json({message: "Product deleted", data: currentProduct});
    } else {
        res.status(404).json({message: "Product not found"});
    }
});


module.exports = router;
```

**prduct.service.js**

```javascript
const faker = require("faker");

class ProductService {

    constructor() {
        this.products = [];
        this.faker();
    }

    faker(quantity = 100) {
        for (let i = 0; i < quantity; i++) {
            this.products.push({
                id: i + 1,
                name: faker.commerce.productName(),
                price: parseInt(faker.commerce.price(), 10),
                image: faker.image.imageUrl()
            });
        }

    }

    create(product = {}) {
        const newProduct = {
            id: this.products[this.products.length - 1].id + 1,
            name: product.name,
            price: parseInt(product.price),
            image: product.image
        }
        this.products.push(newProduct);

        return newProduct;
    }

    find(filter = {}) {
        return this.products.slice(filter.offset | 0, filter.limit ? parseInt(filter.limit) + parseInt((filter.offset | 0)) : undefined)
    }


    findOne(id) {
        return this.products.find((p) => p.id = id);
    }

    update(id, changes = {}) {
        const product = this.products.find((prod) => prod.id === id);
        const idx = this.products.findIndex((prod) => prod.id === id);
        if (product) {
            this.products[idx] = {
                id: product.id,
                name: changes.name || product.name,
                price: changes.price || product.price,
                image: changes.image || product.image
            };
            return product;
        }
    }


    delete(id) {
        const product = {...this.products.find((prod) => prod)};
        if (product) {
            this.products = this.products.filter((p) => p.id !== id);
            return product;
        }
    }
}

module.exports = ProductService;
```

## Crear, editar y eliminar

El **`create`** quize hacerlo de la siguiente manera, y así créo un producto con las propiedades que quiero 👇… solo por seguridad 😉

```js
create({ name, price, image }) {
  const product = {
    id: faker.datatype.uuid(),
    name,
    price,
    image,
  };
  this.products.push(product);
  return product;
}
```

## Async await y captura de errores

 opción implementar el patrón singleton

Products Service

```
class ProductsService {
  static _productsServiceInstance = null;

  static getInstance() {
    if (ProductsService._productsServiceInstance === null) {
      ProductsService._productsServiceInstance = new ProductsService();
    }
    return ProductsService._productsServiceInstance;
  }
```

Products.router

```js
const express = require('express');
const ProductsService = require('../services/Products.service');

const router = express.Router();

//* GET all products
router.get('/', getAll);

//* GET product by ID
router.get('/:id', getOne);

//! ADD new product
router.post('/', create);

//* UPDATE partial product
router.patch('/:id', updateProduct);

//! DELETE product
router.delete('/:id', deleteProduct);


//* Internal Functions
async function getAll(req, res) {
  const productsService = await ProductsService.getInstance();
  const products = await productsService.find();
  res.json(products);
}

async function getOne(req, res) {
  const productsService = await ProductsService.getInstance();

  const { id } = req.params;
  const product = await productsService.findOne(id);

  res.json(product);
}

async function create(req, res) {
  const productsService = await ProductsService.getInstance();

  const product = await productsService.create(req.body);

  res.json(product);
}

async function updateProduct(req, res) {
  const productsService = await ProductsService.getInstance();

  const { id } = req.params;
  const data = req.body

  const answer = await productsService.update(id, data);

  res.json(answer);
}

async function deleteProduct(req, res) {
  const productsService = await ProductsService.getInstance();

  const { id } = req.params;

  const answer = await productsService.delete(id);

  res.json(answer);
}

module.exports = router;
```

# 4. Middlewares

## ¿Qué son los Middlewares?

Middleware es software que permite uno o más tipos de comunicación o conectividad entre dos o más aplicaciones o componentes de aplicaciones en una red distribuida. Al facilitar la conexión de aplicaciones que no fueron diseñadas para conectarse entre sí, y al brindar funcionalidad para conectarlas de manera inteligente, el middleware agiliza el desarrollo de aplicaciones y acelera el tiempo de comercialización.

Un punto a tener presente es que todo código que se encuentre después de `res.send('end')` se ejecuta común y corriente (ver ejemplo).

```javascript
function example(req, res, next) {
  res.send('end');
  console.log('Hola a todos'); // Se imprime en consola el mensaje
  res.send('Bye'); // Esto provoca un error en ejecución
}
```

Por tanto, es recomendable validar los flujos de ejecución usando condicionales, o indicando una sentencia `return` para finalizar la ejecución de una función.

```javascript
function example(req, res, next) {
  if (!condition1) {
    res.send('Error en condition1');
    return;
  }

  if (condition2) {
    res.send('Error en condition2');
  } else {
    res.send('Execution finished');
  }
}
```

## Middleware para HttpErrors

> 500 Internal Server Error

`Error.handler.js`

```js
function logErrors (err, req, res, next) {
  console.log('logErrors');
  console.error(err);
  next(err);
}

function errorHandler(err, req, res, next) {
  console.log('errorHandler');
  res.status(500).json({
    message: err.message,
    stack: err.stack,
  });
}


module.exports = { logErrors, errorHandler }
```

`products.ruoter.js`

```js
const express = require('express');
const routerApi = require('./routes');

const { logErrors, errorHandler } = require('./middlewares/error.handler');

const app = express();
const port = 3000;

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hola mi server en express');
});

app.get('/nueva-ruta', (req, res) => {
  res.send('Hola, soy una nueva ruta');
});

routerApi(app);

app.use(logErrors);
app.use(errorHandler);


app.listen(port, () => {
  console.log('Mi port' +  port);
});
```

## Manejo de errores con Boom

### **buscar la documentación de alguna librería de una manera mas fácil solo escribe**

Install `boom`

```sh
npm i @hapi/boom
```

Documentacion de boom:

```sh

npm docs <nombre de la libreria>
```

[Acá me abrió la doc de boom](https://github.com/hapijs/boom#readme)

```sh
npm docs boom
```

```sh
➜ npm i @hapi/boom
```

### [ boom.](https://hapi.dev/module/boom/api?v=9.1.4)

## Validacion de datos con Joi

Documentacion

```sh
npm docs joi
```

Instalar 

```sh
npm i joi
```

`product.schema.js`

```js
const Joi = require('joi');

const id = Joi.string().uuid();
const name = Joi.string().alphanum().min(3).max(15);
const price = Joi.number().integer().min(10);

const createProductSchema = Joi.object({
  name: name.required(),
  price: price.required(),
});
const updateProductSchema = Joi.object({
  name: name,
  price: price,
});

const getProductSchema =  Joi.object({
  id: id.required(),
});

module.exports = { createProductSchema, updateProductSchema, getProductSchema }
```

`validator.handler.js`

```js
const boom = require('@hapi/boom');

function validatorHandler(schema, property) {
  return (req, res, next) => {
    const data = req[property];
    const { error } = schema.validate(data);
    if (error) {
      next(boom.badRequest(error));
    }
    next();
  }
}

module.export = validatorHandler;
```



## Probando nuestros endpoints

Error en los endpoints, corrections.

## Middlewares populares en Express.js

A continuación te compartiré una lista de los *middlewares* más populares en Express.

#### CORS

*Middleware* para habilitar CORS (Cross-origin resource sharing) en nuestras rutas o aplicación. [http://expressjs.com/en/resources/middleware/cors.html](https://expressjs.com/en/resources/middleware/cors.html)

#### Morgan

Un *logger* de solicitudes HTTP para Node.js. [http://expressjs.com/en/resources/middleware/morgan.html](https://expressjs.com/en/resources/middleware/morgan.html)

#### Helmet

Helmet nos ayuda a proteger nuestras aplicaciones Express configurando varios encabezados HTTP. ¡No es a prueba de balas de plata, pero puede ayudar! https://github.com/helmetjs/helmet

#### Express Debug

Nos permite hacer *debugging* de nuestras aplicaciones en Express mediante el uso de un *toolbar* en la pagina cuando las estamos desarrollando. https://github.com/devoidfury/express-debug

#### Express Slash

Este *middleware* nos permite evitar preocuparnos por escribir las rutas con o sin *slash* al final de ellas. https://github.com/ericf/express-slash

#### Passport

Passport es un *middleware* que nos permite establecer diferentes estrategias de autenticación a nuestras aplicaciones. https://github.com/jaredhanson/passport

> Puedes encontrar más *middlewares* populares en el siguiente enlace: [http://expressjs.com/en/resources/middleware.html](https://expressjs.com/en/resources/middleware.html)

# 5. Deployment

## Consideraciones para producción

Documentación para habilitar CORS en todos los requests la solución sería añadir:

```javascript
const cors = require('cors');
app.use(cors());
```

Si solo queremos hacer CORS a los endpoints de nuestra API, bajamos `app.use(cors())` justo antes de que empiecen nuestras rutas hacia la API. (Esa sería mi solución)

Existen distintas soluciones.

Podemos agregar el cors como middleware a la ruta:

```js
const cors = require('cors');

app.get('/products/:id', cors(), function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for a Single Route'})
})
```

O también podemos agregarlo como un middleware general.

```js
const cors = require('cors');
app.use(cors());
```

Recuerda que también podemos configurar algunas opciones:

```js
const cors = require('cors');

var corsOptions = {
  origin: 'https://example.com',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions ));
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/favicon-48x48.97046865.png)Cross-Origin Resource Sharing (CORS) - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

## Problema de CORS

Instalar `cors`

```sh
npm i cors
```

Limitar el alcance para usuarios

```js
const whitelist = ['http://localhost:8080', 'https://myapp.co'];
```

## Deployment a Heroku

Instalar Heroku.
[enlce de Instalación Windows, macOS y Ubuntu 16+](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)

Instalar Heroku

```sh
curl https://cli-assets.heroku.com/install.sh | sh
```

```sh
heroku login
heroku create 

git remote -v

# comprobar en local que funcione nuestra aplicacion
heroku local web
```

# 6. Próximos pasos

## Continúa en el Curso de Node.js con PostgreSQL

Nunca pares de Aprender!