<h1>Fundamentos de Node.js</h1>

<h3>Carlos Hernández</h3>

<h1>Tabla de Contenido</h1>

- [1. Conocer los conceptos básicos de NodeJS](#1-conocer-los-conceptos-básicos-de-nodejs)
  - [Instalación de Node.js](#instalación-de-nodejs)
  - [Node: orígenes y filosofía](#node-orígenes-y-filosofía)
  - [EventLoop: asíncrona por diseño](#eventloop-asíncrona-por-diseño)
  - [Monohilo: implicaciones en diseño y seguridad](#monohilo-implicaciones-en-diseño-y-seguridad)
  - [Variables de entorno](#variables-de-entorno)
  - [Herramientas para ser más felices: Nodemon y PM2](#herramientas-para-ser-más-felices-nodemon-y-pm2)
- [2. Cómo manejar la asincronía](#2-cómo-manejar-la-asincronía)
  - [Callbacks](#callbacks)
  - [Callback Hell: refactorizar o sufrir](#callback-hell-refactorizar-o-sufrir)
  - [Promesas](#promesas)
  - [Async/await](#asyncawait)
- [3. Entender los módulos del core](#3-entender-los-módulos-del-core)
  - [Globals](#globals)
  - [File system](#file-system)
  - [Console](#console)
  - [Errores (try / catch)](#errores-try--catch)
  - [Procesos hijo](#procesos-hijo)
  - [Módulos nativos en C++](#módulos-nativos-en-c)
  - [HTTP](#http)
  - [OS](#os)
  - [Process](#process)
- [4. Utilizar los módulos y paquetes externos](#4-utilizar-los-módulos-y-paquetes-externos)
  - [Gestión de paquetes: NPM y package.json](#gestión-de-paquetes-npm-y-packagejson)
  - [Construyendo módulos: Require e Import](#construyendo-módulos-require-e-import)
  - [Módulos útiles](#módulos-útiles)
  - [Datos almacenados vs en memoria](#datos-almacenados-vs-en-memoria)
  - [Buffers](#buffers)
  - [Streams](#streams)
- [5. Conocer trucos que no quieren que sepas](#5-conocer-trucos-que-no-quieren-que-sepas)
  - [Benchmarking (console time y timeEnd)](#benchmarking-console-time-y-timeend)
  - [Debugger](#debugger)
  - [Error First Callbacks](#error-first-callbacks)
- [6. Manejar herramientas con Node](#6-manejar-herramientas-con-node)
  - [Scraping](#scraping)
  - [Automatización de procesos](#automatización-de-procesos)
  - [Aplicaciones de escritorio](#aplicaciones-de-escritorio)

# 1. Conocer los conceptos básicos de NodeJS


## Instalación de Node.js


## Node: orígenes y filosofía


## EventLoop: asíncrona por diseño


## Monohilo: implicaciones en diseño y seguridad


## Variables de entorno


## Herramientas para ser más felices: Nodemon y PM2

# 2. Cómo manejar la asincronía


## Callbacks


## Callback Hell: refactorizar o sufrir


## Promesas


## Async/await

# 3. Entender los módulos del core


## Globals


## File system


## Console


## Errores (try / catch)


## Procesos hijo


## Módulos nativos en C++


## HTTP


## OS


## Process

# 4. Utilizar los módulos y paquetes externos


## Gestión de paquetes: NPM y package.json


## Construyendo módulos: Require e Import


## Módulos útiles


## Datos almacenados vs en memoria


## Buffers


## Streams

# 5. Conocer trucos que no quieren que sepas


## Benchmarking (console time y timeEnd)


## Debugger


## Error First Callbacks

# 6. Manejar herramientas con Node


## Scraping


## Automatización de procesos


## Aplicaciones de escritorio