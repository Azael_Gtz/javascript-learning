<h1>Frontend Developer</h1>

<h3>Estefany Aguilar</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Inicia tu camino como frontend developer](#inicia-tu-camino-como-frontend-developer)
  - [¿Qué es HTML y CSS? ¿Para qué sirven?](#qué-es-html-y-css-para-qué-sirven)
  - [Motores de render: de archivos a píxeles](#motores-de-render-de-archivos-a-píxeles)
- [2. Maquetación con HTML](#2-maquetación-con-html)
  - [Anatomía de un documento HTML y sus elementos](#anatomía-de-un-documento-html-y-sus-elementos)
  - [¿Qué es HTML semántico?](#qué-es-html-semántico)
  - [Etiquetas de HTML más usadas](#etiquetas-de-html-más-usadas)
- [3. Maquetación con CSS](#3-maquetación-con-css)
  - [Anatomía de una declaración CSS: selectores, propiedades y valores](#anatomía-de-una-declaración-css-selectores-propiedades-y-valores)
  - [Tipos de selectores: básicos y combinadores](#tipos-de-selectores-básicos-y-combinadores)
  - [Tipos de selectores: pseudoclases y pseudoelementos](#tipos-de-selectores-pseudoclases-y-pseudoelementos)
  - [Cascada y especificidad en CSS](#cascada-y-especificidad-en-css)
  - [Tipos de display más usados: block, inline e inline-block](#tipos-de-display-más-usados-block-inline-e-inline-block)
  - [Tipos de display más usados: flexbox y CSS grid](#tipos-de-display-más-usados-flexbox-y-css-grid)
  - [Modelo de caja](#modelo-de-caja)
  - [Colapso de márgenes](#colapso-de-márgenes)
  - [Posicionamiento en CSS](#posicionamiento-en-css)
  - [Z-index y el contexto de apilamiento](#z-index-y-el-contexto-de-apilamiento)
  - [Propiedades y valores de CSS más usados](#propiedades-y-valores-de-css-más-usados)
- [.4 Diseño responsivo](#4-diseño-responsivo)
  - [Unidades de medida](#unidades-de-medida)
  - [Responsive Design](#responsive-design)
- [5. Arquitectura CSS](#5-arquitectura-css)
  - [¿Qué son las arquitecturas CSS? ¿Para qué sirven?](#qué-son-las-arquitecturas-css-para-qué-sirven)
  - [OOCSS, BEM, SMACSS, ITCSS y Atomic Design](#oocss-bem-smacss-itcss-y-atomic-design)
- [6. Próximos pasos](#6-próximos-pasos)
  - [CSS para entrevistas y mundo laboral](#css-para-entrevistas-y-mundo-laboral)
  - [Continúa con el Curso Práctico de Frontend](#continúa-con-el-curso-práctico-de-frontend)

# 1. Introducción

## Inicia tu camino como frontend developer

[curso-frontend-developer](https://github.com/platzi/curso-frontend-developer)


## ¿Qué es HTML y CSS? ¿Para qué sirven?

HTML: nos ayuda a darle una estructura a nuestra página web.
CSS: Nos ayuda a darle estilos a los componentes de la estructura del HTML.

![progressive-enhancement.png](https://static.platzi.com/media/user_upload/progressive-enhancement-03760340-70f2-493d-8fda-dd4228b7ee30.jpg)

## Motores de render: de archivos a píxeles

Cada navegador implementa su propio motor de renderizado.

**Los 5 pasos de los motores:**

1. Pasa los archivos de HTML a objetos (El DOM). Esto para que el navegador pueda entenderlo.
2. Calcula el estilo correspondiente a cada nodo en el DOM.
3. Calcula las dimensiones de cada nodo y va a empezar a estructurar la página web.
4. Pinta las diferentes cajas.
5. Toma las capas y las convierte en una imagen, para finalmente mostrar esta imagen en la pantalla.

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Redirect Notice](https://www.google.com/url?q=https://hacks.mozilla.org/2017/08/inside-a-super-fast-css-engine-quantum-css-aka-stylo/&sa=D&source=editors&ust=1627399047869000&usg=AOvVaw0GQyUucT2YxttrUdJ3QWvB)

# 2. Maquetación con HTML

## Anatomía de un documento HTML y sus elementos

```html
<h1 class="title">Platzi</h1>
```

- `<h1>`: Etiqueta de apertura
- `</h1>`: Etiqueta de cierre
- `Platzi`: Contenido
- `class`: Atributo
- `"title"`: Valor

Anidamiento: Etiquetas dentro de otras.

Estructura básica de un HTML:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

⚗ [allthetags.com](https://allthetags.com/): *Tabla periódica* de las etiquetas con su descripción y un pequeño ejemplo de uso.

📡 [htmlreference.io](https://htmlreference.io/): Todas las etiquetas con su descripción, su `display` por defecto y ejemplos de uso mas extenso

![cheasheet-tags-html.png](https://static.platzi.com/media/user_upload/cheasheet-tags-html-937e2103-6d4f-4f67-aa16-6f30f6b8c88d.jpg)

## ¿Qué es HTML semántico?

usar html semantico tambien ayuda mucho en la lectura de nuestro código

![divVsSemantic](https://static.platzi.com/media/user_upload/html5_sectioning_high_level-8080ec80-4567-4401-a429-7e7a0ddf26c4.jpg)

## Etiquetas de HTML más usadas

Es así de fácil, simplemente le dices qué quieres poner y HTML lo hará 🥳. Otras etiquetas que también suelen ser muy usadas son estas:

- `<b>`: Pone tu texto en negritas (pero esta etiqueta NO tiene sentido semántico).
- `<strong>`: También pone tu texto en negrita, pero esta SÍ tiene sentido semántico (Google le da relevancia al texto que pongas ahí).
- `<i>`: Pone tu texto en cursivas (pero esta etiqueta NO tiene sentido semántico).
- `<em>`: También pone tu texto en cursivas, pero esta SÍ tiene sentido semántico (Google le da relevancia al texto que pongas ahí).
- `<br>`: Hace un salto de línea, funciona como si diéramos un enter con el teclado 😄.

[![img](https://www.google.com/s2/favicons?domain=https://htmlreference.io/favicons/apple-touch-icon.png?v=201702051021)HTML Reference - A free guide to all HTML elements and attributes.](http://htmlreference.io/)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)44 etiquetas HTML que debes conocer](https://platzi.com/blog/etiquetas-html-debes-conocer/)

# 3. Maquetación con CSS

## Anatomía de una declaración CSS: selectores, propiedades y valores

###  clase

```css
h1 {
	color: pink;
}
```

- `h1`: selector
- `color`: propiedad
- `pink;`: valor

La anatomiía de CSS es:

```css
Selector {
//aqui declaramos los pares de propiedad - valor
color: pink;
//color es propiedad y su valor es pink
}
```

## Tipos de selectores: básicos y combinadores

**Básicos**

- De tipo: `div {...}`
- De clase: `.elemento {...}`
- De ID: `#id-del-elemento`
- De atributo: `a[href="..."]{...}`
- universal: `*{...}`

**Combinadores**

- Descendientes: `div p`
- Hijo directo: `div > p`
- Elemento adyaente: `div + p`
- General de hermanos: `div ~ p`

![sintaxis-avanzada-css.png](https://static.platzi.com/media/user_upload/sintaxis-avanzada-css-e80923a7-69e3-449a-8449-66ac9be75436.jpg)

**Selector ID #** : Se usa para etiquetas contenedoras y no son reutiliazables, pues cuando ponemos un id es algo unico

**Selector Class** : Es el mas usado, por que se puede reutilizar, pues varios elementos puede tener la misma clase

**Selector Universal** * :Se usa muy poco pues este afecta a todos nuestro estilos, solo se usa resetear los estilos que trae el navegador por defecto.

**Combinadores**

```css
//Aplica estilos a todos los p que esten dentro del Div
Descendientes:   div  p { color:red;}

//Aplica el estilo a la etiqueta p que este despues del div padre

Hijo Directo: div > p { color: blue;}

//Aplica estilos a la etiqueta P que este seguida del DIV
Elemento Adyacente: div + p { color: green;}

//Aplica estilos a todos los hermanos P despues de Div

General de Herencia:  div ~ p { color: black}
```

[![img](https://www.google.com/s2/favicons?domain=https://htmlcolorcodes.com/es//assets/images/favicon.png)Códigos de Colores HTML](https://htmlcolorcodes.com/es/)

## Tipos de selectores: pseudoclases y pseudoelementos

Las pseudoclases nos permite llegar a aquellas acciones que hace el usuario.

- `:active`
- `:focus`
- `:hover`
- `:nth-child(n)`

Los pseudoelementos nos permiten acceder a elementos de HTML que no son accesibles con los selectores ya vistos.

- `::after`
- `::before`
- `::first-letter`
- `::placeholder`

> La pseudoclase se escribe con : y los pseudoelementos se escriben con ::

Pseudo-clase es una palabra clave añadido a un selector que especifica un estado especial del elemento seleccionado.

```
:hover
```

#### Pseudoclases lingüísticas

Estas pseudoclases reflejan el idioma del documento y permiten la selección de elementos en función del idioma o la dirección del guión.

```css
:dir
:lang
```

#### Pseudoclases de ubicación

Estas pseudoclases se relacionan con enlaces y elementos específicos dentro del documento actual.

```css
:link 
:visited
```

#### Pseudoclases de acción del usuario

Estas pseudoclases requieren cierta interacción por parte del usuario para que se apliquen, como mantener el puntero del mouse sobre un elemento.

```css
:hover
:focus
```

#### Pseudoclases de dimensión temporal

Estas pseudoclases se aplican cuando se visualiza algo que tiene sincronización, como una pista de subtítulos.

```css
:currrent
:past
:future
```

#### Las pseudoclases de entrada

Estas pseudoclases se relacionan con elementos de formulario y permiten seleccionar elementos basados en atributos HTML y el estado en el que se encuentra el campo antes y después de la interacción.

```css
:autofill
:read-only
:read-write
```

#### Pseudoclases de estructura de árbol

Estas pseudoclases se relacionan con la ubicación de un elemento dentro del árbol del documento.

```css
:root
:empty
```

## Cascada y especificidad en CSS

A un nivel muy simple, esto significa que el orden de las reglas CSS importa; cuando se aplican dos reglas que tienen igual especificidad, la que viene en último lugar en el CSS es la que se utilizará.

#### Especificidad

La especificidad es el medio por el que los navegadores deciden qué valores de propiedades CSS son los más relevantes para un elemento y, por tanto, se aplicarán. La especificidad se basa en las reglas de concordancia que se componen de diferentes tipos de selectores CSS.
La cantidad de especificidad que tiene un selector se mide utilizando cuatro valores diferentes (o componentes), que se pueden considerar como miles, cientos, decenas y unos

**La especifidad**
Es como el navegador va a mostrar el CSS de acuerdo a que tan especifico es el selector que creaste.

Especifidad > Cascada

- **Miles**
  Puntúa uno en esta columna si la declaración está dentro de un atributo style, también conocido como estilos en línea. Estas declaraciones no tienen selectores, por lo que su especificidad es siempre 1000.

- **Cientos**

  Puntúa uno en esta columna por cada selector de ID contenido dentro del selector general.

- **Decenas**

  Puntúa uno en esta columna por cada selector de clase, selector de atributos o pseudoclase que contenga el selector general.

- **Unos**

  Puntúa uno en esta columna por cada selector de elemento o pseudo-elemento contenido dentro del selector general.

`!important` cambia la forma en que la cascada funciona normalmente, por lo que puede hacer que la depuración de los problemas de CSS sea realmente difícil de resolver, especialmente en una hoja de estilos grande.

**No lo uses si puedes evitarlo.**

La única manera de anular esta declaración `!important` sería incluir otra declaración !important en una declaración con la misma especificidad más adelante en el orden de las fuentes, o una con mayor especificidad.

[Specifficity](https://specificity.keegan.st/)

## Tipos de display más usados: block, inline e inline-block

#### Display

La propiedad CSS display establece si un elemento se trata como un elemento en bloque o en línea y el diseño utilizado para sus hijos, como el flow layout, grid o flex.

#### **Outside<display-outside>**

Estas palabras clave especifican el tipo de visualización exterior del elemento, que es esencialmente su papel en la disposición de flujo.

#### Inside <display-inside>

Estas palabras clave especifican el tipo de visualización interior del elemento, que define el tipo de contexto de formato en el que se presenta su contenido (suponiendo que se trate de un elemento no sustituido).

#### Internal<display-internal>

Some layout models such as table and ruby have a complex internal structure, with several different roles that their children and descendants can fill. This section defines those “internal” display values, which only have meaning within that particular layout mode.

#### Box<display-box>

Estos valores definen si un elemento genera cajas de visualización en absoluto.

#### Legacy<display-legacy>

CSS 2 utilizaba una sintaxis de una sola palabra clave para la propiedad display, que requería palabras clave separadas para las variantes a nivel de bloque y a nivel de línea del mismo modo de diseño.

`display: block; ` indican que el elemento se colocará uno debajo del otro, sin importar el tamaño o el contenido.

`display: inline;` indica que el elemento se posicionará a la derecha del otro.

`display: inline-block;` Ocupan todo el tamaño del contenido y permite posicionar más elementos a la derecha.

## Tipos de display más usados: flexbox y CSS grid

[![img](https://www.google.com/s2/favicons?domain=https://css-tricks.com/snippets/css/a-guide-to-flexbox//favicon.ico)A Complete Guide to Flexbox | CSS-Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[🐸Flexbox froggy](https://flexboxfroggy.com/#es)

[🌾Grid Garden](https://cssgridgarden.com/)

## Modelo de caja

Con la propiedad `box-sizing: border-box;` css busca que todo el elemento mida lo que indicamos en width y height, esto con ayuda del padding y el border, de tal manera que se le resta espacio al content para poder lograr las dimensiones específicadas.

CSS Box Model
El tamaño de lo que se muestra en pantalla estará delimitado por 4 cosas: tamaño de contenido, tamaño de relleno (padding), tamaño del borde y el tamaño del margen.
![model box.PNG](https://static.platzi.com/media/user_upload/model%20box-07332c42-24d4-4b47-a66f-ce974dd4a2a5.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://learn-the-web.algonquindesign.ca/topics/flexbox//favicon.ico)Flexbox · Web Dev Topics · Learn the Web](https://learn-the-web.algonquindesign.ca/topics/flexbox/)

[![img](https://www.google.com/s2/favicons?domain=https://www.joshwcomeau.com/css/rules-of-margin-collapse//_next/static/media/favicon-dd02c73c991090257b7c2ddbb1255af4.png?v=4)The Rules of Margin Collapse](https://www.joshwcomeau.com/css/rules-of-margin-collapse/)

## Colapso de márgenes

En el caso de CSS Grid.

![colapso.png](https://static.platzi.com/media/user_upload/colapso-066a50f6-ddaf-425c-bcf3-4fd591a205b5.jpg)

Entender esto está genial, porque a veces queremos poner márgenes de X cantidad de pixeles, pero luego vemos que entre los elementos también se ponen y acabamos teniendo el doble de márgenes jaja. Con el colapso de márgenes evitamos que se dupliquen.

## Posicionamiento en CSS

```html
<div class="container">
	<div class="item"></div>
</div>
```

```css
.container {
  width: 200px;
  height: 200px;
  background: pink;
  position: relative;
  top: 20px;
}

.item {
  widht: 20px;
  height: 20px;
  bakground: peachpuff;
  position: absolute;
  right: 0;
}
```

`realtive`:  cambia la position.

`absolute`:  

`fixed`:  Position estatica.

`sticky`: Mantiene la position, cuando hacemos scroll. 

## Z-index y el contexto de apilamiento

 z-index en el `.container` y el `.nav`, de tal manera que el `.container` no se sobrepusiera sobre el `nav`. Así me quedo el css:

Le puse un z-index mayor al nav para que estuviera arriba del container-

```css
.container {
            width: 200px;
            height: 200px;
            background: peachpuff;
            position: relative;
            top: 20px;
            z-index: 1;
        }
        .item {
            width: 20px;
            height: 20px;
            background: hotpink;
            position: absolute;
            right: 0;
        }
        nav {
            width: 100%;
            height: 60px;
            background: lightcyan;
            position: fixed;
            z-index: 2;
        }
        ul p{
            position: sticky;
            top: 0;
        }
```

Aquí una captura de como quedo:
![Captura de Pantalla 2021-09-29 a la(s) 11.58.33.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-09-29%20a%20la%28s%29%2011.58.33-4e66b020-8a45-41c3-8966-3c06c29cd200.jpg)

## Propiedades y valores de CSS más usados

![stones.png](https://static.platzi.com/media/user_upload/stones-32e4544d-c819-4756-82f8-7a6079fa5815.jpg)

HTML:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./styl.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Luckiest+Guy&display=swap" rel="stylesheet">
</head>
<body>
    <section>
        <article class="section__article">
            <figure><img src="file:///C:/Users/Usuario%20iTC/Pictures/stones.jpg" alt="Stones"></figure>
            <h1>The Rolling Stones</h1>
            <p>Soon 60 years with us!</p>
        </article>
        
    </section>
</body>
</html>
```

CSS:

```css
* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}
html {
    font-size: 62.5%;
}
section {
    width: 100%;
    min-height: 500px;
    display: flex;
    justify-content: center;
}
.section__article {
    text-align: center;
    margin-top: 40px;
    width: 700px;
    height: 500px;
    background-color: #cdb496; 
    overflow: hidden;
    border-radius: 25px;
}
.section__article p:nth-child(3) {
    color: #a14014;
}
.section__article img {
    width: 100%;
}
.section__article h1 {
    font-size: 4rem;
    font-family: 'Luckiest Guy', cursive;
}
.section__article p {
    font-size: 2rem;
    font-weight: bold;
}
```

[![img](https://www.google.com/s2/favicons?domain=https://cssreference.io/favicons/apple-touch-icon.png?v=201702181118)CSS Reference - A free visual guide to CSS](https://cssreference.io/)

# 4. Diseño responsivo

## Unidades de medida

Las medidas relativas dependen de algo para poder calcular su medida, por ejemplo, los porcentajes dependen del elemento padre, los `em` `rem` dependen del `font-size`, los `vw` y `vh` dependen del tamaño del viewport.

Básicamente esa es la diferencia entre medidas relativas y absolutas, de nuevo, ninguna es mejor que otra y todas tienen sus casos de uso 👀.

Los em toman el tamaño de si padre directo. Los rem siempre tienen la referencia al tamaño de fuente en el root de css.  vh y vw se refieren a todo el espacio y se usan de 0-100vh o 0-100vw.

### Medidas Absolutas

Es la unidad de medida que no va a cambiar sin importar el tamaño de la pantalla.

Una unidad absoluta puede ser los píxeles.

```css
p {
    font-size: 18px;
}
```

### Medidas Relativas

Estás si pueden cambiar en relación con el dispositivo que lo estamos viendo.

Las unidades relativas pueden ser porcentajes, em (elemento), rem (root em o root elemento), max-width, max-height, min-width, min-height. Estas últimas varían basándonos en el tamaño de la pantalla.

```css
main {
    background-color: red;
    width: 100%; /*Unidad relativa*/
    height: 500px;
}
```

### em

EM es un acrónimo de elemento, va a tomar el tamaño de fuente que tenga el padre directo.

Por default un em es 16px que es de la etiqueta `html`.

### Ejemplo

Yo al colocar `1.5em` al tamaño de fuente de un elemento, este elemento pasa a tener un tamaño 50% más grande que la fuente que tiene el padre directo. Esto pasa cada vez que usamos la medida relativa em, los hijos toman como medidas de referencia las medidas que tiene su padre directo y esto puede causar confusiones.

EM no es de las mejores medidas.

### REM

Esta medida relativa siempre va a tener referencia o correlación con el estilo que tenga la etiqueta root o el root de nuestro proyecto, que siempre es nuestra etiqueta `html`.

Por defecto el navegador le da a la etiqueta `html` un font-size de 16px.

Aun que coloquemos textos anidados veremos que no tendremos ningún cambio como pasaba antes con la medida em.

REM es menos confuso que EM.

### Truco para usar rem

Debemos setear nuestra etiqueta `html` de la siguiente forma:

```css
html {
    font-size: 62.5%; 
}
```

Con esto estamos estableciendo un tamaño de 10px al tamaño de fuente.

### vw

Lo que hace esta unidad relativa es que toma como referencia el tamaño del viewport o pantalla.

### vh

Lo que hace esta medida relativa es que toma como referencia el alto del viewport o pantalla.

### Min-width y max-width

Una regla impórtate cuando los usamos es que necesitamos tener un width base que casi siempre es en porcentajes.

Lo que sude con estos dos que limitan el crecimiento del contenedor (max-width) o la reducción del contenedor (min-width). Una vez que llegue al límite que establecemos este contenedor deja de crecer o reducirse.

En este caso el selector crece como máximo hasta los 500 px y se reduce como máximo hasta los 320 px.

```css
selector {
	width: 80%;
	min-width: 320px;
	max-width: 500px;
}
```

### Min-height y max-height

En este caso no necesitamos un height base. Se utiliza para evitar problemas de overflow.

Con min-height estamos diciendo que la altura mínima del contenedor va a ser X, pero si tiene contenido que exceda esa altura mínima que se expanda hasta donde el contenido deje de crecer.

Con max-height limitamos cuanto puede crecer el contenedor a lo alto.

### Buenas prácticas

- Es buena práctica utilizar el rem y em.
- No es buena práctica tener un scroll horizontal en dispositivos mobiles.

### Estilos que siempre debe estar al comienzo de nuestro archivo.css

```css
* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}

html {
    font-size: 62.5%; 
}
```

## Responsive Design

En este artículo hablan sobre [patrones de diseño web adaptables](https://developers.google.com/web/fundamentals/design-and-ux/responsive/patterns) es muy interesante para aplicar el Responsive Web Design.

> Diseño responsivo: Que tu sitio se vea bien en varias medidas de pantalla. Para esto usamos los *media queries*.

```css
@media (min-width: 300px) Desde 300 px de width
@media (max-width: 300px) Hasta 300 px de width
```



## ¿Para qué es responsive design?

Es para que nuestro proyecto web pueda ser multiplataforma, que se pueda ver bien en un smartphone, desde una tablet, iPad y que se vean excelente desde una laptop o computadora de escritorio.

Esto realizándolo siempre con buenas prácticas.

Para empezar debemos saber:

## Media Queries

Con los media queries podemos jugar con el layout, cuando la pantalla del dispositivo sea pequeña el contenido se ve de una forma, pero si está pantalla crece el contenido también lo hace sin perjudicar el diseño.

Podemos cambiar la orientación de los contenedores, podemos cambiar su orden, incluso cambiar las dimensiones.

### Breakpoint

Son la dimensión en el viewport, es decir el width y el height de la pantalla, en donde vamos a generar un cambio.

Este cambio es la forma en la que puedo reposicionar ciertos elementos o redimensionar ciertos contenedores, todo esto para que se vea bien la web app y sin importar el dispositivo en el que se abra.

### min-width

Esto quiere decir que cuando la pantalla sea igual o más grande que el valor que coloquemos, el código que esté adentro del media querie se va a ejecutar.

Pero si la pantalla es mayor a ese min-width habrá otro media querie que aplicará estilos diferentes.

### max-width

Esto quiere decir que cuando la pantalla sea igual o más pequeña que el valor que coloquemos, el código que esté adentro del media querie se va a ejecutar.

## La mejor forma de aplicar media queries

Tiene de nombre Mobile First o Mobile Only.

Esto quiere decir que el proyecto ya debe estar diseñado para dispositivos mobile, ya no debemos preocuparnos por que se vea bien desde una laptop o computadora de escritorio.

El diseño del proyecto va a partir desde un dispositivo mobile y desde ahí va a ir creciendo a los demás dispositivos con mayor pantalla.

Si hacemos lo contrario de ir de una pantalla grande a una más pequeña, esto se llama solamente responsive design y no es lo que estamos buscando.

### Aplicado directo desde CSS con media queries

1. Arriba de los media queries vamos a tener el código base, que es el que está hecho y optimizado para dispositivos mobile.
2. Vamos a generar un breakpoint para realizar ciertos cambios en dispositivos más grandes.
3. Vamos a generar otro breakpoint que va a ser para una tablet o para computadoras con un viewport más pequeño como ser netbooks
4. Luego vamos a generar otro breakpoint que será para computadoras de escritorio, desktop o dispositivos con pantallas más grandes.

#### Orden para aplicar los media queries

Partimos desde los dispositivos más pequeños y terminamos con los dispositivos más grandes.

Si lo hacemos de forma inversa tendremos problemas, ya que como CSS funciona en cascada, nunca se van a aplicar los estilos de los medias queries con un viewport más grande.

Empezamos por:

1. Los celulares o dispositivos mobile.
2. Las tablets.
3. Laptops o computadores de escritorio.

### Aplicado directo desde HTML (la mejor practica)

Este método se utiliza, ya que dependiendo del dispositivo donde esté el usuario va a necesitar un archivo CSS u otro, esto es para evitar que carguen archivos que el usuario no va a necesitar ni usar.

Lo agregamos en el `head`, aquí en vez de ligar un archivo de CSS vamos a ligar más de uno, dependiendo de los dispositivos en los que queramos aplicar los estilos.

#### Estilos enfocados a mobile

```html
<link rel="stylesheet" href="style.css">
```

#### Si tenemos archivos CSS que van a impactar en otros dispositivos con diferente viewport

```html
<link rel="stylesheet" href="tablet.css" media="screen and (min-width: 768px)">
<link rel="stylesheet" href="desktop.css" media="screen and (min-width: 1024px)">
```

Agregamos el atributo `media` cuyo valores va a ser el mínimo que necesitamos para hacer ese breakpoint, que es ese cambio en el layout.

#### Orden final

```html
<link rel="stylesheet" href="style.css"> <!-- Los dispositiivos mobiles -->
<link rel="stylesheet" href="tablet.css" media="screen and (min-width: 768px)">
<link rel="stylesheet" href="desktop.css" media="screen and (min-width: 1024px)">
```

# 5. Arquitectura CSS

## ¿Qué son las arquitecturas CSS? ¿Para qué sirven?

Sirven para mantener un orden y una coherencia durante todo el proyecto. Tiene los siguientes objetivos:

- **Predecibles**: escribir reglas claras.
- **Reutilizable:** no escribir código redundante.
- **Mantenible:** que sea fácil de leer y adaptable a los estándares.
- **Escalable:** que pueda crecer fácilmente sin afectar el rendimiento.

.
Estos objetivos se deben ver reflejadas en buenas practicas que debe conocer todo el equipo involucrado en el proyecto como:

- Establecer reglas
- Explicar la estructura base
- Establecer estándares de codificación
- Evitar largas hojas de estilo
- Documentación

> Las arquitecturas de CSS nos ayudan a ser predecibles y que nuestro código sea más reutilizable, mantenible y escalable.

<img src="https://cssreference.io/images/css-reference-icon.png" alt="CSS Reference icon" style="zoom:25%;" />[<img src="https://cssreference.io/images/css-reference-type.png" alt="CSS Reference type" style="zoom:25%;" />](https://cssreference.io/)

## OOCSS, BEM, SMACSS, ITCSS y Atomic Design

Las siguiente lecturas sirven para profundizar en cada metodología:

- [OOCSS](https://www.smashingmagazine.com/2011/12/an-introduction-to-object-oriented-css-oocss/#top)
- [BEM](https://en.bem.info/methodology/)
- [SMACSS](https://medium.com/@GreenXIII/organize-your-css-smacss-way-89c087db5092)
- [ITCSS](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/)
- [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/)
- [How to organize your CSS with OOCSS, BEM & SMACSS](https://intelygenz.medium.com/how-to-organize-your-css-with-oocss-bem-smacss-a2317fa083a7)

También podemos revisar los Design System de diferentes empresas para guiarnos y observar como implementen estas metodologías. En lo personal me gusta mucho el Design System de [Help Scout](https://style.helpscout.com/visual-elements/).

En este link puedes encontrar varios [Design System en figma](https://www.designsystemsforfigma.com/).

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)Home · stubbornella/oocss Wiki · GitHub](https://github.com/stubbornella/oocss/wiki)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Guía de BEM para CSS | Cohete Falcon 9 de SpaceX](https://platzi.com/blog/bem/)

# 6. Próximos pasos

## CSS para entrevistas y mundo laboral

Aqui les dejo un link a algunas preguntas de [CSS en entrevistas](https://github.com/learning-zone/css-interview-questions)



## Continúa con el Curso Práctico de Frontend

Frontend Developer, pueden tomar estos retos:

#### Para prácticar maquetado

- [PlatziWeb Challenge](https://platzi.com/blog/platzi_web_challenge/)
- [Frontend Mentor](https://www.frontendmentor.io/challenges)
- [Coderbyte](https://coderbyte.com/challenges)
- [Codewell](https://www.codewell.cc/challenges)

#### Para reforzar habilidades

- [CSS Battle](https://cssbattle.dev/) (Mejora tus habilidades en CSS para replicar elmeentos con el menor codigo posible).
- [Codepen.io](https://codepen.io/) (Con variedad de retos que puedes usar para mejorar tus habilidades)

#### Para entrar con javascript

- [30 dias con javascript](https://javascript30.com/)