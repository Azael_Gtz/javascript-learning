var nombre = 'Victor', apellido = 'Juarez';

var nombreEnMayusculas = nombre.toUpperCase(); // Palabra en mayuscula
var apellidoEnMinusculas = apellido.toLowerCase(); // La palabra en minuscula

var primeraLetraDelNombre = nombre.charAt(0) // Primer caracter de la palabra
var cantidadDeLetrasDellNombre = nombre.length // cantidad de caracteres de la palabra

var nombreCompleto = nombre + ' ' + apellido  // contatenar 

var apellidoCompleto = `${apellido.toUpperCase()} ${nombre}` // Interpolacion de variables

var str = nombre.charAt(1) + nombre.charAt(2) // Aceder a una sub varaible
var str1= nombre.substr(-1)  // Acceder a una sub clase de una variable