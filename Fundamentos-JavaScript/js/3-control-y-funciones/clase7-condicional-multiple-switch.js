var signo = prompt('Cual es tu signo?')

switch (signo) {
    //case 'acuario':
  //
    //break
  case 'acuario':
    console.log('Debería tener en cuenta las diferentes señales que se le presenten en el día. Muchas de ellas, contendrán la clave que orientará su futuro próximo.')
    break
  case 'aries':
    console.log('Por más que se sienta presionado por las situaciones que vive, su habilidad le permitirá esquivar cualquier dificultad que se le presente en la jornada.')
    break
  case 'tauro':
    console.log('Aproveche, ya que será una jornada en la que podrá exponer su vitalidad e inteligencia en todo lo que emprenda. No dude y ponga en marcha esos proyectos postergados')
    break
  case 'geminis':
    console.log('"Durante el día, sepa que con su vitalidad y pasión logrará llevar a buen término los proyectos que muchos de su entorno creían irrealizables por usted."')
    break
  case 'cancer':
    console.log('Será una jornada donde podrá luchar para conseguir todo lo que desea hace tiempo y podrá obtenerlo sin inconvenientes. No permita que se escape ninguna oportunidad.')
    break
  default:
    console.log('No es un signo zodiacal valido')
    break
}

  /*
if (signo === 'acuario') {
  console.log('Debería tener en cuenta las diferentes señales que se le presenten en el día. Muchas de ellas, contendrán la clave que orientará su futuro próximo.')
}

if (signo === 'aries') {
  console.log('Por más que se sienta presionado por las situaciones que vive, su habilidad le permitirá esquivar cualquier dificultad que se le presente en la jornada.')
}

if (signo === 'tauro') {
  console.log('Aproveche, ya que será una jornada en la que podrá exponer su vitalidad e inteligencia en todo lo que emprenda. No dude y ponga en marcha esos proyectos postergados')
}

if (signo === 'geminis') {
  console.log('"Durante el día, sepa que con su vitalidad y pasión logrará llevar a buen término los proyectos que muchos de su entorno creían irrealizables por usted."')
}

if (signo === 'cancer') {
  console.log('Será una jornada donde podrá luchar para conseguir todo lo que desea hace tiempo y podrá obtenerlo sin inconvenientes. No permita que se escape ninguna oportunidad.')
}
*/
