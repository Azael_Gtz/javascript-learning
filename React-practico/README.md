<h1>Práctico de React.js</h1>

<h3>Oscar Barajas Tavares</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [¿Qué es React.js?](#qué-es-reactjs)
  - [Conceptos fundamentales de React](#conceptos-fundamentales-de-react)
- [2. Configurando el entorno de desarrollo](#2-configurando-el-entorno-de-desarrollo)
  - [Instalación de React y React DOM](#instalación-de-react-y-react-dom)
  - [Configuración de Webpack y Babel](#configuración-de-webpack-y-babel)
  - [Cambios en tiempo real con Webpack](#cambios-en-tiempo-real-con-webpack)
  - [React con CSS y Sass](#react-con-css-y-sass)
- [3. Maquetación en React](#3-maquetación-en-react)
  - [Transformando HTML y CSS en componentes de React](#transformando-html-y-css-en-componentes-de-react)
  - [Estilos en los componentes de React](#estilos-en-los-componentes-de-react)
- [4. Páginas, rutas y componentes](#4-páginas-rutas-y-componentes)
  - [React Router DOM](#react-router-dom)
  - [Navegación entre rutas](#navegación-entre-rutas)
  - [Header en todas las rutas](#header-en-todas-las-rutas)
  - [¿Qué es Atomic Design?](#qué-es-atomic-design)
  - [Solución al reto: componentes y rutas de React Shop](#solución-al-reto-componentes-y-rutas-de-react-shop)
  - [Tipos de componentes en React: stateful vs. stateless](#tipos-de-componentes-en-react-stateful-vs-stateless)
  - [Imágenes y alias en Webpack](#imágenes-y-alias-en-webpack)
- [5. React Hooks](#5-react-hooks)
  - [React.useState](#reactusestate)
  - [Toggle del menú](#toggle-del-menú)
  - [useEffect y consumo de APIs](#useeffect-y-consumo-de-apis)
  - [Custom Hooks para la tienda](#custom-hooks-para-la-tienda)
  - [useRef y formularios](#useref-y-formularios)
  - [React Context](#react-context)
  - [Completando el carrito de compras](#completando-el-carrito-de-compras)
  - [Orden de compra](#orden-de-compra)
  - [Calculando el precio total](#calculando-el-precio-total)
  - [Eliminando productos del carrito](#eliminando-productos-del-carrito)
- [6. Deploy](#6-deploy)
  - [Cómo comprar tu dominio y conexión con Cloudflare](#cómo-comprar-tu-dominio-y-conexión-con-cloudflare)
  - [Automatizando el despliegue con GitHub Actions](#automatizando-el-despliegue-con-github-actions)
- [7. Próximos pasos](#7-próximos-pasos)
  - [Retos de React.js (bug hunters)](#retos-de-reactjs-bug-hunters)
  - [Continúa con el Curso Práctico de Next.js](#continúa-con-el-curso-práctico-de-nextjs)

# 1. Introducción

## ¿Qué es React.js?

https://es.reactjs.org/docs/getting-started.html (español)
https://reactjs.org/docs/getting-started.html (english)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/react-practico](https://github.com/platzi/react-practico)

## Conceptos fundamentales de React

### JSX

Es la extensión de archivos que se usa en react donde podemos hacer html dentro de js facilitando el uso sacando lo mejor de html css y js.

#### Virual DOM

Es una copia del **DOM real** y lo que hace es compararlo, asi cuando existe algun cambio no se tiene que renderizar toda la pantalla si no solo lo que se cambio mejorando el desempeño de nuestra app, como lo comente antes esto es por que se compara el V**irtual** **DOM** con el **DOM** R**eal** encontrando los cambios

#### Ciclo de vida

Este concepto es ampliamente conocido en la programación, en este curso vamos a conocer cual es el ciclo de vida de los elementos que vamos a crear en react desde que nace, se combina hasta que muere

#### Estado

Esto es fundamental, ya que podemos ver los estados y ver como es el flujo de la información entre componentes a travez de un imputs, botones, interacciones entre otros elementos.

#### **Eventos** 

Los componentes, pueden configurarse con eventos como *onclick* para responder antes ciertas interacciones con el usuario, tal como los haríamos en Html

#### React Hooks

Es otra manera de escribir los componentes con estado, si usar clases. No se pretenden reemplazar, sin embargo, usar funciones para los componentes pueden facilitar el entendimiento de la aplicación.

> JSX: JavaScript + HTML
> Virtual DOM es una copia del DOM.
> El virtual DOM lo que hace es compara entre la copia antes de la actualización y la actualización y una vez que sabe cuáles fueron los cambios, únicamente esos son los que se actualiza en el DOM.

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)DOM vs. Virtual DOM](https://platzi.com/blog/virtual-dom/)

# 2. Configurando el entorno de desarrollo

## Instalación de React y React DOM

**Comandos de inicialización**

1. Creación de la carpeta de trabajo

```bash
mkdir react-shop
cd react-shop
```

2. Inicializar

```bash
git init
npm init
// colocamos datos relevantes al momento de inicializar con git
// name: react-shop
// description: react-eshop
// entry point: src/index.js
// author: nombre <correo>
// license: MIT 
```

3. Instalar dependencias

```bash
npm intall react react-dom
// luego, abrir el proyecto en vscode
```

4. Crear estructura de carpetas del proyecto

```bash
mkdir public
cd public 
touch index.html
cd ..
mkdir src
cd src
touch index.js
mkdir components
cd components
touch App.jsx
```

5. Importación de react y creación de componentes básicos

```jsx
// en index.js
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<App />, document.getElementById('app'));
// en App.jsx
import React from 'react';

const App = () => {
    return (
        <h1>Hola Mundo!</h1>
    );
}

export default App;
```

[<img src="https://dsznajder.gallerycdn.vsassets.io/extensions/dsznajder/es7-react-js-snippets/3.1.1/1615634123388/Microsoft.VisualStudio.Services.Icons.Default" alt="img" style="zoom:10%;" />](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)

[<img src="https://equimper.gallerycdn.vsassets.io/extensions/equimper/react-native-react-redux/2.0.6/1602247317454/Microsoft.VisualStudio.Services.Icons.Default" alt="img" style="zoom:10%;" />](https://marketplace.visualstudio.com/items?itemName=EQuimper.react-native-react-redux)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/react-practico](https://github.com/platzi/react-practico/tree/react/01)

[![img](https://www.google.com/s2/favicons?domain=https://gndx.dev/blog/tema-oh-my-zsh-gndxfavicon.png)Tema personalizado para ZSH](https://gndx.dev/blog/tema-oh-my-zsh-gndx)

[![img](https://www.google.com/s2/favicons?domain=https://docs.github.com/es/repositories/managing-your-repositorys-settings-and-features/customizing-your-repository/licensing-a-repository/assets/images/site/favicon.png)Licensing a repository - GitHub Docs](https://docs.github.com/es/repositories/managing-your-repositorys-settings-and-features/customizing-your-repository/licensing-a-repository)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Reactjs code snippets - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=xabikos.ReactSnippets)

## Configuración de Webpack y Babel

**Comandos NPM**

Babel:

```sh
 npm install @babel/core @babel/preset-env @babel/preset-react 
```

Webpack:

```sh
npm install webpack webpack-cli webpack-dev-server 
```

HTML plugin:

```sh
 npm install babel-loader html-loader html-webpack-plugin
```

**Archivos de configuración**

```jsx
node_modules
// .gitignore
{
    "presets": [
        "@babel/preset-env",
        "@babel/preset-react"
    ]
}
//.babelrc
```

Configuración del archivo webpack.config.js

```js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // ... Configuración de empaquetado
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    resolve: {
        extensions:['.js','.jsx']
    },
    module: {
        // ... Lista de reglas respecto a los loaders	
        rules : [
            // Reglas para babel
            {
                test: /\.(js | jsx)$/,
                use: { loader: 'babel-loader'},
                exclude: /node_modules/
            },
            // Reglas para HTML loader
            {
		test: /\.html$/,
		use: [{ loader: 'html-loader'}]
           }

        ]

    },
    plugins: [
	    //... Configuración de plugins
        new HtmlWebpackPlugin(
		{ 
      		template: './public/index.html', 
		filename: './index.html'   
		}
	)
	]
}
```

## Cambios en tiempo real con Webpack

`css`

```sh
npm install css-loader

npm install  -g sass

npm i mini-css-extract-plugin

 npm install style-loader --save
```

**Cambios en tiempo real con Webpack**

**Ahora** configuraremos webpack de modo que podamos compilar nuestra App, y ver los resultados en una pestaña de nuestro navegador.

```jsx
// en package.json
{
	"scripts" : {
		"start": "webpack serve --open", // compilar para desarrollo
    "build": "webpack --mode production" // compilar para producción
	}
}
// en webpack.config.js
module.exports = {
	...
	mode: 'development',
	...
}
// en index.js nos habíamos olvidado de importar App
import App from './components/App';
//por último, el index.html//
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>React Shop</title>
</head>
<body>
    <div id="app">// en index, escribimos que debería agregarse al elemento con 
//el id "app", por eso creamos un div con dicho id, para poder insertarlo
            
    </div>
</body>
</html>
//ahora, en la terminal corremos el comando
npm run start
// y listo, tenemos nuestra app
```

> Los errores son amigos nuestros, por ello, debemos aprender a leerlos y comprenderlos, para así realizar las correciones correspondientes. Si no, recordemos que siempre podemos googlear 🙂

## React con CSS y Sass

**Instalaremos** algunos loaders y plugins para que nuestro proyecto pueda funcionar con estilos de css y sass

`sass`

```sh
npm instal mini-css-extract-plugin css-loader style-loader sass sass-loader -D
```



```bash
// en el video, faltaba solamente el loader de sass por los que nos dio un error
// como siempre, cada vez que instalamos dichas dependencias, en webpack debemos 
// indicar reglas y los plugins

module.exports = {
	...
		module: {
			rules: [
				...
				{
	         test: /\.s[ac]ss$/i,
           use: [
               "style-loader",
               "css-loader",
               "sass-loader"
	        ]
        }
				...
			]
		}
	...,
	plugins: [
		...
		new MiniCssExtractPlugin({
			filename: '[name].css'
    })
		...
	],
	devServer: {
        allowedHosts: path.join(__dirname, 'dist'), // contentBase corresponde a webpack 4
// ahora en Webpack 5 se usa allowedHosts
// créditos al compañero Fabian Rivera Restrepo
        port: 3005,
        compress: true,
    }
}
// creamos la carpeta styles dentro de src
// en styles, creamos el archivo global.scss

$base-color: #ff0000;
$color: rgb(black, 0.88);

body {
    background-color: $base-color;
    color: $color;
}
// en App.jsx, importamos los estilos de scss
import '../styles/global.scss';
// ahora si podemos compilar otra vez nuestra app
npm run build
```

# 3. Maquetación en React

## Transformando HTML y CSS en componentes de React

1. **Inspeccionemos** el proyecto anterior. En este caso, se nos facilitará transformar el proyecto del curso práctico de frontend a react pues en cada archivo html tenemos una *sección*
2. **Abrimos** el archivo de la clase1.html y copiamos el html principal.
3. **Estructuremos** nuestro proyecto en react. Para ello, creamos una carpeta llamada containers (dentro de src), dónde tendremos estos mismos módulos de cada archivo html, pero transformado en react. Seamos precavidos en cerrar todas las etiquetas html pues nos pueden dar error.

> Diferenciemos *componente* de “*contenedor*” en react. Los contenedores o módulos son partes más grandes. En este caso puede ser todo el login. Sin embargo, un componente puede ser un *input* o un *form*. Los componentes deben mantenerse pequeños y responder a uno sola necesidad, si no, perdemos funcionalidad.

1. **Código en React**

```jsx
import React from 'react';

const Login = () => {
    return (
        <div className="login">
            <div className="form-container">
                <img src="./logos/logo_yard_sale.svg" alt="logo" className="logo" />

                <h1 className="title">Create a new password</h1>
                <p className="subtitle">Enter a new passwrd for yue account</p>

                <form action="/" classNameName="form">
                    <label for="password" classNameName="label">Password</label>
                    <input type="password" id="password" placeholder="*********" classNameName="input input-password" />

                    <label for="new-password" className="label">Password</label>
                    <input type="password" id="new-password" placeholder="*********" className="input input-password" />

                    <input type="submit" value="Confirm" className="primary-button login-button" />
                </form>
            </div>
        </div>
    );
}

export default Login;
```

**Advertencia** => la explicación puede contener errores, pues estaba viendo que los contenedores son más complejos.

[![img](https://www.google.com/s2/favicons?domain=https://static.figma.com/app/icon/1/icon-192.png)Figma](https://www.figma.com/proto/bcEVujIzJj5PNIWwF9pP2w/Platzi_YardSale?node-id=0%3A719&scaling=scale-down&page-id=0%3A1&starting-point-node-id=0%3A719)

[![img](https://www.google.com/s2/favicons?domain=https://static.figma.com/app/icon/1/icon-192.png)Figma](https://www.figma.com/proto/bcEVujIzJj5PNIWwF9pP2w/Platzi_YardSale?node-id=5%3A2808[…]ing=scale-down&page-id=0%3A998&starting-point-node-id=5%3A2808)

## Estilos en los componentes de React

En la documentación de Sass desaconsejan usar **@import** para traer archivos de estilos externos.

```jsx
@import //The Sass team discourages the continued use of the @import rule.
```

En cambio recomiendan usar **@use**, esto evita que variables, mixins, placeholders y funciones sean accesibles de manera global, mejora el tiempo de compilación entre otros problemas que surgen cunado se usa @import

```scss
@use 'path/for/external/styles';
```

Esto se soluciona importando la imagen en nuestro container/Login.jsx asi:

```jsx
import logo from '../../public/assets/logos/logo_yard_sale.svg';
```

e insertándola como una variable en nuestra etiqueta, de esta forma:

```html
<img src={logo} alt='logo' className='logo' />
```

Si estas siguiendo el curso, tendrás un error al intentar leer los .svgs. Para solucionar esto.
1.-necesitamos fili-loader

```sh
npm i -D file-loader
```

2.- Modificamos nuestro webpack agregando una nueva regla.

```jsx
rules: {
	[
	{...},
	{
 	test: /\.(png|jp(e*)g|svg|gif)$/,
       	use: [
          	{
         	loader: 'file-loader',
            	options: {
              		name: 'images/[hash]-[name].[ext]',
            		},
          	},
              ],
	},
	]
}
```

por ultimo matamos el proceso si no lo has hecho, y nuevamente

```sh
npm run start
```

![dots.jpg](https://static.platzi.com/media/user_upload/dots-5a45d0e1-1195-4cef-a59f-7bd2993282e4.jpg)

[Ver documentación](https://sass-lang.com/documentation/at-rules/import)

# 4. Páginas, rutas y componentes

## React Router DOM

Debido a que React es de tipo SPA(single page application), no recarga la página cuando cambiamos de url. Sin embargo, router nos ayuda a crear otra página para poder navegar en nuestra aplicación. Imagina twitter web, cuando das click en un tweet, se abre otra sección donde puedes ver el tweet. *Sería un problema que al momento de darle click, no cambie la url, por lo que ese tweet no tiene dirección propia, no se guardaría en tu historial y sería un problema el SEO.* Para ello, usamos router, que se encargará de administrar esta situación, donde en el momento que abras el tweet, cambie la URL, pero todavía mantenga ese dinamismo y rapidez de una SPA.

#### ¿Entonces qué es ReactRouterDOM?

```bash
#Para instalar
npm install react-router-dom
```

**Importar**

```jsx
//import en App.jsx
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// usaremos esas 3 herramientas
```

**ReactRouterDOM** te permite implementar enrutado dinámico en la aplicación. Nos facilita pues podemos enrutar nuestra app basada en componentes de la app (como login o recoverypassword).

```jsx
const App = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Layout>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="recovery-password" component={Recoverypassword} />
                    <Route component={NotFound} />
                </Layout>
            </Switch>
        </BrowserRouter>
    );
}
```

#### ¿Qué estamos haciendo?

**BrowserRoute** sirve para implementar *router* en el navegador

**Switch** regresa la primera ruta que coincida. En pocas palabras, si estamos en *[www.platzi.com/contacto](https://platzi.com/clases/2484-react-practico/42074-react-router-dom/platzi.com/contacto)* , regresará el componente que coincida a este (es decir, el componente que contenga la lógica de contacto). En esta caso, estamos poniendo varios *routes dentro de switch, ¿para qué?* para que solamente traiga esa misma ruta, y no tenga que buscar más. Como si fuese un condicional switch de javascript efectivamente. Y por ello tenemos un route sin path, que será el valor por defecto.

**Layout** solamente renderizará el route que coincida efectivamente con la URL especificada.

- BrowserRouter: Este componente es el encargado de envolver nuestra aplicación dándonos acceso al API historial de HTML5 (pushState, replaceState y el evento popstate) para mantener su UI sincronizada con la URL.

- Switch: Este componente es el encargado de que solo se renderice el primer hijo Route o Redirect que coincide con la ubicación. Si no usar este componente todos los componentes Route o Redirect se van a renderizar mientras cumplan con la condición establecida.

- Route: Con Route podemos definir las rutas de nuestra aplicación, quizás sea el componente más importante de React Router para llegar a comprender todo el manejo de esta librería. Cuando definimos una ruta con Route le indicamos que componente debe renderizar.

  Este componente cuanta con algunas propiedades:

  - Path: la ruta donde debemos renderizar nuestro componente podemos pasar un string o un array de string.
  - Exact: Solo vamos a mostrar nuestro componente cuando la ruta sea exacta. Ej: /home === /home.
  - Strict: Solo vamos a mostrar nuestro componente si al final de la ruta tiene un slash. Ej: /home/ === /home/
  - Sensitive: Si le pasamos true vamos a tener en cuenta las mayúsculas y las minúsculas de nuestras rutas. Ej: /Home === /Home
  - Component: Le pasamos un componente para renderizar solo cuando la ubicación coincide. En este caso el componente se monta y se desmonta no se actualiza.
  - Render: Le pasamos una función para montar el componente en línea.

[**Crea Rutas con react-router**](https://johnserrano.co/blog/aprende-a-crear-rutas-con-react-router)

Responder

[![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org/docs/react-dom.html/favicon.ico)ReactDOM – React](https://es.reactjs.org/docs/react-dom.html)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/react-practico at react/07](https://github.com/platzi/react-practico/tree/react/07)

## Navegación entre rutas

Utilizando Link de react-router-dom

```jsx
import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/NotFound.scss';

export const NotFound = () => {
   return (
      <div className="NotFound">
         <img
            src="https://i.pinimg.com/originals/86/41/80/86418032b715698a4dfa6684b50c12af.gif"
            alt=""
         />
         <Link to="/">
            <h1>Return to Home</h1>
         </Link>
      </div>
   );
};
```

![ítulo.png](https://static.platzi.com/media/user_upload/%C3%ADtulo-c86834e3-4da8-4cdb-9ecb-ca5da639dc24.jpg)

 simple
![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-6863c226-1b57-4a40-9450-45ebfd0f668f.jpg)

NotFound.jsx

```jsx
        <div>
            <h1 className="title-404">404</h1>
            <p className="text-404">Oops! Pagina no encontrada.</p>
            <a className="button-404" href="#">Regrese a la página inicial, es mejor.</a>
        </div>
```

Además recuerden realizar la importación del scss

```jsx
import '../styles/NotFound.scss'
```

NotFound.scss

```scss
@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,300,500);

.button-404 {
  font-weight: 300;
  background-color:var(--hospital-green);
  color: var(--white);
  font-size: 1.2em;
  text-decoration: none;
  border: 1px solid #efefef;
  padding: .5em;
  border-radius: 3px;
  float: left;
  margin: 0 0 0 -168px;
  left: 50%;
  position: relative;
  transition: all .3s linear;
}

.text-404 {
  font-size: 2em;
  text-align: center;
  font-weight: 100;
}

.title-404 {
  margin-top: 0.2em;
  margin-bottom: 0.2em;
  text-align: center;
  font-size: 10em;
  font-weight: 100;
}
```

## Header en todas las rutas

**Header para todas nuestras páginas**

**Primero**, seleccionemos el html del proyecto anterior y construyamos un componente.

```jsx
// Header.jsx
import React from 'react';
import '../styles/Header.scss'

const Header = () => {
    return (
        <nav>
            <img src="./icons/icon_menu.svg" alt="menu" className="menu" />
            <div className="navbar-left">
                <img src="./logos/logo_yard_sale.svg" alt="logo" className="logo" />
                <ul>
                    <li>
                        <a href="/">All</a>
                    </li>
                    <li>
                        <a href="/">Clothes</a>
                    </li>
                    <li>
                        <a href="/">Electronics</a>
                    </li>
                    <li>
                        <a href="/">Furnitures</a>
                    </li>
                    <li>
                        <a href="/">Toys</a>
                    </li>
                    <li>
                        <a href="/">Others</a>
                    </li>
                </ul>
            </div>
            <div className="navbar-right">
                <ul>
                    <li className="navbar-email">platzi@example.com</li>
                    <li className="navbar-shopping-cart">
                        <img src="./icons/icon_shopping_cart.svg" alt="shopping cart" />
                        <div>2</div>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Header;
```

> Recordemos que debemos cambiar class del html por className, para evitar problemas.

**Segundo**, traigamos el css, y creamos el archivo scss.

```scss
// Header.scss
nav {
    display: flex;
    justify-content: space-between;
    padding: 0 24px;
    border-bottom: 1px solid var(--very-light-pink);
}
.menu {
    display: none;
}
.logo {
    width: 100px;
}
.navbar-left ul,
.navbar-right ul {
    list-style: none;
    padding: 0;
    margin: 0;
    display: flex;
    align-items: center;
    height: 60px;
}
.navbar-left {
    display: flex;
}
.navbar-left ul {
    margin-left: 12px;
}
.navbar-left ul li a,
.navbar-right ul li a {
    text-decoration: none;
    color: var(--very-light-pink);
    border: 1px solid var(--white);
    padding: 8px;
    border-radius: 8px;
}
.navbar-left ul li a:hover,
.navbar-right ul li a:hover {
    border: 1px solid var(--hospital-green);
    color: var(--hospital-green);
}
.navbar-email {
    color: var(--very-light-pink);
    font-size: var(--sm);
    margin-right: 12px;
}
.navbar-shopping-cart {
    position: relative;
}
.navbar-shopping-cart div {
    width: 16px;
    height: 16px;
    background-color: var(--hospital-green);
    border-radius: 50%;
    font-size: var(--sm);
    font-weight: bold;
    position: absolute;
    top: -6px;
    right: -3px;
    display: flex;
    justify-content: center;
    align-items: center;
}
@media (max-width: 640px) {
    .menu {
    display: block;
    }
    .navbar-left ul {
    display: none;
    }
    .navbar-email {
    display: none;
    }
}
```

**Por último,** incorporemos dicho header en el home del proyecto.

```jsx
// Home.jsx
import React from 'react';
import Header from '../components/Header';

const Home = () => {
    return (
        <Header />
    );
}

export default Home;
```

Y listo, ya tenemos nuestro header en la página Home.

> *Reto: analizar todos los componentes, containers y páginas del proyecto para terminar toda la estructura final. Es importante que aprendamos a diferenciar cada uno, pues nuestro objetivo siempre será crear el código más reutlizable y fácil de mantener.*

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/react-practico at react/09](https://github.com/platzi/react-practico/tree/react/09)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)ES7 React/Redux/GraphQL/React-Native snippets - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)

## ¿Qué es Atomic Design?

![https://atomicdesign.bradfrost.com/images/content/atomic-design-process.png](https://atomicdesign.bradfrost.com/images/content/atomic-design-process.png)

Piensa en una aplicación. Identifica cada parte, navega por ella, cambia de sección. Te das cuenta? muchos componentes son muy parecidos. Conoce a atomic design, una metodología encargada en interfaces.

**Átomos**

Ahora necesito que recuerdes las clases de química. Sabrás que todo en el universo está compuesto por átomos. Este es nuestro primer nivel de abstracción. Cuando diseñes un UI, mira los botones, textos, imágenes o entradas de texto. Son las partes más fundamentales y pequeñas que usamos.

![https://atomicdesign.bradfrost.com/images/content/html-periodic-table.png](https://atomicdesign.bradfrost.com/images/content/html-periodic-table.png)

La **imágen** de arriba te ayudará a identificar que cosas pueden tomarse como átomos en tu próxima aplicación.

![https://atomicdesign.bradfrost.com/images/content/atoms-form-elements.png](https://atomicdesign.bradfrost.com/images/content/atoms-form-elements.png)

**Moléculas**

Las moléculas son una unión de átomos. Todas estas moléculas, normalmente tienen una función específica para la cuál necesitan varios átomos. Por ejemplo, la glucosa C6H12O6, es la energía en carbohidratos del humanos. Ahora, pasemos al diseño. En interfaces, una parte como un comentario de twitter, una sección de youtube de ME GUSTA y NO ME GUSTA, o el menú en los videos de platzi para avanzar o retroceder en la clase, son todos moléculas. Estas estás compuestas de algunos componentes más pequeños (como por ejemplo, de botón y cuadro de texto). Este es nuestro segundo nivel. Crear moléculas es simple, y recuerda que deberán tener una función única en nuestra UI

![https://atomicdesign.bradfrost.com/images/content/molecule-search-form.png](https://atomicdesign.bradfrost.com/images/content/molecule-search-form.png)

**Organismos**

Los organismos, ya son un nivel mucho más complejo. Los organismo están compuesto de muchas moléculas. Pero lo más interesante, es que tienen vida propia, y pueden interactuar en una manera muy amplia con otros organismos. Imagina una abeja con una flor, ambos colaboran de una u otra manera a que el otro esté bien. En nuestro diseño, imagina al header. El header está compuesto de muchos elementos, y tienen un impacto muy grande en la app. O incluso, de una sección como una tienda de ropa en la paǵina web. Seguramente te das cuenta, que estos tienen muchos artículos, y todos constan de una imaǵen, precio, y un ordenamiento. Puedes verlo así:

*Átomo⇒ imágen, precio, descripción*

*Molécula ⇒ el cuadro que contiene a la imágen, al precio y a la descripción.*

*Organismo ⇒ todos los cuadros ordenados en forma de tabla.*

El organismo si te das cuenta, puede usar moléculas del mismo tipo o diferentes. El punto clave, es que no trates de abarcar tanto, y que pertenecen a una sección claramente definida en nuestra app.

![https://atomicdesign.bradfrost.com/images/content/organism-header.png](https://atomicdesign.bradfrost.com/images/content/organism-header.png)

**Templates**

Los templates son prácticamente lo que vimos de Layouts. Es un poco más fácil de comprender. Es la plantilla en la cual siempre organizarás los organismos. Es decir, el esqueleto que indica donde irá por ejemplo, el Header, el footer, grid y sección de comentarios.

![https://atomicdesign.bradfrost.com/images/content/template.png](https://atomicdesign.bradfrost.com/images/content/template.png)

**Pages**

Finalmente tenemos a la constitución de nuestra app. Las pages son en sí, toda la página funcionando con contenido interactúando entre ellas.

![https://atomicdesign.bradfrost.com/images/content/page.png](https://atomicdesign.bradfrost.com/images/content/page.png)

> Una recomendación. No pienses en forma secuencial el atomic design. Es decir, no pienses ⇒ primero hago los átomos, después hago las moléculas, tercero los organismos… Según el mismo autor de atomic design, dependerá mucho de tu aplicación y de las necesidades que hay que cubrir. Más bien, es una manera mental de interpretar la UI

> No atribuyas atomic design como algo único de React o del desarrollo web ⇒ es un método de desarrollo de UI que se puede usar en cualquier interfaz.

Te recomiendo profundamente leer el siguiente link, del cual usé toda la referencia. Además, es del autor del Atomic Design.

[Atomic Design Methodology | Atomic Design by Brad Frost](https://atomicdesign.bradfrost.com/chapter-2/)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)¿Por qué usar Atomic Design?](https://platzi.com/blog/por-que-atomic-design/)

## Solución al reto: componentes y rutas de React Shop

**Components**: pieza más pequeño (átomo).
**Containers**: Muestran la unión de uno o más componentes.
**Pages**: Son las secciones / rutas que vamos a tener.

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Guía de BEM para CSS | Cohete Falcon 9 de SpaceX](https://platzi.com/blog/bem/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/react-practico at react/challenge](https://github.com/platzi/react-practico/tree/react/challenge)

## Tipos de componentes en React: stateful vs. stateless

Este es un ejemplo para useState, podemos darle un valor inicial, el cual puede ser cambiado por un evento que se puede asignar a este mismo componente, o a otros componentes y hasta pasarlo por medio de un hijo para que éste cambie el inicial.

Para poder usar componentes stateful es necesario llamar useState desde React, la forma de importarlo y usarlo es la siguiente:

```jsx
import React, { useState } from 'react';

const Button = () => {
    const [name, setName] = useState('Hola Mundo'); 
    return (
        <div>
            <h1>{name}</h1>
        </div>
    );
}
```

Los componentes stateless servirán para pasar un estilo visual o props, pero no tendrá otra función más que esa.

Este sería un componente sin estado, stateless.

```jsx
import React from 'react';

const Button = ({ text }) => <ButtonRed text={text}/>;
```

También esta forma es válida:

```jsx
import React from 'react';

const Button = () => (
        <div>
            <h1>Hola mundo!</h1>
        </div>
);
```

Es por eso que hay que tener presente que NO todos los componentes deben de tener estado y muchos de ellos sólo llevarán información que presentar directamente al HTML con CSS, pero sí serán parte de todo lo que se está construyendo.

Los componentes Stateful y Stateless, son los componentes más utilizados hoy en día.

También hay otro tipos de componentes, que están compuestos por clases.

Aquí, tendremos una clase, con el nombre que queramos, que *extiende* de React.Component

```jsx
import React from 'react';

class App extends React.Component {
    render() {
        return (
            <div>
                <h1>Hello world! </h1>
            </div>
        )
    }
}
```

Aunque, si importamos React Component, desde un inicio, podemos simplemente escribirlo de esta forma:

```jsx
import React , { Component } from 'react';

class App extends Component {
    render() {
        return (
            <div>
                <h1>Hello world! </h1>
            </div>
        )
    }
}
```

Este tipo de componentes trabajan con constructores, aunque ya no son tan usados, pues han sido reemplazados por la propuesta de React Hooks.

```jsx
import React , { Component } from 'react';

class App extends Component {

    constructor() {
        super();
        this.sate = {
            count: 0
        };
    }

    render() {
        return (
            <div>
                <h1>Hello world! </h1>
            </div>
        )
    }
}
```

Es importante conocer este tipo de componentes porque si en algún momento tenemos que dar mantenimiento a alguna página que fue construida hace unos años atrás, es muy posible que nos encontremos este tipo de componentes.

Los hooks, tienen una funcionalidad particular, pues reciben un componente, extienden su funcionalidad con lo que esté dentro del componente y retornan un componente compuesto. Así podemos tener funcionalidades muy específicas con las que podemos trabajar según nuestras necesidades.

Esta sería la sintaxis:

```jsx
import React , { Component } from 'react';

function ComponentWrapper(WrapperComponent) {
    class Wrapper extends Component {
        render () {
            return <WrapperComponent {...this.props} />;
        }
    }

    return Wrapper;
}
```

Más adelante aprenderemos más sobre React Context y cómo usarlo.

En react hay 4 tipos principales de componentes en React.

> Props ⇒ propiedades que le pasamos al componente la cual reciben del componente padre en forma de prop. Hacen a los componentes re utilizables (Recibe datos)
> State ⇒ El estado del componente del cual no puede ser accedido ni modificado por fuera de la aplicación. Es un equivalente a variables locales. (Administra datos)

#### Statefull

```jsx
import React, { useState } from 'react';

const Button = () => {
    const [name, setName] = useState('Hola Mundo'); 
    return (
        <div>
            <h1>{name}</h1>
        </div>
    );
}
```

Los componentes statefull son comúnmente usados con state obviamente. *¿Por qué? ⇒* el modelo de react indica que solo puede cambiar si otro componente hijo cambia. Es decir, desde adentro puede cambiar y no por afuera. Imagina el modelo MVC en el cual un componente causa que otro cambie (de manera simplificada).

> Modificar el estado ⇒ setState()
> Cuando hay una modificación de estado, todos los otros componentes se actualizan

#### Stateless

```jsx
import React from 'react';

const Button = () => (
        <div>
            <h1>Hola mundo!</h1>
        </div>
);
// o también
import React from 'react';

const Button = ({ text }) => <ButtonRed text={text}/>;
```

Estos son usados normalmente con props. *¿Por qué? ⇒* porque siempre van a mantener la misma información, y no va a cambiar sin importar lo que pase. Por ejemplo un botón que siempre nos lleve a un link externo. No interactúa con los otros componentes de React.

> No todo debe llevar estado, aprendamos a usar componentes sin estado. Además afectan el rendimiento de la aplicación.
> Los datos son inmutables en Stateless

#### Componentes clase

```jsx
import React from 'react';

class App extends React.Component {
		constructor() {
        super();
        this.sate = {
            count: 0
        };
    }
// también pueden o no llevar estado
    render() {
        return (
            <div>
                <h1>Hello world! </h1>
            </div>
        )
    }
}
```

Son la manera anterior a los React Hooks en la cual usábamos los componentes de React

#### HOC (high order components)

Reciben un componente, y los adaptan de otra manera para usarlos con “modificaciones”. Es como el patrón decorator. Suelen regresar un componente que modifica la lógica del componente anterior

```jsx
import React , { Component } from 'react';

		function ComponentWrapper(WrapperComponent) {
	    class Wrapper extends Component {
        render () {
            return <WrapperComponent {...this.props} />;
        }
    }

    return Wrapper;
}
```

## Imágenes y alias en Webpack

alias 😄

```js
    alias: {
      '@components': path.resolve(__dirname, 'src/components/'),
      '@containers': path.resolve(__dirname, 'src/containers/'),
      '@pages': path.resolve(__dirname, 'src/pages/'),
      '@styles': path.resolve(__dirname, 'sass/'),
      '@icons': path.resolve(__dirname, 'src/assets/icons/'),
      '@logos': path.resolve(__dirname, 'src/assets/logos/'),
    }
```

**Primero** editemos el webpack.config.js para poder tener imágenes en nuestra Web.

```jsx
module: {
		rules: [
			{
				test: /\.(png|svg|jpg|gif)$/,
				type: 'asset',
			}
		]
	},
// ahora es super sencillo porque webpack 5 por defecto ya puede manejar 
// extensiones de imágenes
```

**Al momento** que queramos usar estas imágenes podemos realizarlo de la siguiente manera

```jsx
// Header.jsx como ejemplo
import menu from '@icons/icon_menu.svg';
import logo from '@logos/logo_yard_sale.svg'
import shoppingCart from '@icons/icon_shopping_cart.svg';

<img src={menu} alt="menu" className="menu" />
<img src={logo} alt="logo" className="nav-logo" />
<img src={shoppingCart} alt="shopping cart" />
```

**Usemos** alias para facilitar la importación y rutas en nuestra aplicación

```jsx
module.exports = {
	resolve: {		
		alias: {
			'@components': path.resolve(__dirname, 'src/components/'),
			'@containers': path.resolve(__dirname, 'src/containers/'),
			'@pages': path.resolve(__dirname, 'src/pages/'),
      '@styles': path.resolve(__dirname, 'src/styles/'),
			'@icons': path.resolve(__dirname, 'src/assets/icons/'),
			'@logos': path.resolve(__dirname, 'src/assets/logos/'),
			'@routes': path.resolve(__dirname, 'src/routes/'),
		}
	}
}
```

# 5. React Hooks

## React.useState

useState es una manera de usar estado con los React Hooks. Recordemos que los estados son maneras en la que un componente puede administrar información cambiante en el entorno, y después de ser llamado se renderiza el React DOM de nuevo.

Para ello primero importamos useState de react

```jsx
import React, { useState } from 'react';
```

**Ahora**, useState será incorporado en nuestro componente ProductItem

```jsx
const ProductItem = () => {
	const [cart, setCart] = useState('Hola');

	const handleClick = () => {
		setCart('Hola mundo');
	}
}
```

Para poder usar los estados, primero debemos crear una constante en la cual tendrá un array. El primero elemento *en este caso cart* será la variable a la cual le asignaremos un valor de estado. Este valor puede ser de cualquier tipo. En segundo lugar tenemos a setCart. Por convención siempre deberemos escribir esta “función” con set(Variable). Esta será la encargada de asignar un valor cualquiera a cart cada vez que exista algún evento. Esto lo igualamos a *useState*, que es como una manera de inicializar la variable cart. En segundo lugar tenemos a la función handleClick. **handleClick** es la función que dada un evento, *como un click,* será llamada y por dentro usaremos a setCart para asignarle un nuevo estado a la variable cart. No podemos usar directamente setCart, pues puede dar algún error y no es la manera correcta. Por ello, después en el return, donde tenemos el html, lleva la siguiente estructura.

```jsx
<figure onClick={handleClick}>
		<img src={buttonAddCart} alt="" />
</figure>
```

Al momento de darle click en figure, llama a la función handleClick del componente, y handleClick por dentro cambiará el estado de la variable de estado por uno nuevo. En este caso, cambiamos el valor de cart de hola, por hola mundo.

**¿Cómo acceder a la variable?**

Para acceder a dicha variable cart en el html, podemos usar llaves en donde pasaremos el nombre. Esto es más fácil, pues de otra manera, tendremos que usar más array’s y acceder con el índice, el cual dificulta la lectura del código.

```jsx
<div className="product-info">
				<div>
					<p>$120,00</p>
					<p>Bike</p>
				</div>
				<figure onClick={handleClick}>
					<img src={buttonAddCart} alt="" />
				</figure>
				{cart} // acceder a la variable
</div>
```

## Toggle del menú

**React.useState:**

1. ```jsx
   import React, {useState} from ‘react’;
   ```

   

2. Crear 

   ```jsx
   const [uno, setUno] = useState(‘1’)
   ```

   

   Donde ‘uno’ es la variable que modificara su estado, ‘setUno’ es la función que modificara su estado al suceder X cosa, y ‘1’ es el valor inicial que tendrá esa variable

3. En el valor inicial o ‘1’ puede ir: bool, numero, string, objeto o array.

4. Manejando el evento del elemento que modificara su estado, activaremos la función que recibirá el evento y activara la función ‘setUno( )’ para modificar su valor, ej: setUno(‘2’)

5. Siempre que se recargue la página, el valor volverá a su valor inicial!

**Primero,** importemos el componente menu

```jsx
import Menu from '@components/Menu'
```

**Segundo,** incorporemos la lógica para el menu, tal que al momento de dar click en el correo del header, se muestre dicho menu. Para ello usaremos estado.

```jsx
const Header = () => {
	const [toggle, setToggle] = useState(false);

	const handleToggle = () => {
		setToggle(!toggle);
	}
}
```

Inicializamos la variable toggle en falso, para que al inicio de la app, no se muestre. Luego creamos la función handleToggle que será invocado en algun evento. Esta tiene por dentro *setToggle(!toggle)*. Lo que queremos decir, es que cada vez ocurra el evento, el estado de toggle será el opuesto booleano. En el html, se vería de la siguiente manera.

```jsx
<div className="navbar-right">
		<ul>
			<li className="navbar-email" onClick={handleToggle}>
						platzi@example.com
			</li>
			<li className="navbar-shopping-cart">
				<img src={shoppingCart} alt="shopping cart" />
				<div>2</div>
			</li>
		</ul>
</div>
{toggle && <Menu />}
```

En la etiqueta **li** con el className navbar-email, está el evento onClick, el cuál invoca la función del componente llamada handleToggle. Es decir, cada vez que demos click en el correo, la función cambiará el valor de toggle del estado, en pocas palabras. Para mostrar dicho Menu, más abajo tenemos una expresión la cual renderiza Menu solamente si toggle es verdadero.

Editemos un poco el scss para corregir unos errores al momento en que se muestra

```scss
.Menu {
	width: 100px;
	height: auto;
	border: 1px solid var(--very-light-pink);
	border-radius: 6px;
	padding: 20px 20px 0 20px;
	position: absolute; //nuevo
	top: 60px;
	right: 50px;
}
```

## useEffect y consumo de APIs

React.UseEffect
Ejecuta el código que tengamos adentro justo antes de que React tiene todo listo para renderizar el componente

- El segundo argumento que le enviamos al hook puede ser un array vacío [ ], este le dice al UseEffect que solo se ejecute la primera vez que el componente se renderize, cuando se rerenderize no se ejecutara. Si dejamos ese segundo argumento vacío, el hook se ejecutara todas las veces que nuestro componente se rerenderize.
- Podemos ejecutarlo cuando hayan cambios específicos en cierta variable o cierto componente, para eso podemos poner como segundo argumento un arreglo y adentro el nombre de la variable: [TotalToDos].

React.UseLayoutEffect
Ejecuta el código que tengamos adentro justo después de que React renderizo el componente

Para manejar los estados de request hacia una API:
Se necesitan 3 estados de carga:

1. Estamos cargando tu información
2. Ya cargamos tu información pero tiene hubo un error
3. Ya cargamos tu información y todo esta perfecto.

***useEffect\*** es una manera de que nuestro componente de React, puede recibir nueva info, re-renderizar o cambiar su contenido, cuando una función se haya completado. Es decir, podemos controlar el momento en el cual nuestro componente tome un cierto comportamiento. Por ejemplo, en situaciones como funciones asíncronas ⇒ ***setTimeout o asyn y await,\*** fetch requests o manipulaciones del DOM. Veamos un ejemplo de como usar useEffect.

#### Pre-configuración

> Instalar axios para realizar peticiones get, también instalar el plugin de babel para manejar el asincronismo

```bash
npm install axios
npm install @babel/plugin-transform-runtime
```

Editemos rápidamente .babelrc

```jsx
{
	"presets": [
		"@babel/preset-env",
		"@babel/preset-react"
	],
	"plugins": [
		"@babel/plugin-transform-runtime"
	]
}
```

**Ahora** si veamos como funciona.

```jsx
// ProductList.jsx
import React, { useEffect, useState } from 'react';
import ProductItem from '@components/ProductItem';
import axios from 'axios';

const API = 'https://api.escuelajs.co/api/v1/products';

const ProductList = () => {
	const [products, setProducts] = useState([]);

	useEffect(async () => {
		const response = await axios(API);
		setProducts(response.data);
	}, [])

	return (
		<section className="main-container">
			<div className="ProductList">
				{products.map(product => (
					<ProductItem />
				))}
			</div>
		</section>
	);
}
```

En el inicio estamos importando axios para las peticiones, y creando una constante API que será la necesaria para traer información de los productos

Analicemos el componente productList. Al inicio creamos la estructura inicial de estado, en la cual guardaremos los artículos que traeremos de nuestra API. Ahora, como indicamos, useEffect es muy útil para peticiones HTTP. Para ello, creamos la función anónima que usa useEffect. Por dentro creamos la función que usara async. Dentro creamos una constante llamada response a la cual creamos la petición y guadamos el resultado de la API. A continuación, usamos setProducts para poder guardar la información nueva en products, por eso por dentro le pasamos response.data. Lo más destacable viene ahora, en el momento que pasamos un segundo argumento a useEffect.

#### Maneras de usar useEffect

- **Array Vacío** ⇒ ejecuta el callback solamente una vez, después de que el componente sea cargado en el DOM. Es decir, solamente cuando nuestro componente este cargado en el DOM, ejecutará la función por dentro SOLO UNA VEZ y nunca más

```jsx
const ProductList = () => {
	const [products, setProducts] = useState([]);

	useEffect(async () => {
		const response = await axios(API);
		setProducts(response.data);
	}, [])
}
```

- **Sin argumentos** ⇒ cuando usemos useEffect, pero sin segundo argumento, este ejecutará dicho callback cada vez que se re-rendirece en el DOM. Es decir, cada vez que cambie cualquier valor del componente, este callback siempre se ejecuta

```jsx
const ProductList = () => {
	const [products, setProducts] = useState([]);

	useEffect(async () => {
		const response = await axios(API);
		setProducts(response.data);
	},)
}
```

- **Array con datos** ⇒ este tipo se ejecuta solamente cuando un valor de prop o state de nuestro componente cambie. Es decir, imaginemos que existe un contador interno de clicks. Cada vez que el contador indique explicita mente que un valor del componente cambió, el callback de useEffect siempre se ejecutará.

```jsx
const ProductList = () => {
	const [products, setProducts] = useState([]);

	useEffect(async () => {
		const response = await axios(API);
		setProducts(response.data);
	}, [props, state])
}
```

**Tienen relación a la manera de anterior de componentes con clase**

Usar la manera de array con datos o sin datos son equivalentes a **componentDidUpdate** o **componentDidMount**. Así como indica sus nombres ⇒

- Si el componente se actualizó, este ejecuta un callback el cual tiene cierta función. Esta manera es igual a usar useEffect con un array con datos. *Es decir, ¿hay nueva info? ⇒ realiza esto cada vez que la info nueva se actualice*
- Si el componente se cargó en el DOM, un callback será ejecutado y nunca más. Esta manera es igual a usar useEffect con un array sin datos. *Es decir, ¿ya está cargado? ⇒ necesito que hagas esto y después te puedes quedar dormido*

Documentación de la API: https://api.escuelajs.co/docs/

Constante de la API:

```jsx
const API = 'https://api.escuelajs.co/api/v1/products';
```

Luego declarar las variables para el useState

```jsx
const [products,setProducts] = useState([]);
```

Tienen que instalar axios
Documentación de axios: https://axios-http.com/docs/intro

```npm
npm install axios
```

Solicitud GET

```jsx
useEffect(async ()=>{
	const response = await axios(API);
	setProducts(response.data);
},[])
```

Para recorrer el array de productos se usa el map

```jsx
{products.map(product =>(
	<ProductItem />
))}
```

Docs de la API 👉 https://api.escuelajs.co/docs/
API para desarrollo 👉 https://api.escuelajs.co/api/v1/

[![img](https://www.google.com/s2/favicons?domain=https://babeljs.io/docs/en/babel-plugin-transform-runtime/img/favicon.png)@babel/plugin-transform-runtime · Babel](https://babeljs.io/docs/en/babel-plugin-transform-runtime)

## Custom Hooks para la tienda

En React, podemos crear hooks por nuestra propia cuenta, donde nosotros podemos escribir toda la funcionalidad que deseamos. **Ahora,** haremos un hook el cual servirá para realizar la petición a todos los productos y traer su precio, imágen y descripción.

```jsx
//useGetProducts.js
import { useEffect, useState } from 'react';
import axios from 'axios';

const useGetProducts = (API) => {
    const [products, setProducts] = useState([]);

	useEffect(async () => {
		const response = await axios(API);
		setProducts(response.data);
	}, [])

    return products;
}

export default useGetProducts;
```

El hook es muy sencillo. En el, creamos una array llamado products. Después con ayuda de useEffect realizamos una solicitud a una API (que es pasada como argumento), para traernos toda la información y guardarla con ayuda de axios. setProducts (de useState) guarda el response. Al final regresamos products.

Para poder usar el custom hook, lo implementamos en ProductList

```jsx
// ProductList.jsx
import useGetProducts from '@hooks/useGetProducts'; // Lo importamos

const API = 'https://api.escuelajs.co/api/v1/products';

const ProductList = () => {
	const products = useGetProducts(API);

	return (
		<section className="main-container">
			<div className="ProductList">
				{products.map(product => (
					<ProductItem product={product} key={product.id}/>
				))}
			</div>
		</section>
	);
}
```

Ahora, creamos una constante llamada products que será el mismo array el cual contiene toda la información de los productos. En el, le pasamos API que será un argumento del hook. Como ya sabemos, más abajo en el div, usamos el método map para el array, en donde por cada producto creará una etiqueta del componente ProductItem. ProductItem recibe como datos un key, que es igual a product. id y también product que es igual al producto del array.

Para poder aprovechar esta información, editamos ProductItem.

```jsx
import React, { useState } from 'react';
import '@styles/ProductItem.scss';
import buttonAddCart from '@icons/bt_add_to_cart.svg'

const ProductItem = ({product}) => {
	const [cart, setCart] = useState([]);

	const handleClick = () => {
		setCart([]);
	}

	return (
		<div className="ProductItem">
			<img src={product.images[0]} alt={product.title} />
			<div className="product-info">
				<div>
					<p>${product.price}</p>
					<p>{product.title}</p>
				</div>
				<figure onClick={handleClick}>
					<img src={buttonAddCart} alt="" />
				</figure>
			</div>
		</div>
	);
}

export default ProductItem;
```

En product item, recibimos estos datos en argumentos de la función dentro de llaves. Más abajo, en img, alt, y p, usamos las características de cada producto que son la imágen, titulo, descripción y precio. Así, podemos mostrar la información que corresponde a cada una.

Recomiendo añadirle un estado de “loading” al custom hook:

```js
export default function useGetProducts(API) {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(API)
      .then((res) => res.json())
      .then((response) => {
        setProducts(response);
        setIsLoading(false);
      });
  }, []);

  return { products, isLoading };
}
```

También se le podría añadir un estado de error 🤔

## useRef y formularios

useRef es un hook utilizado para obtener una referencia a los datos de un objeto con información mutable. Es decir, es como una manera de siempre poder obtener los datos mas recientes mediante referencia de algún objeto de html. En este caso referenciamos a los valores recientes de un formulario. Dos características importantes de useRef es que los datos son persistentes en caso de que se re-renderice el componente. Así como también, actualizar los datos de esta referencia no causan el re-render. Cabe recalcar la diferencia con useState, que la actualización de datos es síncrona, ya además como hemos mencionado, no se re-renderiza.

**useRef:**

1. Genera una referencia al elemento y podremos acceder a los valores por medio de ‘current’, y por este medio obtener lo que estamos typeando según sea el caso y poderlo transmitir a donde lo necesitemos.
2. El elemento que tendrá la referencia debe tener atributo: ref={NOMBRE_USEREF}
3. Podemos acceder a toda la data de la siguiente manera: new FormData(NOMBRE_USEREF.current);
4. El elemento también debe tener un atributo: name=“NOMBRE” y podremos acceder a la data que trae en current de la siguiente manera: formData.get(‘NOMBRE’);

[**`preventDefault()`**](https://developer.mozilla.org/es/docs/Web/API/Event/preventDefault)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/API/FormData/favicon-48x48.97046865.png)FormData - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/FormData)

## React Context

**Context** es una herramienta para pasar propiedades en un arbol de componentes de arriba hacia abjo sin tener que pasar por componentes intermedios.

Par usar context debemos importar dos cosas:
**createContext** -> Permite crear el contexto
**useContext** -> Este hook nos va permitir uusar contextos dentro de los componentes

```jsx
import { createContext, useContext} from 'react'; // Tambien podemos importarlo de esta manera
// Creando el contexto
const Context = createContext('valor');
```

**createContext** recibe un valor inicial que se va seleccionar en caso de no tener un provider. Puede ser cualquier valor (string, number, objeto, array…)

### ¿Que es un provider?

Es el encargado de poder pasar el contexto hacia los componentes hijos

```jsx
import { createContext, useContext} from 'react';

//creando el contexto
const Context = createContext('valor por defecto'); 
const Provider = ({children} => {
	return (
//Recibe un valor obligatorio que es el que se va estar pasando a los componentes hijos****
	<Context.Provider value={"soy un valor"} > 
				{children}
		  </Context.Provider>
	)
}

//Finalmente usamos nuestro componente Provider

function App() {
	return (
	<Provider>
			<Contenido>
		</Provider>
	);
}

export default App;
```

React context es una manera de acceder a un tipo de “variables globales” entre nuestros componentes de react. Es decir, hay situaciones en las que quisieramos pasarles datos importantes a un componente de react, y a todos sus nodos hijos. Sin embargo, usando props esta tarea es muy repetitiva y poco efectiva de mantener. Incluso, existen ocasiones que le pasamos props a nodos obligadamente aunque nunca la necesiten. Es aquí cuando entra en acción react context. Nosostros podemos acceder desde donde sea a una variables en nuestra aplicación. E inlcuso podemos crear cuantos contexto queramos donde cada uno mantendra información necesaria.

## Completando el carrito de compras

Podemos destructurar las propiedades del objeto initialState definidos en el hook useInitialState de la siguiente forma:

```jsx
const {state:{cart}}=useContext(AppContext);
```

De esta manera nos evitamos escribir state. cada que hagamos referencia a una propiedad del objeto.

```jsx
 {cart.length > 0 ? <div>{cart.length}</div> : null}
```

Puede que no sea muy relevante en este caso… Por el momento, pero imaginate cuando tengamos un objeto con muchas propiedades. 😀💚

> Una alternativa es usar el operador ‘&&’ y así evitar poner la validacion negativa:
>
> ```jsx
>  {cart.length > 0 && <div>{cart.length}</div>}
> ```

## Orden de compra

 implementaremos una manera para poder desplegar nuestro carrito de compras. Para ello, debemos acceder primero a Header.jsx, donde está el ícono del carrito de compras

```jsx
//Header.jsx
import MyOrder from '@containers/MyOrder';

const Header = () => {
	const { state } = useContext(AppContext);
	const [toggleOrders, setToggleOrders] = useState(false);	

	const handleToggle = () => {
		setToggle(!toggle);
	}

	return (
			<div className="navbar-right">
				<ul>
					<li className="navbar-shopping-cart" onClick={() => setToggleOrders(!toggleOrders)}>
						<img src={shoppingCart} alt="shopping cart" />
						{state.cart.length > 0 ? <div>{state.cart.length}</div> : null}
					</li>
				</ul>
			</div>
			{toggleOrders && <MyOrder />}
		</nav>
	);
}

export default Header;
```

Ahora, para poder mostrar el carrito de compras, añadimos otra variable de estado en el componente llamada toggleOrders. Esta se inicializa en falso para indicar que no queremos mostrar el carrito. Para ello, añadimos 2 expresiones. Una en donde solamente se añadira el nodo MyOrder si toggleOrders es verdadero. Por eso se inicia en falso. El evento que se ejecuta cuando damos click esta en el carrito de compras. Añadimos un onClick, el cual ejecuta una función anónima en la que cambia el valor booleano de toggleOrders. Así, cada vez que demos click, alterna en visibilidad. Para que se pueda ver bien el elemento, editamos el css.

```scss
// MyOrder.scss
.MyOrder {
	top: 60px;
	bottom: auto; // créditos => @Manuel Valencia Londoño
	border-radius: 6px;
	border: 1px solid var(--very-light-pink);
}
```

Ahora, una vez tenemos la lógica para mostrar el elemento de MyOrder, con todas las ordenes correspondientes, debemos editar este mismo componente, para que se muestren tantos elementos en el carrito como diga el contexto. Es decir, si en el carrito hay 5 productos, después en el menu de MyOrder deberían haber 5 elementos. Para ello, editamos MyOrder.jsx

```jsx
// MyOrder.jsx
import React, { useContext } from 'react'; // añadimos useContext
import AppContext from '@context/AppContext'; // añadimos AppContext

const MyOrder = () => {
	const { state } = useContext(AppContext);

	return (
			<div className="my-order-content">
				{state.cart.map(product => (
					<OrderItem product={product} key={`orderItem-${product.id}`}/>
				))}
			</div>
		</aside>
	);
}

export default MyOrder;
```

Ahora, lo que hacemos es traer las herramientas necesarias para poder trabajar con contexto. En el componente, traemos el valor de state y lo guardamos como contexto para poder acceder a dicho estado.

> Este es un ejemplo perfecto del uso de context, donde simplemente cada componente accede a las variables si es necesario o no. No hay que trasladar estos datos de manera tediosa y repetitiva

Como en state tenemos un array, por ello creamos una lógica con map. Al ejecutar *state.cart.map* estaremos recorriendo cada elemento de state (es decir, del array de los elementos del carrito) Y regresamos un nodo llamado OrderItem. En orderItem le pasamos un valor product y key, en el cual podrán guardar cada componente un elemento product y un key identificador único. Simplemente editamos de tal manera que el key sea totalmente único, por ello usamos templateliterals.

A los estilos de `MyOrder.js` yo le agregué los siguientes estilos para darle un poquito de animación 😄 al hacer click ahora la barra lateral “crece” de derecha a izquierda.

```css
@keyframes swipeLeft {
  0% {
    width: 0;
  }
  100% {
    width: 360px;
  }
}

.MyOrder {
  ...
  ...
    animation: swipeLeft 0.26s ease;
}
```

## Calculando el precio total

Para calcular el precio real de los elementos añadidos al carrito y poder mostrar la información correctamente, trabajaremos sobre MyOrder y OrderItem.

```jsx
// OrderItem.jsx
import React from 'react';

const OrderItem = ({ product }) => {
	return (
		<div className="OrderItem">
			<figure>
				<img src={product.images[0]} alt={product.title} />
			</figure>
			<p>{product.title}</p>
			<p>{product.price}</p>
			<img src={icon_close} alt="close" />
		</div>
	);
}

export default OrderItem;
```

Primero, en OrderItem recordemos que desde MyOrder le estabamos pasando el producto. Sin embargo, en todos los casos estábamos mostrando una bicicleta. Ahora, para poder mostrar la información de cada producto, simplemente le indicamos al componente que recibimos como parámetro un product. Después, editamos el HTML, para que la imágen, título y precio correspondan al del producto, así que solamente accedemos a las propiedades de cada uno.

```jsx
// MyOrder.jsx
const MyOrder = () => {
	const { state } = useContext(AppContext);

	const sumTotal = () => {
		const reducer = (accumulator, currentValue) => accumulator + currentValue.price;
		const sum = state.cart.reduce(reducer, 0);
		return sum;
	}

	return (
		<div className="order">
				<p>
					<span>Total</span>
				</p>
			<p>${sumTotal()}</p>
		</div>
	);
}

export default MyOrder;
```

Ahora, editemos el componente MyOrder. En my order, creamos una función llamada sumTotal donde creamos la lógica de la suma de los precios de los productos del carrito. Para poder crear esta función, primero creamos la función llamada reducer, en la cual le pasamos dos valores, un acumulador y un valor corriente, para después regresar la suma de ambos. Después, en la constante sum, usamos el método reduce para los arrays. El método se aplica sobre el array state.cart con todos estos elementos. Simplemente, le pasamos la función antes definida, y un valor inicial igual a cero. Por último, regresamos sum. Más abajo, en la etiqueta p, entre llaves indicamos que se ejecute esta función para calcular el precio cada vez que se renderice el componente.

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/react-practico at react/18](https://github.com/platzi/react-practico/tree/react/18)

## Eliminando productos del carrito

lógica para crear una función que elimine productos del carrito. Recordemos, que la función para poder añadir la teníamos en nuestro hook, así que ahí mismo vamos a crear una función para eliminar.

```jsx
// useInitialState
import { useState } from "react";

const useInitialState = () => {
    const removeFromCart = (payload) => {
        setState({// minificado
            cart: state.cart.filter(item => item.id !== payload.id),
        })
    }

    return {
        state,
        addToCart,
        removeFromCart,
    }
}

export default useInitialState;
```

Dentro del hook, vamos a crear una función llamada removeFromCart. Esta función recibirá como parámetro un payload ( o el producto ), y después actualizaremos el estado. Para ello, con ayuda de setState, indicamos que ahora el estado seguirá siendo un objeto, donde cart, será el array, PERO usaremos el método filter, donde eliminaremos el item del array con ayuda de su id, y después regresará el array sin este elemento. Al final, tenemos que regresar la nueva función en el hook, es decir, añadir return … , removeFromCart.

```jsx
// OrderItem.jsx
import React, { useContext } from 'react';
import AppContext from '@context/AppContext';

const OrderItem = ({ product }) => {
	const { removeFromCart } = useContext(AppContext);

	const handleRemove = () => {
		removeFromCart(product);
	}

	return (
		<div className="OrderItem">
			<img src={icon_close} alt="close" onClick={() => handleRemove(product)} />
		</div>
	);
}

export default OrderItem;
```

Recordemos, que el ícono de eliminar, lo tenemos en OrderItem, así que este componente será el encargado de usar la función para remover el producto. Para ello, debemos importar también useContext para poder trabajar con el contexto. Para implementar la función, creamos una constante donde traemos la función removeFromCart del contexto. Después, creamos una función que se encargará de manejar el click sobre el ícono de eliminar. En esta función, siempre ejecutaremos la función removeFromCart con el argumento product, para eventualmente quitarlo de nuestro carrito de compras.

# 6. Deploy

## Cómo comprar tu dominio y conexión con Cloudflare

[![img](https://www.google.com/s2/favicons?domain=https://www.namecheap.com/assets/img/nc-icon/favicon.ico)Buy a domain name - Register cheap domain names from $0.99 - Namecheap](https://www.namecheap.com/)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Cloudflare - The Web Performance & Security Company | Cloudflare](https://www.cloudflare.com/)

## Automatizando el despliegue con GitHub Actions

🚀 Desplegar su app de React en GitHub Pages, pero ya tienen otros proyectos usando GitHub Pages.

Es decir, quieren esta app en `user.github.io/repo` o `mi-dominio.com/repo`

### 🚀 Desplegar una app de React junto con otros proyectos en Github Pages

Proyectos usando GitHub Pages.

- Es decir, [usuario.github.io/](http://usuario.github.io/) ya está ocupado y la app la queremos en **[usuario.github.io/nombre-de-mi-repo](http://usuario.github.io/nombre-de-mi-repo)**
- O bien si tenemos un dominio personalizado ya asociado a GitHub Pages, nuestra app la queremos en **[mi-dominio.com/nombre-de-mi-repo](http://mi-dominio.com/nombre-de-mi-repo)**.

Para conseguir que nuestra app se muestre en la url con **/nombre-de-mi-repo** debemos cambiar lo siguiente:

en el `package.json` agregaremos el `homepage` con la url para GitHub Pages de nuestro repo así:

```json
{
  "homepage": "https://username.github.io/nombre-de-mi-repo/",
  "name": "my-app",
  "version": "1.0.0",
  "description": "Desc",
  "main": "src/index.js",
```

en el `webpack.config.js`

- A `filename` le agregaremos el nombre de nuestro repo al bundle.js así: `nombre-de-mi-repo/bundle.js`
- El `publicPath` será el nombre de nuestro repo, así: `/nombre-de-mi-repo/`

Nos quedará así

```j
module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "nombre-de-mi-repo/bundle.js",
    publicPath: "/nombre-de-mi-repo/",
  },
```

Por último a nuestro `BrowserRouter` le debemos agregar un `basename` con el nombre de nuestro repo, así:

```jsx
<BrowserRouter  "/nombre-de-mi-repo">
```

Una vez hechos estos cambios nuestra app se verá en [usuario.github.io/nombre-de-mi-repo](http://usuario.github.io/nombre-de-mi-repo) o (si tenemos un dominio personalizado) en [mi-dominio.com/nombre-de-mi-repo](http://mi-dominio.com/nombre-de-mi-repo)

En [esta clase puedes ver como usar GitHub Actions para automatizar el despliegue](https://platzi.com/clases/2484-react-practico/42218-automatizando-el-despliegue-con-github-actions/) 🚀

**Usar GitHub Pages:**

1. npm install —save-dev gh-pages

2. Agregar reglas al package.json:

   1. “homepage”:”[https://SergioMaurySterling.github.io/NOMBRE_REPO”](https://sergiomaurysterling.github.io/NOMBRE_REPO”)

3. Crear nuevos scripts en package.json

   1. “predeploy”:“npm run build”, :Este ejecutara automáticamente después el “deploy”
   2. “deploy”:“gh-pages -d build”

4. npm run deploy (Crea nueva carpeta llamada build que tiene todos nuestros archivos del proyecto)

5. En GitHub ingresamos a:

   1. Settings
   2. Pages
   3. Podemos ver el link de publicación

   **GitHub Actions / Pages**

   1. https://github.com/marketplace?type=&verification=&query=deploy+github+page+
   2. Deploy to GitHub Pages script
   3. Crear folder llamado “.github”, adentro otra llamada workflows, y adentro un archivo llamado deploy.yml
   4. Copiar el primer recurso y pegarlo de forma manual, personalizado en el archivo deploy.yml
   5. Cambiar el nombre de la carpeta “build” a “diste” ya que esa acciona todo
   6. Tener el repo actualizado y todo en la rama main o principal
   7. Podemos revisar en el repo, en actions que el workflow corra y finalice exitosamente
      ￼
   8. Settings -> pages: Cambiar el source a la rama gh-pages y guardar
   9. Configurar dominio:
      1. Se pone el dominio en Custom Domain
      2. Ingresamos al dashboard de Cloudflare -> DNS
      3. https://docs.github.com/en/pages/configuring-a-custom-domain-for-your-github-pages-site/managing-a-custom-domain-for-your-github-pages-site
      4. Agregamos en Cloudflare los 4 apuntadores A que nos entrega GitHub
      5. Agregamos CNAME, con nombre www, que apunte a nuestro dominio personal

# 7. Próximos pasos

## Retos de React.js (bug hunters)

![img](https://github.com/Jason171096/Shopping-Cart/blob/main/gif.gif?raw=true)

## Continúa con el Curso Práctico de Next.js

Nunca pares de Aprender