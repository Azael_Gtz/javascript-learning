<h1>Backend con Node.js</h1

<h3>Guillermo Rodas</h3>

<h1>Tabla de Contenido</h1>

- [1. Tu primera experiencia con Node.js](#1-tu-primera-experiencia-con-nodejs)
  - [Introducción y bienvenida](#introducción-y-bienvenida)
  - [¿Qué es Node.js?](#qué-es-nodejs)
  - [¿Qué es Node.js y para qué sirve?](#qué-es-nodejs-y-para-qué-sirve)
  - [Diferencias entre Node.js y JavaScript](#diferencias-entre-nodejs-y-javascript)
  - [Resumen: Diferencias Nodejs y Javascript](#resumen-diferencias-nodejs-y-javascript)
  - [Instalación de Node.js](#instalación-de-nodejs)
  - [Arquitectura orientada a eventos](#arquitectura-orientada-a-eventos)
  - [Node.js para la web](#nodejs-para-la-web)
- [2. Manejo y uso de Streams con Node.js](#2-manejo-y-uso-de-streams-con-nodejs)
  - [Introducción a streams](#introducción-a-streams)
    - [Rendimiento sin streams](#rendimiento-sin-streams)
  - [Readable y Writable streams](#readable-y-writable-streams)
  - [Duplex y Transforms streams](#duplex-y-transforms-streams)
- [3. Uso de utilidades de Node.js](#3-uso-de-utilidades-de-nodejs)
  - [Sistema operativo y sistema de archivos](#sistema-operativo-y-sistema-de-archivos)
  - [Administrar directorios y archivos](#administrar-directorios-y-archivos)
  - [Código de la clase](#código-de-la-clase)
  - [Consola, utilidades y debugging](#consola-utilidades-y-debugging)
  - [Clusters y procesos hijos](#clusters-y-procesos-hijos)
- [4. Crea tu primer proyecto en Express.js](#4-crea-tu-primer-proyecto-en-expressjs)
  - [¿Qué es Express.js y para qué sirve?](#qué-es-expressjs-y-para-qué-sirve)
  - [Creando tu primer servidor con Express.js](#creando-tu-primer-servidor-con-expressjs)
  - [Request y Response Objects](#request-y-response-objects)
  - [Exploremos las propiedades más importantes](#exploremos-las-propiedades-más-importantes)
    - [req.body](#reqbody)
    - [req.params](#reqparams)
    - [req.query](#reqquery)
- [Response Object](#response-object)
  - [Exploremos los métodos más comunes](#exploremos-los-métodos-más-comunes)
    - [res.end()](#resend)
    - [res.json()](#resjson)
    - [res.send()](#ressend)
- [5. Aprende a crear un API con REST](#5-aprende-a-crear-un-api-con-rest)
  - [Anatomía de una API Restful](#anatomía-de-una-api-restful)
  - [Estructura de una película con Moockaru](#estructura-de-una-película-con-moockaru)
  - [Implementando un CRUD en Express.js](#implementando-un-crud-en-expressjs)
  - [Métodos idempotentes del CRUD](#métodos-idempotentes-del-crud)
  - [Implementando una capa de servicios](#implementando-una-capa-de-servicios)
- [6. Cómo conectarse con librerías externas en Express.js](#6-cómo-conectarse-con-librerías-externas-en-expressjs)
  - [Creación de una BD en MongoAtlas](#creación-de-una-bd-en-mongoatlas)
  - [Conexión a MongoAtlas una instancia de MongoDB](#conexión-a-mongoatlas-una-instancia-de-mongodb)
  - [Conexión con Robot3T y MongoDB Compass a una BD](#conexión-con-robot3t-y-mongodb-compass-a-una-bd)
  - [Implementación de las acciones de MongoDB](#implementación-de-las-acciones-de-mongodb)
  - [Conexión de nuestros servicios con MongoDB](#conexión-de-nuestros-servicios-con-mongodb)
- [7. Conoce como funcionan los Middleware en Express.js](#7-conoce-como-funcionan-los-middleware-en-expressjs)
  - [¿Qué es un middleware? Capa de manejo de errores usando un middleware](#qué-es-un-middleware-capa-de-manejo-de-errores-usando-un-middleware)
  - [Manejo de errores asíncronos y síncronos en Express](#manejo-de-errores-asíncronos-y-síncronos-en-express)
  - [Capa de validación de datos usando un middleware](#capa-de-validación-de-datos-usando-un-middleware)
  - [¿Qué es Joi y Boom?](#qué-es-joi-y-boom)
  - [Implementando Boom](#implementando-boom)
  - [Implementando Joi](#implementando-joi)
  - [Probar la validación de nuestros endpoints](#probar-la-validación-de-nuestros-endpoints)
  - [Middlewares populares en Express.js](#middlewares-populares-en-expressjs)
- [8. Implementa tests en Node.js](#8-implementa-tests-en-nodejs)
  - [Creación de tests para nuestros endpoints](#creación-de-tests-para-nuestros-endpoints)
  - [Creación de tests para nuestros servicios](#creación-de-tests-para-nuestros-servicios)
  - [Creación de tests para nuestras utilidades](#creación-de-tests-para-nuestras-utilidades)
  - [Agregando un comando para coverage](#agregando-un-comando-para-coverage)
  - [Debugging e inspect](#debugging-e-inspect)
- [9. Despliega tu primera aplicación en Express.js](#9-despliega-tu-primera-aplicación-en-expressjs)
  - [Considerando las mejores prácticas para el despliegue](#considerando-las-mejores-prácticas-para-el-despliegue)
  - [Variables de entorno, CORS y HTTPS](#variables-de-entorno-cors-y-https)
  - [¿Cómo implementar una capa de manejo de caché?](#cómo-implementar-una-capa-de-manejo-de-caché)
  - [¿Cómo contener tu aplicación en Docker?](#cómo-contener-tu-aplicación-en-docker)
  - [Despliegue en Now](#despliegue-en-now)
- [10. Conclusiones](#10-conclusiones)
  - [¿Qué aprendiste en este curso?](#qué-aprendiste-en-este-curso)



# 1. Tu primera experiencia con Node.js

## Introducción y bienvenida

**Guillermo Rodas** será tu profesor en este curso, él tiene más 6 años dedicado a programar sólo en JavaScript y forma parte del equipo de **Auth0**, además de ser **Google Developer Expert** (GDE) en Web Technologies y organizador de eventos como Medellin CSS y CSS Conf.

Requisitos antes de iniciar:

- Node y NPM
- Editor de texto ya sea vsCode, Atom o Sublime Text
- Navegador Chrome o Firefox
- Extensión JSON viewer
- Postman

## ¿Qué es Node.js?

En esta clase el profesor Guillermo Rodas nos explica qué es Node.js y algunas de sus ventajas.

Node.js es un entorno de ejecución para JavaScript construido con el motor JavaScript V8 de Chrome. JavaScript es un lenguaje interpretado pero en Node.js tenemos algo llamado el JIT Compiler que es una especie de monitor que optimiza fragmentos de código que son ejecutados frecuentemente.

**¿Qué es node.js ?**
Node.js es un entorno de ejecucion para javascript

**¿Qué es un entorno de ejecución?**

Un entorno de ejecucion es una capa encima del sistema operativo que ejecuta una pieza de software.

**¿Qué nos permite?**

Usar javascript para el servidor.

> Node js: es el resultado de tomar el motor de JavaScript Chrome V8 para crear un entorno de ejecución y así ejecutar JavaScript del lado del Servidor. 👨‍💻

![img](https://www.google.com/s2/favicons?domain=https://blog.risingstack.com/history-of-node-js//assets/favicons/favicon-32x32.png?v=92af52a22a)[History of Node.js on a Timeline | @RisingStack](https://blog.risingstack.com/history-of-node-js/)

## ¿Qué es Node.js y para qué sirve?

**Dato curioso:** el mismo creador de Node, Ryan Dahl creó hace poco **Deno** (Node al revéz). El mismo esta creado con Rust y Typescript.

> **Lenguaje compilado** está optimizado para el momento de la ejecución, aunque esto signifique una carga adicional para el programador. Por otro lado, un **lenguaje interpretado** está optimizado para hacerle la vida más fácil al programador, aunque eso signifique una carga adicional para la máquina.

JavaScript cuando un código se repite demasiadas veces, por ejemplo una el llamado a una función, se toma como función caliente y es optimizado al ser compilado a **Bytecode**

El punto principal es que Node.js fue construido en motor V8, y de esa manera poder tener un entorno de desarrollo para usar javascript del lado del servidor.

> El garbage collector es una forma de limpiar la memoria de forma automática, busca los elementos no utilizados y los destruye.

- Node.js es un entorno de ejecución para JavaScript construido con el motor JavaScript V8 de Chrome.
- JavaScript es un lenguaje interpretado pero en Node.js tenemos algo llamado el JIT Compiler que optimiza los fragmentos de codigo.

## Diferencias entre Node.js y JavaScript

En JavaScript del lado del cliente tenemos el DOM y el CSSDOM así como el objeto window para manipular los elementos de nuestra página además una serie de APIs, aquí unos ejemplos:

- fetch
- SessionStorage y LocalStorage
- canvas
- bluetooth
- audio
- web authentication

Mientras que en Node.js no tenemos un DOM ni un objeto windows, lo que sí tenemos son una serie de módulos que nos permiten interactuar con los recursos de la máquina como el sistema operativo y el sistema de archivos, por ejemplo:

- os
- fs
- http
- util
- debugger
- stream
- events

**En javascript**

- En javascript tenemos el DOM. La interfaz web.
- En javascript tambien tenemos el CSSOM
- Tambien el fetch API
- Toda la capa del web storage
- Tambien el canvas API
- Las APIs en general del navegador

**En Node.js**

- Node tiene el modulo de HTTP para crear servidores
- El modulo del sistema operativo para comunicarse con el SO
- El modulo utiliddes
- El modulo debugger

**En común**

- Librerias comunes streams
- Eventos
- Ecmascript modules
- Consola

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/static/img/favicon32.7f3da72dcea1.png)[Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API)

[Index | Node.js v12.9.0 Documentation](https://nodejs.org/api/)

## Resumen: Diferencias Nodejs y Javascript

Ahora que ya sabes qué es Node.js y su diferencia con Javascript, te compartimos este resumen gráfico para que no olvides las actividades que los hacen distintos.

<img src="https://i.ibb.co/YZpxF5s/js.jpg" alt="js" border="0">

## Instalación de Node.js

Para instalar Node.js tienes que dirigirte a https://nodejs.org/es/ y elegir entre la ultima versión o la version LTS.

Por defecto Node.js detecta tu sistema operativo y descarga el archivo indicado para la instalación, si no es tu caso puedes dirigirte al enlace de otras descargas: https://nodejs.org/es/download/.

![download-node.png](https://static.platzi.com/media/user_upload/download-node-7c420a71-2425-49bf-a95c-d089d500f823.jpg)

La diferencia entre la version LTS a la ultima versión es que la versión LTS tiene soporte de mantenimiento por 3 años.

## Arquitectura orientada a eventos

a versión de Node.js v12.11.1 estos son los módulos nativos que vienen por default:

- [Assertion Testing](https://nodejs.org/api/assert.html)
- [Async Hooks](https://nodejs.org/api/async_hooks.html)
- [Buffer](https://nodejs.org/api/buffer.html)
- [C++ Addons](https://nodejs.org/api/addons.html)
- [C/C++ Addons - N-API](https://nodejs.org/api/n-api.html)
- [Child Processes](https://nodejs.org/api/child_process.html)
- [Cluster](https://nodejs.org/api/cluster.html)
- [Command Line Options](https://nodejs.org/api/cli.html)
- [Console](https://nodejs.org/api/console.html)
- [Crypto](https://nodejs.org/api/crypto.html)
- [Debugger](https://nodejs.org/api/debugger.html)
- [Deprecated APIs](https://nodejs.org/api/deprecations.html)
- [DNS](https://nodejs.org/api/dns.html)
- [Domain](https://nodejs.org/api/domain.html)
- [ECMAScript Modules](https://nodejs.org/api/esm.html)
- [Errors](https://nodejs.org/api/errors.html)
- [Events](https://nodejs.org/api/events.html)
- [File System](https://nodejs.org/api/fs.html)
- [Globals](https://nodejs.org/api/globals.html)
- [HTTP](https://nodejs.org/api/http.html)
- [HTTP/2](https://nodejs.org/api/http2.html)
- [HTTPS](https://nodejs.org/api/https.html)
- [Inspector](https://nodejs.org/api/inspector.html)
- [Internationalization](https://nodejs.org/api/intl.html)
- [Modules](https://nodejs.org/api/modules.html)
- [Net](https://nodejs.org/api/net.html)
- [OS](https://nodejs.org/api/os.html)
- [Path](https://nodejs.org/api/path.html)
- [Performance Hooks](https://nodejs.org/api/perf_hooks.html)
- [Policies](https://nodejs.org/api/policy.html)
- [Process](https://nodejs.org/api/process.html)
- [Punycode](https://nodejs.org/api/punycode.html)
- [Query Strings](https://nodejs.org/api/querystring.html)
- [Readline](https://nodejs.org/api/readline.html)
- [REPL](https://nodejs.org/api/repl.html)
- [Report](https://nodejs.org/api/report.html)
- [Stream](https://nodejs.org/api/stream.html)
- [String Decoder](https://nodejs.org/api/string_decoder.html)
- [Timers](https://nodejs.org/api/timers.html)
- [TLS/SSL](https://nodejs.org/api/tls.html)
- [Trace Events](https://nodejs.org/api/tracing.html)
- [TTY](https://nodejs.org/api/tty.html)
- [UDP/Datagram](https://nodejs.org/api/dgram.html)
- [URL](https://nodejs.org/api/url.html)
- [Utilities](https://nodejs.org/api/util.html)
- [V8](https://nodejs.org/api/v8.html)
- [VM](https://nodejs.org/api/vm.html)
- [Worker Threads](https://nodejs.org/api/worker_threads.html)
- [Zlib](https://nodejs.org/api/zlib.html)

Una arquitectura orientada por eventos es un patron de diseño el cual permite a un conjunto de sistemas comunicarse entre si de forma reactiva mediante la publicacion y el consumo de eventos, los cuales se pueden interpretar como cambios de estado de objetos.

> Hay diferentes maneras de hacer que el código sea asíncrono (o al menos que se comporte como tal). En JavaScript podemos hacer esto con callbacks, promises o con async-await. Pero con Node.js hay otra forma de hacerlo, y esto es usando `EventEmmiter` de los módulos nativos de Node.js.

[Events | Node.js v12.9.0 Documentation](https://nodejs.org/api/events.html#events_class_eventemitter)

## Node.js para la web

```js
const http = require("http");
const moment = require("moment");

const server = http.createServer();

server.on("request", (req, res) => {
  if (req.method === "POST" && req.url == "/echo") {
    let body = [];
    req
      .on("data", chunk => {
        body.push(chunk);
      })
      .on("end", () => {
        res.writeHead(200, { "Content-type": "text/plain" });
        body = Buffer.concat(body).toString();
        if (!moment(body, "YYYY-MM-DD").isValid()) {
          res.end("no es un formato valido. Formato esperado YYYY-MM-DD");
        } else {
          var weekDayName = moment(body).format("dddd");

          res.end("Tu dia de Nacimiento es:" + weekDayName);
        }
      });
  } else {
    res.statusCode = 404;
    res.end();
  }
});

server.listen(8002);
console.log("Servidor enla url http://localhost:8002");
```

Utilizando el Modulo Moment

# 2. Manejo y uso de Streams con Node.js

## Introducción a streams

Los Streams son una colección de datos como los arrays o strings sólo que se van procesando pedazo por pedazo, esta capacidad los hace muy poderosos porque podemos manejar una gran cantidad de datos de manera óptima.

> Estaba la pájara pinta sentada en un verde limón. Con el pico cortaba la rama, con la rama cortaba la flor. Ay, ay, ay! Cuándo vendrá mi amor…Me arrodillo a los pies de mi amante, me levanto constante, constante. Dame la mano, dame la otra, dame un besito sobre la boca.Daré la media vuelta, Daré la vuelta entera,Con un pasito atrás,Haciendo la reverencia. Pero no, pero no, pero no, porque me da vergüenza, pero sí, pero sí, pero sí, porque te quiero a ti.
>
> 😃 [Streams en Node.js](https://elabismodenull.wordpress.com/2017/03/28/el-manejo-de-streams-en-nodejs/)

```js
//módulo para crear un servidor
const http = require('http')
//módulo para trabajar con archivos
const fs = require('fs')
//inicializamos el servidor
const server = http.createServer()
//manejamos los eventos del servidor
server.on('request', (request, response) => {
  /**
   * lee el archivo como un stream
   * el tamaño del chunk por defecto es de 64kb
   * para un fs, para un stream normal es de 16kb
   */
  const src = fs.createReadStream('./big')
  
  /**
   * la función de 'pipe()' es limitar el 
   * almacenamiento en buffer de datos a niveles
   * aceptables de modo que no se sobrecargue la
   * memoria disponible.
   */
  src.pipe(response)
})

server.listen(3000)
```

### Rendimiento sin streams

1. Crea un archivo gigante: crea big-file.js, correlo con `$ node big-file.js`
2. Crea un server que lo lea: crea file-server.js correlo con `$ node file-server.js`
3. Monitoriza la lectura: `$ curl -i localhost:3001`
4. Podrás ver como tu PC se quema por la sobrecarga. En administrador de tareas y ve como sube la RAM utilizada y el CPU.

```jsx
//big-file.js
//al ejecutar esto se crea un archivo gigante de texto
//llamado big.txt
const fs = require('fs')
const file = fs.createWriteStream('./big.txt')
for (let i = 0; i < 1e6; i++) {
  file.write(
    'Estaba la pájara pinta sentada en un verde limón. Con el pico cortaba la rama, con la rama cortaba la flor. Ay, ay, ay! Cuándo vendrá mi amor…Me arrodillo a los pies de mi amante, me levanto constante, constante. Dame la mano, dame la otra, dame un besito sobre la boca.Daré la media vuelta, Daré la vuelta entera,Con un pasito atrás,Haciendo la reverencia. Pero no, pero no, pero no, porque me da vergüenza, pero sí, pero sí, pero sí, porque te quiero a ti. X 1e6'
  )
}
file.end()
//file-server.js
//Mediante esto leemos ese archivo gigante
const fs = require('fs')
const server = require('http').createServer()

// event => cb(info.req, res.data)
server.on('request', (req, res) => {
  // promise => cb(info.req, res.data)
  fs.readFile("./big.txt", (err, data) => {
    if (err) { //maneja el error
      console.log(err)
    } else { //maneja la data
      res.end(data)
    }
  })
})
server.listen(3001)
console.log('Server in port 3001')
```

<h3>Rendimiento con streams</h3>

1. Crea un servidor que lea el archivo gigante con streams
2. Correlo con `$ node stream-server.js`
3. Monitoriza la lectura: `$ curl -i localhost:3001`
4. Podrás ver como tu PC se quema, pero optimizadamente. En administrador de tareas y ve como sube la RAM utilizada y el CPU en comparacion a cuando no se usa streams, en mi caso fue optimizacion del 50%.

```jsx
//stream-server.js
const fs = require('fs')
const server = require('http').createServer()

// event => cb(info.req, res.data)
//res es stream de los datos que regresamos
server.on('request', (req, res) => { 

  // event => cb(info.req, res.data)
  //obtenemos la data en forma de stream
  const src = fs.createReadStream('./big.txt') 
  
  // mediate pipe conectamos un stream de lectura con uno de escritura que es res
  src.pipe(res) 
})
server.listen(3001)
console.log('Server in port 3001')
```

## Readable y Writable streams

**writable-stream.js**

```js
//importamos Writable del módulo de streams
const { Writable } = require('stream')

/**
 * Creamos un nuevo Writable y lo asignamos
 * a la constante writableStream
 */
const writableStream = new Writable({
  defaultEncoding: 'utf8',
  /**
   *
   * @param {*} chunk el buffer de entrada
   * @param {*} encoding la codificación
   * del buffer, si el chunk es un string
   * el enconding es la codificación en
   * caracteres de esa cadena, si la
   * codificación es un buffer esta se puede
   * ignorar
   * @param {*} callback esta función es
   * llamada cuando se complete el
   * procesamiento para el chunk
   * proporcionado.
   */
  write(chunk, encoding, callback) {
    console.log(chunk.toString())
    callback()
  }
})
process.stdin.pipe(writableStream)
```

**readable-stream.js**

```js
//requerimos el módulo de Readable stream
const { Readable } = require('stream')
//instanciamos un nuevo Readable stream
const readableStream = new Readable()
/**
 * Cuando se ejecuta el método push()
 * los datos son almacenados en el buffer
 * si no se consumen los datos en el buffer 
 * estos se almacenan en la cola interna 
 * hasta que son consumidos
 */
readableStream.push(`${0 / 0}`.repeat(10).concat(' Batman, Batman!'))
/**
 * El stream de lectura se da por terminado
 * cuándo el buffer recibe un null en este caso
 * con el método push(null)
 */
readableStream.push(null)

/**
 * "pipe(writable)" Este método nos permite 
 * encadenar diferentes streams para su 
 * manipulación por medio de cómputos. Lo que
 * hace es recibir un stream
 * de entrada, realiza una operación sobre 
 * dicho stream y devuelve un nuevo stream con
 * dicha transformación.
 * 
 * "stdout" es un writable stream que toma el 
 * buffer y lo muestra en pantalla
 */
readableStream.pipe(process.stdout)
```

**readable-stream-on-demand.js**

```js
//importamos el modulo readable
const { Readable } = require('stream')

//instanciamos un nuevo readable stream
const readableStream = new Readable({
  /**
   * constructor
   * @param {*} size tamaño del buffer
   * de lectura este se representa en bytes
   * y el valor por defecto es 16kb para un
   * readable stream y para fs es de 64kb
   * parámetro opcional
   */
  read(size) {
    setTimeout(() => {
      //la letra es mayor que z
      if (this.chartCode > 90) {
        //finalizamos la lectura
        this.push(null)
        return;
      }
      /**
       * agregamos la letra al buffer y después
       * se le suma 1 
       */
      this.push(String.fromCharCode(this.chartCode++))
    }, 100)
  }
})

/**
 * inicializamos el atributo chartCode
 * y le asignamos el valor  ASCII de la letra A
 */
readableStream.chartCode = 65
/**
 * manejamos el stream de lectura y le asignamos
 * un stream de salida por pantalla
 */
readableStream.pipe(process.stdout)
```

**`process.stdout`**, cuando corremos un programa en consola tenemos 3 outputs posibles

- standard input (stdin)
- standard output (stdout)
- standard error (stderr)
  .
  El output es lo que mostramos en consola
  el input es el argumento que le damos al programa, todo lo que sigue al `programa.js`
  El error es un número que regresamos dependiendo del error que queramos mostrar (Puede no ser un número)

Por lo mismo, hice el siguiente programa usando lo que vimos en Readable y Writable, pero el output del Readable se tomará como argumento del Writable. Así:

```js
const  {Writable, Readable} = require( 'stream' );

const  readableStream = new Readable();
readableStream.push(`${0/0} `.repeat(10).concat('Batman! Batman\n'))
readableStream.push(null);
readableStream.pipe(process.stdin);

const  writableStream = new Writable({
    write(chunk, encoding, callback){
        console.log(chunk.toString());
        callback()
    }

});

process.stdin.pipe(writableStream);
```

[streams de node](https://www.freecodecamp.org/news/node-js-streams-everything-you-need-to-know-c9141306be93/)

## Duplex y Transforms streams

Ambos sirven para simplificar nuestro código:

- **Duplex:** implementa los métodos write y read a la vez.
- **Transform:** es similar a Duplex pero con una sintaxis más corta.

El propósito de tener distintos tipos de streams no solo es simplificar el código, tienen pequeñas diferencias en su objetivo.

**Tipos de streams**
Hay cuatro tipos fundamentales de streams en Node.js

- Writable: streams en los que los datos pueden ser escritos.
- Readable: streams en los que los datos pueden ser leídos.
- Duplex: streams que pueden ser leídos y escritos.
- Transform: Duplex streams que pueden modificar o transformar los datos cuando son escritos o leídos.

**Mi Reto desglosado**

```js
const { Transform } = require('stream')

/** ingresa un string y convierte el primer
 * carácter a mayúscula
 */
const upperFirst = text => {
  //obtiene primer letra y convierte a mayúscula
  let first = text.charAt(0).toUpperCase()
  //el string a partir de la segunda letra
  let rest = text.slice(1)
  //unión primer letra + resto
  return first + rest
}

const transformStream = new Transform({
  transform(chunk, encoding, callback) {
    //la cadena de entrada en string
    const strChunk = chunk.toString()
    //cadena en minúsculas
    const lowerChunk = strChunk.toLowerCase()
    //array separado por espacio en blanco
    const arrayChunk = lowerChunk.split(' ')
    /**
     * creación de un nuevo array con camelCase
     * si  i (index) es 0 se retorna la primer
     * palabra sin cambios, si no se cambia la
     * primer letra de la palabra a mayúscula
     */
    const arrayCamel = arrayChunk.map((word,i)=>{
      return i === 0 ? word : upperFirst(word)
    })
    //se junta el array anterior y listo
    const camelCase = arrayCamel.join('')
    this.push(camelCase)
    //finaliza la el flujo para esta chunk
    callback()
  }
})
process.stdin.pipe(transformStream)
.pipe(process.stdout)
```

**Mi reto simplificado**

```js
const { Transform } = require('stream')

const upperFirst = text => {
  return text.charAt(0)
    .toUpperCase()
    .concat(text.slice(1))
}

const transformStream = new Transform({
  transform(chunk, encoding, callback) {
    const camelCase = 
    chunk
    .toString()
    .toLowerCase()
    .split(' ')
    .map((word, index) => {
      return index === 0 ? word : upperFirst(word)
    })
    .join('')
    this.push(camelCase)
    callback()
  }
})

process.stdin.pipe(transformStream)
.pipe(process.stdout)H
```

# 3. Uso de utilidades de Node.js

## Sistema operativo y sistema de archivos

En esta clase vemos dos módulos básicos:

- **os**. Sirve para consultar y manejar los recursos del sistema operativo.
- **fs**. Sirve para administrar (copiar, crear, borrar etc.) archivos y directorios.

Los métodos contenidos en estos módulos (y en todo Node.js) funcionan de forma asíncrona por default, pero también se pueden ejecutar de forma síncrona, por ejemplo el método `readFile()` tiene su versión síncrona `readFileSync()`.

**Naranja.txt:**
Naranja dulce
Limon partido
Dame un abrazo
Que yo te pido
Si fuera falso
Mis juramentos
En otros tiempos
Se olvidaran
Toca la marcha
Mi pecho llora
Adios senora
Yo ya me voy
A mi casita
De sololoy
A comer tacos
Y no le doy

![img](https://www.google.com/s2/favicons?domain=https://nodeschool.io//favicon.ico)[NodeSchool](https://nodeschool.io/)

## Administrar directorios y archivos

Pasados los vectores como parametros en el comando de ejecucion:

```js
const fs = require('fs');

const arg1= process.argv[2];

const arg2= process.argv[3];

fs.copyFile(arg1, arg2, error => {
    if (error) {
        console.log(error);
    }
    console.log(`${arg1} fue copiado como ${arg2}`);
})
```

Para convertir a promesas:

```js
const fs = require('fs/promises');

(async function () {
  try {
    const files = await fs.readdir(__dirname);
    console.log(files);
  } catch (err) {
    console.error(err);
  }

  try {
    await fs.mkdir('platzi/escuela-js/node', { recursive: true });
  } catch (err) {
    console.error(err);
  }

  try {
    await fs.copyFile('naranja.txt', 'limon.txt');
    console.log('naranja.txt fue copiado como limon.txt');
  } catch (err) {
    console.error(err);
  }
})();
```

## Código de la clase

- **readdir.js**

```js
const fs = require('fs')

const files = fs.readdir(__dirname, (err, files) => {
    if (err) {
        return console.error(err)
    }

    console.log(files)
})
```

- **mkdir.js**

```js
const fs = require('fs')

fs.mkdir('carpeta-no-recursiva', err => (err) ? console.error(err) : '')
fs.mkdir('nueva-carpeta/otra-carpeta', { recursive: true }, err => (err) ? console.error(err) : '')
```

- **copy.js**

```js
const fs = require('fs')

fs.copyFile('naranja.txt', 'limon.txt', err => (err) ? console.error(err) : console.log('Naranja.txt fue copiado como limon.txt'))
```

## Consola, utilidades y debugging

El módulo útil esta diseñado para resolver las necesidades internas de las API de Node, sin embargo muchas de estas utilidades también son útiles para los módulos de las aplicaciones en desarrollo. Se puede acceder a estas utilidades usando:
`const util = require('util');`

**util.format()**
El método util.format () devuelve una cadena formateada utilizando el primer argumento como una cadena de formato tipo printf que puede contener cero o más especificadores de formato. Cada especificador se reemplaza con el valor convertido del argumento correspondiente. Los especificadores compatibles son:

- %s - String
- %d - Number
- %i - parseInt(value, 10)
- %f - parseFloat(value)
- %j - JSON
- %o - Object
- %c - Css
- %% - signo de '%'
  **Inspector**
  cuando se inicia la inspección `--inspect`, Node escucha a un cliente de depuración , Por defecto escuchara el host y el puerto `127.0.0.1:9229` y a cada proceso se le asigna un id único.
  **Opciones de la línea de comandos**
  `--inspect`: Habilita el agente de inspección y escucha el puerto por defecto 127.0.0.1:9229
  `--inspect=[host:port]`: Habilita el agente de inspección, vincula la dirección y el puerto a la dirección de inspección.

**Reto (estuvo super interesante me costo mucho trabajo)**

```js
const log = new console.Console({
  stdout: process.stdout,
  strerr: process.strerr
});

const emojis = {
  log: "  \u001b[37m ☺",
  danger: "  \u001b[31m ☠",
  info: "  \u001b[34m ℹ",
  warning: "  \u001b[33m ⚠",
  love: "  \u001b[35m ❤"
};

console.Console.prototype.logger = function(text) {
  this.log(`${emojis.log} ${text}`);
};

console.Console.prototype.warning = function(text) {
  this.warn(`${emojis.warning} ${text}`);
};

console.Console.prototype.information = function(text) {
  this.info(`${emojis.info} ${text}`);
};

console.Console.prototype.danger = function(text) {
  this.error(`${emojis.danger} ${text}`);
};

console.Console.prototype.love = function() {
  this.error(`${emojis.love} This was made with love for you`);
};

log.logger("this is a normal log");
log.information("this is a info log");
log.warning("this is a warning log you should read it");
log.danger("this is a error log, You are in fire");
log.love();
```

**output**
![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-96668d2f-ae5e-443a-ad9c-0e269dda8945.jpg)



[Console | Node.js v12.9.0 Documentation](https://nodejs.org/api/console.htmlDebugger) 

[| Node.js v12.9.0 Documentation](https://nodejs.org/api/debugger.htmlUtil) 

[| Node.js v12.9.0 Documentation](https://nodejs.org/api/util.html)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[GitHub - workshopper/learnyounode: Learn You The Node.js For Much Win! An intro to Node.js via a set of self-guided workshops](https://github.com/workshopper/learnyounode)

## Clusters y procesos hijos

Una sola instancia de Node.js corre un solo hilo de ejecución. Para tomar ventaja de los sistemas con multiples core, necesitamos lanzar un cluster de procesos de Node.js para manejar la carga.

El módulo cluster nos permite la creación fácil de procesos hijos que comparten el mismo puerto del servidor. Veamos un ejemplo en código:

```javascript
const cluster = require("cluster");
const http = require("http");


// Requerimos la cantidad de CPUs que tiene la maquina actual
const numCPUs = require("os").cpus().length;


if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);


  // Si el cluster es maestro, creamos tantos procesos como numero de CPUS
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }


  // Si por alguna razón el cluster se finaliza hacemos un log
  cluster.on("exit", (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Los diferentes workers pueden compartir la conexión TCP
  // En este caso es una servidor HTTP
  http
    .createServer((req, res) => {
      res.writeHead(200);
      res.end("hello world\n");
    })
    .listen(8000);


  console.log(`Worker ${process.pid} started`);
}
```

Si corremos nuestro archivo de Node.js ahora compartirá el puerto 8000 con los diferentes workers:

```javascript
$ node server.js
Master 3596 is running
Worker 4324 started
Worker 4520 started
Worker 6056 started
Worker 5644 started
```

En Windows, todavía no es posible establecer un nombre de proceso server en un worker.

[clusters y processos hijos](https://pinchito.es/2013/modo-cluster) 

# 4. Crea tu primer proyecto en Express.js

## ¿Qué es Express.js y para qué sirve?

Express.js es un framework para crear **Web Apps**, **Web APIs** o cualquier tipo de **Web Services**, es libre bajo la licencia MIT.

Express es muy liviano y minimalista además de ser extensible a través de Middlewares.

Los Middlewares interceptan el request y el response para ejecutar una acción en medio.

**¿Que es Express.js?**
Express es un framework o marco de trabajo desarrollado sobre Nodejs, lo que quiere decir que provee toda la funcionalidad que necesitas para hacer aplicaciones web, permitiendo estructurar tu proyecto de una manera más fácil.
Express.js es un Framework, del creador T.J. Holowaychuk, , inspirado en el framework Sinatra de Ruby. Es considerado un framework minimalista, es decir que viene con funcionalidades muy básicas, que luego a medida que crezca la aplicación se puede ir extendiendo a través de los módulos que puedes encontrar en npm, Además que es el framework de Node.js Más popular.

**Template:** Como su nombre en espanol lo dice, es una plantilla que podemos usar para apartir de ahi desarrollar nuestro proyecto.

**Routing:** Es el sistema de rutas que maneja express.js para manejar las rutas de una manera mas facil y sencilla, asi podremos ejecutar o mostrar un codigo en determinada ruta o proceso.

**Middlewares:** Las funciones de middleware son funciones que tienen acceso al objeto de solicitud (req), al objeto de respuesta (res), esto quiere decir que es auqella funcion que esta en medio del req y el res, y con la cual podemos manipular los datos antes de que lleguen a su destino.

caracteristicas

- minimalista
- template engines
- routing (uso de expreciones regulares)
- middlewares
- plugins

> **Express** es un framework o marco de trabajo desarrollado sobre Nodejs, lo que quiere decir que provee toda la funcionalidad que necesitas para hacer aplicaciones web, permitiendo estructurar tu proyecto de una manera más fácil.

Express.js es un framework open-source para crear aplicaciones web, web services o web APIs.
Sus principales características son:

1. Minimalista.
2. Tiene template engines.
3. Routing.
4. Middlewares.
5. Plugins.

![img](https://www.google.com/s2/favicons?domain=http://sinatrarb.com//sinatra.github.com/images/favicon.ico)[Sinatra](http://sinatrarb.com/)

## Creando tu primer servidor con Express.js

**dotenv**
Es un módulo independiente que carga variables de entorno de un archivo .env en process.env.

```npm
npm i express dotenv
```

**Nodemon**
Nodemon es una utilidad que supervisará cualquier cambio en los recursos y reiniciará automáticamente su servidor.

```bash
npm i -D nodemon eslint eslint-config-prettier eslint-plugin-prettier prettier
```

**Husky hooks**
Es un módulo que puede prevenir realizar **git commit** o **git push** sin formato u otros conflictos no deseados.

```sh
npx mrm lint-staged
```



```js
const express = require('express');
const app = express();
const { config } = require('./config/index');

const divider = (year, divisor) => {
  return year % divisor === 0 ? true : false;
};

app.get('/', (request, response) => {
  response.send(`Place a year after the url and find out if it is leap year: 
  http://localhost:${config.port}/1994`);
});

/**
 * The year is leap year if
 * first - it is divisible by 4
 * second - it is not divisible by 100
 * third - or it is divisible by 400
 */
app.get('/:year', (request, response) => {
  const { year } = request.params;
  if ((divider(year, 4) && !divider(year, 100)) || divider(year, 400)) {
    response.send(`The year 
    ${year} is leap year :D`);
  }
  response.send(`The year 
  ${year} is NOT leap year :C`);
});

app.listen(config.port, () => {
  console.log(`listening address http://localhost:${config.port}`);
});
```

Para los que le de error DEBUG o NODE_ENV en el package.json en Windows.
SOLUCION:

```json
"scripts": {
    "dev": "SET DEBUG=app:* & nodemon index",
    "start": "SET NODE_ENV=production & node index"
  },
```

Configuracion .eslintrc.json

```json
{
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "extends": [
    "eslint:recommended",
    "prettier"
  ],
  "env": {
    "es6": true,
    "node": true,
    "mocha": true
  },
  "rules": {
    "no-console": "warn"
  }
}
```

> ```sh
> npx mrm@2 lint-staged
> ```
>
> Instala la versión 2 de mrm porque la actual, la versión 3, es incompatible con lint-staged

<img src="https://jakob101.gallerycdn.vsassets.io/extensions/jakob101/relativepath/1.4.0/1524403325976/Microsoft.VisualStudio.Services.Icons.Default" alt="img" style="zoom: 25%;" />[Relative Path - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=jakob101.RelativePath)

## Request y Response Objects

El objeto `req` (Request) en Express representa el llamado HTTP y tiene diferentes propiedades del llamado, como la cadena de texto *query* (Query params), los parámetros de la URL (URL params), el cuerpo (Body), los encabezados (HTTP headers), etc.

Para acceder al `req` basta con acceder al primer parámetro de nuestros router handlers (router middleware) ó middleware.

Como por ejemplo así lo hemos visto siempre:

```javascript
app.get("/user/:id", function(req, res) {
  res.send("user " + req.params.id);
});
```

Pero también funcionaria sin problemas:

```javascript
app.get("/user/:id", function(request, response) {
  response.send("user " + request.params.id);
});
```

## Exploremos las propiedades más importantes

### req.body

Contiene los pares de llave-valor de los datos enviados en el cuerpo (body) del llamado (request). Por defecto es `undefined` pero es establecido cuando se usa algún “body-parser” middleware como `body-parser` y `multer`.

En **Postman** cuando hacemos un *request* y enviamos datos en la pestaña **Body**, estos middlewares son los que nos ayudan a entender el tipo de datos que vamos a recibir en el `req.body`.

Aquí podemos ver como se pueden usar estos middlwares para establecer el valor del `req.body`:

```javascript
const app = require("express")();
const bodyParser = require("body-parser");
const multer = require("multer");
const upload = multer(); // Para datos tipo multipart/form-data

app.use(bodyParser.json()); // Para datos tipo application/json
app.use(bodyParser.urlencoded({ extended: true })); // Para datos tipo application/x-www-form-urlencoded

app.post("/profile", upload.array(), function(req, res, next) {
  console.log(req.body);
  res.json(req.body);
});
```

> Más información sobre los diferentes formatos que puede tener el body: https://developer.mozilla.org/es/docs/Web/HTTP/Methods/POST

### req.params

Esta propiedad contiene un objeto con las propiedades equivalentes a los parámetros nombrados en la ruta. Por ejemplo, si tenemos una ruta de la forma `/user/:name` entonces la propiedad `name` está disponible como `req.params.name` y allí podremos ver su valor. Supongamos que llamaramos a la ruta con `/user/glrodasz`, entonces el valor de `req.params.name` sería `glrodasz`. Este objeto por defecto tiene el valor de un objeto vacío `{}`.

```javascript
// GET /user/glrodasz
req.params.name;
// => "glrodasz"
```

### req.query

Esta propiedad contiene un objeto con las propiedades equivalentes a las cadenas de texto *query* de la ruta. Si no hay ninguna cadena de texto *query* tendrá como valor por defecto un objeto vacío `{}`.

```javascript
req.query.q;
// => "tobi ferret"

// GET /shoes?order=desc&shoe[color]=blue&shoe[type]=converse
req.query.order;
// => "desc"

req.query.shoe.color;
// => "blue"

req.query.shoe.type;
// => "converse"
```

> Más información sobre los query strings en: https://es.wikipedia.org/wiki/Query_string y https://tools.ietf.org/html/rfc3986#section-3.4

# Response Object

El objeto `res` representa la respuesta HTTP que envía una aplicación en Express.

Para acceder al `res` basta con acceder al segundo parámetro de nuestros *router handlers* (router middleware) o *middleware*.

Como por ejemplo así lo hemos visto siempre:

```javascript
app.get("/user/:id", function(req, res) {
  res.send("user " + req.params.id);
});
```

Pero también funcionaría sin problemas:

```javascript
app.get("/user/:id", function(request, response) {
  response.send("user " + request.params.id);
});
```

## Exploremos los métodos más comunes

### res.end()

Finaliza el proceso de respuesta. Este método viene realmente del *core* de Node.js, específicamente del método `response.end()` de `http.ServerResponse`.

Se usa para finalizar el *request* rápidamente sin ningún dato. Si necesitas enviar datos se debe usar `res.send()` y `res.json()`.

```javascript
res.end();
res.status(404).end();
```

### res.json()

Envía una respuesta JSON. Este método envía una respuesta (con el content-type correcto) y convierte el parámetro enviado a una cadena de texto JSON haciendo uso de `JSON.stringify()`.

El parámetro puede ser cualquier tipo de JSON, incluido un objeto, un arreglo, una cadena de texto, un *boolean*, número, *null* y también puede ser usado para convertir otros valores a JSON.

```javascript
res.json(null);
res.json({ user: "tobi" });
res.status(500).json({ error: "message" });
```

### res.send()

Envía una respuesta HTTP. El parámetro `body` puede ser un objeto tipo *Buffer*, una cadena de texto, un objeto, o un arreglo. Por ejemplo:

```javascript
res.send(Buffer.from("whoop"));
res.send({ some: "json" });
res.send("<p>some html</p>");
res.status(404).send("Sorry, we cannot find that!");
res.status(500).send({ error: "something blew up" });
```

# 5. Aprende a crear un API con REST

## Anatomía de una API Restful

**REST (Representational State Transfer)** es un estilo de arquitectura para construir web services, no es un estándar pero si una especificación muy usada.

Las peticiones HTTP van acompañadas de un “verbo” que define el tipo de petición:

- **GET**. Lectura de datos.
- **PUT**. Reemplazar datos.
- **PATCH**. Actualizar datos en un recurso específico.
- **POST**. Creación de datos.
- **DELETE**. Eliminación de datos.

No es recomendable habilitar un endpoint de tipo PUT y DELETE para toda nuestra colección de datos, sólo hacerlos para recursos específicos, ya que no queremos que por error se puedan borrar todos nuestros datos.

> CRUD: Create - Read- Update - Delete

<img src="https://i.ibb.co/NxL3jtk/rest.jpg" alt="rest" border="0">

![img](https://www.google.com/s2/favicons?domain=https://www.getpostman.com//favicon.ico)[Postman | API Development Environment](https://www.getpostman.com/)

![img](https://www.google.com/s2/favicons?domain=https://swagger.io//swagger/media/assets/swagger_fav.png)[The Best APIs are Built with Swagger Tools | Swagger](https://swagger.io/)

## Estructura de una película con Moockaru

*Mockaroo** es un servicio que nos permite crear datos simulados a partir de una estructura, por ejemplo para generar la estructura de nuestra película:

```javascript
{
    id: 'd2a4a062-d256-41bb-b1b2-9d915af6b75e',
    title: 'Notti bianche, Le (White Nights)',
    year: 2019,
    cover: 'http://dummyimage.com/800x600.png/ff4444/ffffff',
    description:
      'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
    duration: 66,
    contentRating: 'G',
    source: 'https://ovh.net/semper/rutrum/nulla/nunc.jsp',
    tags: [
      'Action|Adventure',
      'Action|Adventure|Thriller',
      'Horror|Western',
      'Horror|Thriller',
      'Comedy|Romance|Sci-Fi',
      'Adventure|Animation|Children|Comedy|Fantasy',
      'Drama'
    ]
  }
```

Lo que podemos hacer en Mockaroo es seleccionar la siguiente estructura:

<img src="https://i.ibb.co/Vmjp3Zm/moock.webp" alt="moock" border="0">

Luego seleccionamos el número de filas (rows) que queremos generar y elegimos el formato, en este caso será de tipo JSON. Podemos hacer clic en *preview* para ver cómo queda y finalmente para descargar los datos hacemos clic en **Download Data**.

<img src="https://i.ibb.co/vBMxJvr/moock1.webp" alt="moock1" border="0">

[Moockaroo](https://mockaroo.com/)

## Implementando un CRUD en Express.js

Trabajar con express es facil, me gusta 😄

```js
/*
  Creando rutas con express
*/
const express = require('express');
const { moviesMock } = require('../utils/mocks/movies');

function moviesApi(app){
  /*
    Middleware de rutas, siempre se va a consultar la ruta
    /api/movies antes que cualquier otra.
  */
  const router = express.Router();
  app.use('/api/movies', router);

  router.get('/', async function(req, res, next){
    try {
      const movies = await Promise.resolve(moviesMock);

      res.status(200).json({
        data: movies,
        message: 'Movies listed'
      });

    } catch(error){
      next(error);
    }
  });
}

module.exports = moviesApi;
```

**TIP:**

En el caso de que algún endpoint no regreses ningún dato (como en el caso de DELETE), lo mejor es regresar un status 204 (No content), no 200. No afecta en nada, pero es el estándar.

<img src="https://i.ibb.co/VYMnFL7/https.jpg" alt="https" border="0">

Las siglas CRUD vienen de las palabras en inglés:

- Create - crear
- Read - leer
- Update - actualizar
- Delete - eliminar



 [🕵️‍♂️ | Códigos de estado de respuesta HTTP](https://github.com/for-GET/http-decision-diagram)

[Códigos de estado de respuesta HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Status)

## Métodos idempotentes del CRUD

Así se vería el método del año bisiesto en una ruta

```js
const express = require('express');

function yearApi(app) {
  const router = express.Router();
  app.use('/', router);
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());

  router.get('/year/:year', (req, res, next) => {
    try {
      const { year } = req.params;
      const isFourDivisible = !(year % 4);
      const isHundredDivisible = !(year % 100);
      const isFourHundredDivisible = !(year % 400);

      if (isFourDivisible && !isHundredDivisible || isFourHundredDivisible) {
        res.send(`El año ${year} es bisiesto`);
      } else {
        res.send(`El año ${year} no es bisiesto`);
      }
    } catch (error) {
      next(error);

    }
  });

}

module.exports = yearApi;
```

Otra forma de generar el gitignore seria usando:

```sh
npx gitignore node
```

También funciona para crear el archivo de licencia:

```sh
npx license MIT
```

![img](https://www.google.com/s2/favicons?domain=//ssl.gstatic.com/docs/doclist/images/infinite_arrow_favicon_5.ico)[Postman - Google Drive](https://drive.google.com/drive/folders/1Latsb5hLuGS9XuLprGtbpgL3NLGnxh6O?usp=sharing)

## Implementando una capa de servicios

La arquitectura tradicional MVC se queda corta en aplicaciones modernas, por eso necesitamos una arquitectura diferente cómo la **Clean Arquitecture** que tiene una capa de servicios para manejar la lógica de negocio.

<img src="https://i.ibb.co/rF2pT4C/cleanarchitecture.png" alt="cleanarchitecture" border="0">

Recordar que la diferencia entre PUT y PATH es:

- PUT: remplazo total del recurso.
- PATH: actualización de algunos elementos del recurso.
  https://medium.com/backticks-tildes/restful-api-design-put-vs-patch-4a061aa3ed0b

**Mi solución al desafío:**

*routes/movies.js*

```js
	router.patch("/:movieId", async function(req,res,next) {
		const {movieId} = req.params;
		const {body: movie} = req;		
		try {
			const updatedMovieId = await moviesService.partialUpdateMovie({movieId,movie});

			res.status(200).json({
				data:updatedMovieId,
				message: "movie updated partially"
			});
		}
		catch(error) {
			next(error);
		}
	});
```

*services/movies.js*

```js
	async partialUpdateMovie() {
		const updatedMovieId = await Promise.resolve(moviesMock[0].id);
		return updatedMovieId;
	}
```

![img](https://www.google.com/s2/favicons?domain=https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html/assets/clean_code_72_color.png)[Clean Coder Blog](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

![img](https://www.google.com/s2/favicons?domain=https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico)[Ryan Florence - ‹Rethinker stop={false}/› - YouTube](https://www.youtube.com/watch?v=kp-NOggyz54)

![img](https://www.google.com/s2/favicons?domain=https://styde.net/porque-laravel-no-es-mvc-y-tu-deberias-olvidarte-de-mvc//images/favicon-196x196.png)[Por qué Laravel NO es un framework MVC y tú deberías olvidarte de MVC – Styde.(https://styde.net/porque-laravel-no-es-mvc-y-tu-deberias-olvidarte-de-mvc/)

![img](https://www.google.com/s2/favicons?domain=//abs.twimg.com/favicons/favicon.ico)[Taylor Otwell 🏝 on Twitter: "I really think the acronym "MVC" has become incredibly unhelpful in development, and instead we should just teach "separation of concerns"](https://twitter.com/taylorotwell/status/262290285499936768)

# 6. Cómo conectarse con librerías externas en Express.js

## Creación de una BD en MongoAtlas

MongoDB está escrito en C++, aunque las consultas se hacen pasando objetos JSON como parámetro. Es algo bastante lógico, dado que los propios documentos se almacenan en BSON. Por ejemplo:

db.Clientes.find({Nombre:“Pedro”});
La consulta anterior buscará todos los clientes cuyo nombre sea Pedro.

MongoDB viene de serie con una consola desde la que podemos ejecutar los distintos comandos. Esta consola está construida sobre JavaScript, por lo que las consultas se realizan utilizando ese lenguaje. Además de las funciones de MongoDB, podemos utilizar muchas de las funciones propias de JavaSciprt. En la consola también podemos definir variables, funciones o utilizar bucles.

Si queremos usar nuestro lenguaje de programación favorito, existen drivers para un gran número de ellos. Hay drivers oficiales para C#, Java, Node.js, PHP, Python, Ruby, C, C++, Perl o Scala. Aunque estos drivers están soportados por MongoDB, no todos están en el mismo estado de madurez. Por ejemplo el de C es una versión alpha. Si queremos utilizar un lenguaje concreto, es mejor revisar los drivers disponibles para comprobar si son adecuados para un entorno de producción.

> Muchas personas creen que las Bases de datos **No relacionales** son el remplazo de las bases de datos **Relacionales**, y la verdad es que no. Aplican para resolver distintos tipos de problemas, he incluso muchos sistemas complejos hacen uso de las dos.

![img](https://www.google.com/s2/favicons?domain=https://www.mongodb.com/cloud/atlas/assets/images/global/favicon.ico)[Managed MongoDB Hosting on AWS, Azure, and GCP | MongoDB](https://www.mongodb.com/cloud/atlas)

## Conexión a MongoAtlas una instancia de MongoDB

Instalamos mongodb en el proyecto.

```sh
npm install mongodb
```




## Conexión con Robot3T y MongoDB Compass a una BD


## Implementación de las acciones de MongoDB


## Conexión de nuestros servicios con MongoDB

# 7. Conoce como funcionan los Middleware en Express.js


## ¿Qué es un middleware? Capa de manejo de errores usando un middleware


## Manejo de errores asíncronos y síncronos en Express


## Capa de validación de datos usando un middleware


## ¿Qué es Joi y Boom?


## Implementando Boom


## Implementando Joi


## Probar la validación de nuestros endpoints


## Middlewares populares en Express.js

# 8. Implementa tests en Node.js


## Creación de tests para nuestros endpoints


## Creación de tests para nuestros servicios


## Creación de tests para nuestras utilidades


## Agregando un comando para coverage


## Debugging e inspect

# 9. Despliega tu primera aplicación en Express.js


## Considerando las mejores prácticas para el despliegue


## Variables de entorno, CORS y HTTPS


## ¿Cómo implementar una capa de manejo de caché?


## ¿Cómo contener tu aplicación en Docker?


## Despliegue en Now

# 10. Conclusiones


## ¿Qué aprendiste en este curso?