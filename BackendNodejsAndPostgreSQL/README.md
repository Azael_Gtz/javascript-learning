<h1>Backend con Node.js: Base de Datos con PostgreSQL</h1>

<h3>Nicolas Molina</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Persistencia de datos en Node.js](#persistencia-de-datos-en-nodejs)
  - [Platzi Store: instalación y presentación del proyecto](#platzi-store-instalación-y-presentación-del-proyecto)
    - [Software para testing web](#software-para-testing-web)
- [2. Base de datos](#2-base-de-datos)
  - [Instalación de Docker](#instalación-de-docker)
  - [Instalación en Windows 🏡](#instalación-en-windows-)
  - [Instalación en Windows con WSL 🐧](#instalación-en-windows-con-wsl-)
  - [Instalación en macOS 🍎](#instalación-en-macos-)
  - [Instalación en Ubuntu 🐧](#instalación-en-ubuntu-)
  - [Configuración de Postgres en Docker](#configuración-de-postgres-en-docker)
  - [Explorando Postgres: interfaces gráficas vs. terminal](#explorando-postgres-interfaces-gráficas-vs-terminal)
    - [Comandos:](#comandos)
  - [Integración de node-postgres](#integración-de-node-postgres)
    - [Serverless](#serverless)
    - [Clean Architecture](#clean-architecture)
    - [Definición de arquitectura](#definición-de-arquitectura)
      - [libs - directorio de drivers connection](#libs---directorio-de-drivers-connection)
      - [services - directorio de controllers](#services---directorio-de-controllers)
      - [routes - endpoints definition](#routes---endpoints-definition)
  - [Manejando un Pool de conexiones](#manejando-un-pool-de-conexiones)
  - [Variables de ambiente en Node.js](#variables-de-ambiente-en-nodejs)
    - [Unit Test](#unit-test)
    - [Configuración](#configuración)
  - [Variables de ambiente en Node.js](#variables-de-ambiente-en-nodejs-1)
      - [Instalamos el siguiente paquete](#instalamos-el-siguiente-paquete)
      - [Adaptamos nuestro pool](#adaptamos-nuestro-pool)
      - [Archivo .env](#archivo-env)
      - [Archivo .env.example](#archivo-envexample)
- [3. Sequelize](#3-sequelize)
  - [¿Qué es un ORM? Instalación y configuración de Sequelize ORM](#qué-es-un-orm-instalación-y-configuración-de-sequelize-orm)
    - [ORM](#orm)
  - [Tu primer modelo en Sequelize](#tu-primer-modelo-en-sequelize)
    - [Configuración de ORM](#configuración-de-orm)
  - [Crear, actualizar y eliminar](#crear-actualizar-y-eliminar)
    - [CRUD Operations](#crud-operations)
  - [Cambiando la base de datos a MySQL🐬](#cambiando-la-base-de-datos-a-mysql)
- [4. Migraciones](#4-migraciones)
  - [¿Qué son las migraciones? Migraciones en Sequelize ORM](#qué-son-las-migraciones-migraciones-en-sequelize-orm)
  - [Configurando y corriendo migraciones con npm scripts](#configurando-y-corriendo-migraciones-con-npm-scripts)
  - [Modificando una entidad](#modificando-una-entidad)
    - [💾 DB Generation](#-db-generation)
    - [Generación de scripts para `migration` y `seeds`](#generación-de-scripts-para-migration-y-seeds)
- [5. Relaciones](#5-relaciones)
  - [Relaciones uno a uno](#relaciones-uno-a-uno)
  - [Resolviendo las relaciones uno a uno](#resolviendo-las-relaciones-uno-a-uno)
  - [Relaciones uno a muchos](#relaciones-uno-a-muchos)
  - [Resolviendo relaciones uno a muchos](#resolviendo-relaciones-uno-a-muchos)
- [6. Consultas](#6-consultas)
  - [Órdenes de compra](#órdenes-de-compra)
  - [Relaciones muchos a muchos](#relaciones-muchos-a-muchos)
  - [Resolviendo relaciones muchos a muchos](#resolviendo-relaciones-muchos-a-muchos)
  - [Paginación](#paginación)
  - [Filtrando precios con operadores](#filtrando-precios-con-operadores)
- [7. Despliegue](#7-despliegue)
  - [Deploy en Heroku](#deploy-en-heroku)
  - [Consideraciones al hacer migraciones](#consideraciones-al-hacer-migraciones)
- [8. Próximos pasos](#8-próximos-pasos)
  - [Toma con el Curso de Backend con Node.js](#toma-con-el-curso-de-backend-con-nodejs)

# 1. Introducción

## Persistencia de datos en Node.js

[sides-del-curso-de-backend-con-node-js-base-de-datos-con-postgresql.pdf](https://static.platzi.com/media/public/uploads/sides-del-curso-de-backend-con-node-js-base-de-datos-con-postgresql_8be993c7-398f-481d-ba8c-a4843dcae7f8.pdf)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Curso de Backend con Node.js: API REST con Express.js - Platzi](https://platzi.com/cursos/backend-nodejs)

## Platzi Store: instalación y presentación del proyecto

Clonar el repositorio 

```sh
# clonar y renombrar la carpeta
git clone git@github.com:platzi/curso-nodejs-postgres.git my-store
```

instalar las dependencias hice:

```sh
npm i
```



### Software para testing web

> 💡 Software con la capacidad de crear conexiones web y, en ocasiones, funge como un medio para el proceso de pruebas end-to-end.

- [Postmant](https://www.postman.com/). Plataforma tipo IDE colaborativa
- [Thunder Client](https://www.thunderclient.io/). Cliente web similar a Postman
- [Thunder Client](https://www.thunderclient.io/). Client para vscode

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-nodejs-postgres](https://github.com/platzi/curso-nodejs-postgres)

# 2. Base de datos

## Instalación de Docker

Según el sistema operativo que utilices puede variar la instalación, así que te doy las indicaciones base para la instalación:

## Instalación en Windows 🏡

Debes descargar el instalador desde la página de [Docker for Windows.](https://docs.docker.com/docker-for-windows/install/)

Una de las cosas que debes tener en cuenta en la instalación en Windows es que debes contar con **Windows 10 de 64 Bits** y debes habilitar el **[Hyper-V](https://docs.docker.com/docker-for-windows/troubleshoot/#virtualization)** de Windows.

![img](https://docs.docker.com/desktop/windows/images/hyperv-enabled.png)

## Instalación en Windows con WSL 🐧

Cuando tienes WSL2 debes asegurarte que tienes las siguientes características habilitadas:

![img](https://docs.docker.com/desktop/windows/images/wsl2-enable.png)

Y además habilitar WSL en Docker:

![img](https://docs.docker.com/docker-for-windows/images/wsl2-enable.png)

Puedes ver más detalles en [Docker Desktop WSL 2 backend](https://docs.docker.com/docker-for-windows/wsl/)

## Instalación en macOS 🍎

En Mac tienes dos opciones. Todo dependerá si tienes los nuevos chips M1 o Intel, ya que hay un instalable apropiado para ambas arquitecturas de chip. Puedes escoger el instalable desde [Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install/).

Adicionalmente si cuentas con los nuevos chips M1, debes ejecutar la siguiente instrucción en tu terminal `softwareupdate --install-rosetta`

Una vez descargues el instalador adecuado, solo debes seguir los pasos y pasar Docker a tus aplicaciones.

![img](https://docs.docker.com/docker-for-mac/images/docker-app-drag.png)

## Instalación en Ubuntu 🐧

Estos son los pasos para instalarlo dentro de Ubuntu sin embargo también puedes ver directamente [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo groupadd docker
sudo usermod -aG docker $USER
# reset terminal
```

Install Docker compose

```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
```

Para otras distribuciones de Linux:

- [Install Docker Engine on CentOS](https://docs.docker.com/engine/install/centos/)
- [Install Docker Engine on Debian](https://docs.docker.com/engine/install/debian/)
- [Install Docker Engine on Fedora](https://docs.docker.com/engine/install/fedora/)

## Configuración de Postgres en Docker

ℹ️**Repositorio**: 

```sh
https://github.com/roremdev/thingst
```

ℹ️**Commit**: 

```sh
https://github.com/roremdev/thingst/commit/88bee9878636286250d3a2b9426474cd8b17d0f2
```

♻️Doy unas recomendaciones:

1. En cuanto a la `version`, viene siendo opcional desde la verison `1.27.0` de Docker.
2. En cuanto a la `image`, puedes omitir `latest` o en su defecto seleccionar la versión que buscas:
   `image: resource_name:version`
3. Es recomendable leer desde las variables de entorno los datos sensibles, estando en dev o no. Opción `--env-file`
4. En cuanto al `ports`, cuando es el mismo (contenedor y host), basta con un solo número y sin `""`
5. En cuanto al `volumes`, no es necesario `~` ya que si quieres indexar desde un path se considera mejor `./`, detalles del server o imagen base de tu postgres (algunos son `alphine`).

✨Al fina puede quedar de la siguiente forma:

> ```
> docker-compose.yml
> ```

```yml
services:
    wordpress:
        image: postgres
        restart: always
        ports:
            - 5432
        environment:
            POSTGRES_DB: ${DB_NAME}
            POSTGRES_USER: ${DB_USER}
            POSTGRES_PASSWORD: ${DB_PASSWORD}
        volumes:
            - ./db:/var/lib/postgresql/data
```

.

> ```
> .env
> ```

```yml
# docker
DB_NAME=thingst
DB_USER=tester
DB_PASSWORD=14db7d47-f442-4a90-b633-2a537c8b5c13
```



Archivo `docker-file.yml`

```yml
version: '3.3'

services:
  postgres:
    image: postgres:latest
    restart: always
    environment:
      - POSTGRES_DB=my_store
      - POSTGRES_USER=victor
      - POSTGRES_PASSWORD=admin123
    ports:
      - "5433:5432"
    volumes:
      - ./postgres_data:/var/lib/postgresql/data

```

cambios en la seccion de `ports`

```yml
# Configuracion predeterminado
ports:
  - 5432:5432
# Modificacion, docker y postgres se ejecutan sin errores.
ports:
  - "5433:5432"
```

🔥Comando de ejecución

```bash
docker compose up
docker compose --env-file [dir_name] up
```

**Docker comando**

```bash
 # Iniciar proceso con Docker 🐳
 docker-compose up -d postgres

# Verificar que este ejecutandose 🐳
 docker-compose ps

# Detener docker 🐳
docker-compose down
```

## Explorando Postgres: interfaces gráficas vs. terminal

Terminal de `docker`

```sh
docker-compose exec postgres bash
# informacion de la maquina
root@c81dedb570c8:/# ls
bin   dev			  etc	lib    media  opt   root  sbin	sys  usr
boot  docker-entrypoint-initdb.d  home	lib64  mnt    proc  run   srv	tmp  var
```

Ingresar  a la base de datos

```sh
root@c81dedb570c8:/# psql -h localhost -d my_store -U victor
```

listar tablas

```sh
my_store=# \d+
Did not find any relations.
```

### Comandos:

- `ls -l` : ver todos los archivos
- `\d+` : Una vez logueado te va a mostrar la estructura de la base de datos
- `\q` : Salir de la base de datos
- `exit` : Nos salimos del contenedor
- `docer ps` : Nos muestra una tabla con los servicios
- `docker inspect <id>` : Nos da un detalle del contenedor

**Config file `dockerfile`**

```yml
version: '3.3'

services:
  postgres:
    image: postgres:latest
    restart: always
    environment:
      - POSTGRES_DB=my_store
      - POSTGRES_USER=victor
      - POSTGRES_PASSWORD=admin123
    ports:
      - "5433:5432"
    volumes:
      - ./postgres_data:/var/lib/postgresql/data

  pgadmin:
    image: dpage/pgadmin4
    environment:
      - PGADMIN_DEFAULT_EMAIL=admin@mail.com
      - PGADMIN_DEFAULT_PASSWORD=root
    ports:
      - 5050:80

```

**Correr docker pgadmind**

```sh
 docker-compose up -d pgamin
```

<img src="https://www.pgadmin.org/static/COMPILED/assets/img/postgres-alt.svg" style="zoom: 50%;" /> [pgAdmin](https://www.pgadmin.org/)

<img src="https://d36jcksde1wxzq.cloudfront.net/be7833db9bddb4494d2a7c3dd659199a.png" alt="img" style="zoom:25%;" />[dpage/pgadmin4](https://hub.docker.com/r/dpage/pgadmin4)

## Integración de node-postgres

Instalar `pg`

```sh
npm install pg
```

[node-postgres](https://node-postgres.com/)

### Serverless

> 💡Es un tipo de arquitectura que nos permite descentralizar los diferentes recursos existentes de nuestra aplicación.

En ocasiones, a serverless se le denomina sistemas distribuidos ya que permite, abstraer desde servidores hasta módulos denominados cloud functions.

Una de las principales ventajas de implementar serverless es la creación de arquitecturas como **cliente-servidor**, **micro-servicios**, entre otros.

### Clean Architecture

> 💡Es un conjunto de principios cuya finalidad principal es ocultar los detalles de implementación a la lógica de dominio de la aplicación.


Las principal característica de Clean Architecture frente a otras arquitecturas es la **regla de dependencia**.

En Clean Architecture, una aplicación se divide en responsabilidades y cada una de estas responsabilidades se representa en forma de capa.

### Definición de arquitectura

ℹ️Repositorio: https://github.com/roremdev/thingst
ℹ️Commit: https://github.com/roremdev/thingst/commit/ead31629469e5a3b923efc42b8b8eb5b18159b97

#### libs - directorio de drivers connection

> ```
> Postgres.js
> ```

```js
import { Pool as PostgresClient } from 'pg';

export default class Postgres {
    /**
     * @private
     * @description singleton pattern for pool connection
     * @returns {object} - connection client
     */
    async #connect() {
        try {
            if (!Postgres.connection) {
                Postgres.connection = new PostgresClient();
                console.log('Connected succesfully');
            }
            return Postgres.connection;
        } catch (error) {
            console.log(error);
        }
    }
    /**
     * @description query process in table
     * @param {string} request - SQL string request
     * @returns {Object} - response query postgresDB
     */
    async query(request) {
        try {
            const db = await this.#connect();
            return await db.query(request);
        } catch (error) {
            console.log(error);
        }
    }
}
```

ℹ️**Nota.** Se utiliza `Pool` para el manejo de múltiples conexiones por usuario donde se delega la administrador por el servidor.

#### services - directorio de controllers

> ```
> Resource.js
> ```

```js
import Postgres from '../libs/Postgres';

export default class ResourcesService {
    /**
     * @description DAO postgresDB tables
     * @param {string} table - table name
     */
    constructor(table) {
        this.table = table;
        this.client = new Postgres();
    }

    /**
     * @description find all registers in table
     * @returns {array} - response query mongoDB as array
     */
    async findAll() {
        const { rows } = await this.client.query(`SELECT * FROM ${this.table}`);
        return rows;
    }
}
```

.

#### routes - endpoints definition

.

> ```
> Resource.js
> ```

```js
import express from 'express';
import Resource from '../../services/Resource';

const router = express.Router();
const taskService = new Resource('tasks');

/**
 * @description get task operation
 * @param {string} path - express path
 * @param {callback} middleware - express generic middleware
 * @returns {ResponseObject}
 */
router.get('/', async (req, res, next) => {
    try {
        const data = await taskService.findAll();
        res.status(200).json({
            status: 'success',
            data,
            message: 'Retrieved all tasks',
        });
    } catch (error) {
        next(error);
    }
});

export default router;
```

## Manejando un Pool de conexiones

Un pool de conexiones es un conjunto limitado de conexiones a una base de datos, que es manejado por un servidor de aplicaciones de forma tal, que dichas conexiones pueden ser reutilizadas por los diferentes usuarios.

![img](https://i.stack.imgur.com/OOFTe.png)

[Documentación](https://node-postgres.com/features/pooling)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-nodejs-postgres at 5-step](https://github.com/platzi/curso-nodejs-postgres/tree/5-step)

## Variables de ambiente en Node.js

### Unit Test

> 💡Es el proceso de ejecutar un programa con la meta de encontrar errores. Si la “prueba” es exitosa, entonces podemos asegurar que, alguna característica o funcionalidad, fue cubierta.


Para este caso, sugiero que implementen sus pruebas ya sea de ***unit test\*** (función o módulo) y/o ***end-to-end\*** (integración).

### Configuración

ℹ️Repositorio: [link](https://github.com/roremdev/thingst/tree/dev)

Para adicionar pruebas a nuestro código utilizamos:

- [jest](https://jestjs.io/). Engine de pruebas para JavaScript
- [supertes](https://www.npmjs.com/package/supertest). Handler Process para peticiones HTTP.

✨Como retornamos un arreglo como `data` para validar el cambio de pool podemos definir el siguiente bloque:

```js
import supertest from 'supertest';
import app from '../src/index';

const request = supertest(app);

/**
 * @description colleciton of test cases on task request
 * @param {string} - case name
 */
describe('routes', () => {
    describe('GET /task', () => {
        it('should respond with a json', async () => {
            const { status, body: response } = await request.get('/task');
            expect(status).toBe(200);
            expect(Array.isArray(response.data)).toBeTruthy();
        });
    });
});
```

♻️Ambas librerías para pruebas, necesitamos obtener un servidor http y express no los retorna cuando utilizamos `express()`.

```js
import express from 'express';
import task from './routes/api/task';

// code
const app = express();

// code
app.use('/task', task);

// code
if (mode !== 'test')
    app.listen(port, () => listen(`⬢ Server Thingst - ${mode}`));
export default app;
```

🔥Evitamos que en `test` no levante el servidor, ya que `supertest` lo hará por nosotros.

♻️Actualizamos nuestro `script` para leer nuestros test:

```json
"test": "NODE_ENV=test jest test/index.test.js --forceExit",
```

## Variables de ambiente en Node.js

Las variables de entorno se utilizan para contener contenido sensible

#### Instalamos el siguiente paquete

Esto nos sirve para tener las variables de entorno corriendo en el proceso de node

```sh
npm i dotenv
```

Creamos nuestro archivo de configuracion `config>config.js`

```jsx
require('dotenv').config();

const config = {
  env: process.env.NODE_ENV || 'dev',
  port: process.env.PORT || 3000,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_PASSWORD,
  dbHost: process.env.DB_HOST,
  dbName: process.env.DB_NAME,
  dbPort: process.env.DB_PORT,
};

module.exports = { config };
```

#### Adaptamos nuestro pool

Archivo anterior

```jsx
const { Pool } = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  user: 'kevin',
  password: 'admin123',
  database: 'my_store',
});

module.exports = pool;
```

Archivo usando las variables de entorno

La URI se conforma por “`protocolo`😕/`USUARIO`:`CONTRASEÑA`@`HOST`:`PUERTO`/`NOMBREDB`”

El **encodeURI** es una manera de proteger estos datos tan sensibles, lo mismo cuando ya tenemos la **URI** armada

```jsx
const { Pool } = require('pg');
const { config } = require('../config/config');

const USER = encodeURIComponent(config.dbUser);
const PASSWORD = encodeURIComponent(config.dbPassword);
const URI = `postgres://${USER}:${PASSWORD}@${config.dbHost}:${config.dbPort}/${config.dbName}`;

const pool = new Pool({ connectionString: URI });

module.exports = pool;
```

#### Archivo .env

```jsx
	PORT=3000
  DB_USER='kevin'
  DB_PASSWORD='admin123'
  DB_HOST='localhost'
  DB_NAME='my_store'
  DB_PORT='5432'
```

#### Archivo .env.example

```jsx
	PORT='',
  DB_USER='',
  DB_PASSWORD'',
  DB_HOST'',
  DB_NAME='',
  DB_PORT='',
```

# 3. Sequelize

## ¿Qué es un ORM? Instalación y configuración de Sequelize ORM

### ORM

> 💡Un ORM es un modelo de programación que permite mapear las estructuras de una base de datos relacionales.

Al abstraer este tipo de programación, delegamos su implementación al backend, es decir, le añadimos una de responsabilidad a la capa transaccional del servidor:

✨Los beneficios son los siguientes:

- Acciones como ***CRUD\*** (Create, Read, Update, Delete) son administradas mediante ORM.
- La implementación de ***seeds\*** o semillas, nos permiten recuperar, mediante código, la estructura de una BD.

Una de las bases teóricas para entender este modelo es mediante el conocimiento de ***DAO\*** (Data Access Object) y ***DTO\*** (Data Transfer Object), los cuales nos permiten desestructurar un ORM en módulos de abstracción para acceder a la DB y transferir datos desde la misma DB, respectivamente hablando.

🙃Los contras sería:

- Delegación de responsabilidades al server
- Descentralización de trabajo, directa, de una BD.

comandos

```sh
 npm install --save sequelize

npm install --save pg-hstore
```

**[Documentación de Sequelize](https://sequelize.org/)**

## Tu primer modelo en Sequelize

🔥Nuestra [documentación](https://sequelize.org/master/manual/model-basics.html) nos permite aclarar unos detalles que quedaron volando:

- Podemos utilizar `define` o `extend Model`.

- Se infiere el nombre de la tabla y se generaliza en plural, `sequelize.define('user', userSchema);` definirá la tabla `users`.

- Existe el modo de sincronización global

   

  ```
  sequelize.sync()
  ```

   

  o particular modelo.sync()` donde tendremos parámetros:

  - `force: true`. Eliminar la existencia previa y creando en secuencia.
  - `alter: true`. Revisa si se cambiará alguna estructura, la nueva vs la previa.

.

### Configuración de ORM

ℹ️Repositorio: [link](https://github.com/roremdev/thingst/tree/orm)
.
♻️Reutilizando las variables de entorno, las organizamos para determinar aquellas para `database`.

> `config.js` . Archivo de definición de variables de entorno

```
/**
 * @description variables of database server
 */
export const database = {
    dbName: process.env.PGDATABASE,
    dbHost: process.env.PGHOST || 'localhost',
    dbPort: process.env.PGPORT || '5432',
    dbUser: process.env.PGUSER,
    dbPassword: process.env.PGPASSWORD,
};
```

.
♻️Definimos la librería de `sequelize` mediante la abstracción OOP.

> `Sequelize.js` . Archivo de definición de variables de entorno

```js
import { Sequelize as SequelizeClient } from 'sequelize';
import { database } from '../config/config';
import Error from '../utils/Error';

const USER = encodeURIComponent(database.dbUser);
const PASSWORD = encodeURIComponent(database.dbPassword);
const URI = `postgres://${USER}:${PASSWORD}@${database.dbHost}:${database.dbPort}/${database.dbName}`;

export default class Sequelize {
    /**
     * @private
     * @description singleton pattern for pool connection
     * @returns {object} - connection client
     */
    async #connect() {
        try {
            if (!Sequelize.connection) {
                Sequelize.connection = new SequelizeClient(URI, {
                    logging: false,
                });
                await Sequelize.connection.authenticate();
            }
            return Sequelize.connection;
        } catch ({ message }) {
            throw new Error(message, 'DRIVER');
        }
    }
    /**
     * @description process definition for create database tables
     * @param {string} name - table name
     * @param {string} schema - table description
     * @returns {Promise} - response of library
     */
    async define(name, schema) {
        try {
            const db = await this.#connect();
            const model = await db.define(name, schema);
            return await model.sync();
        } catch (error) {
            if (!error) throw new Error(message, 'DEFINITION');
            throw error;
        }
    }
}
```

.
✨Creamos el schema

> ```
> user.js
> ```

```js
import { DataTypes, Sequelize as SequelizeClient } from 'sequelize';

/**
 * @description description of each field in the table
 * @typedef {Object} field definition
 * @property {boolean} allowNull - false=NOT NULL
 * @property {boolean} autoIncrement - each insert, increase the counter
 * @property {boolean} primaryKey - define is primary key
 * @property {boolean} type - expresion to match SQL type
 * @property {boolean} unique - difne as unique the field
 * @property {boolean} field - rename the field
 */
export default {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    email: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
    },
    password: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at',
        defaultValue: SequelizeClient.DATE,
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'updated_at',
        defaultValue: SequelizeClient.DATE,
    },
};
```

.
✨Creamos nuestro seed

> ```
> seed.js` . Archivo de creación de entorno limpio `npm run seed
> ```

```js
import Sequelize from './../libs/sequelize';
import userSchema from './models/user';
import { complete, fail } from '../utils/Log';

const sequelize = new Sequelize();
const user = sequelize.define('user', userSchema);

Promise.all([user])
    .then((responses) => complete('Creation process', responses))
    .catch(({ message }) => fail('Creation process', message));
```

## Crear, actualizar y eliminar

### CRUD Operations

En mi caso, sigo ocupando `node-postgres` para el desarrollo del CRUD.
ℹ️ Repositorio: [Link](https://github.com/roremdev/thingst/tree/dev)
.
✨Adicionamos un formato para dar estructura a nuestro request. Con ello, podremos acomodar la instancia para su uso.

```js
    /**
     * @private
     * @description format data structure
     * @param {object} body - body request
     * @returns {object} - { fields, values, tuples }
     */
    #formatStructure(body) {
        return {
            fields: Object.keys(body).join(),
            values: Object.values(body),
            tuples: Object.entries(body).map(([key, value]) =>
                format(`${key} = %L`, value)
            ),
        };
    }
```

.
♻️ Modificamos nuestras funcionalidades para dar servicio a las demás operaciones de CRUD

```js
    /**
     * @description find all registers in table
     * @returns {array} - response query postgresDB as array
     */
    async findAll() {
        return await this.client.query(`SELECT * FROM ${this.table}`);
    }
    /**
     * @description create a register in table
     * @param {object} body - body request
     * @returns {array} - response query postgresDB as array
     */
    async create(body) {
        const { fields, values } = this.#formatStructure(body);
        const request = format(
            `INSERT INTO ${this.table}(${fields}) VALUES(%L) RETURNING *`,
            values
        );
        return await this.client.query(request);
    }
    /**
     * @description updates parcials of a register in table
     * @param {object} param - param request
     * @param {object} body - body request
     * @returns {array} - response query postgresDB as array
     */
    async update({ id }, body) {
        const { tuples } = this.#formatStructure(body);
        const request = format(
            `UPDATE ${this.table} SET ${tuples} WHERE id = ${id} RETURNING *`
        );
        return await this.client.query(request);
    }
    /**
     * @description delete a register in table
     * @param {object} param - param request
     * @returns {array} - response query postgresDB as array
     */
    async delete({ id }) {
        const request = format(
            `DELETE FROM ${this.table} WHERE id = ${id} RETURNING *`
        );
        return await this.client.query(request);
    }
```

## Cambiando la base de datos a MySQL🐬


✨Definición de variables de entorno.

```sql
# mysql and docker
MYSQLDATABASE=thingst
MYSQLUSER=tester
MYSQLPASSWORD=14db7d47-f442-4a90-b633-2a537c8b5c13
```

♻️ Adición de DB como servicio en Docker:

```dockerfile
services:
    mysql:
        image: mysql
        restart: always
        ports:
            - 3306:3306
        environment:
            MYSQL_DATABASE: ${MYSQLDATABASE}
            MYSQL_ROOT_PASSWORD: ${MYSQLPASSWORD}
            MYSQL_USER: ${MYSQLUSER}
            MYSQL_PASSWORD: ${MYSQLPASSWORD}
        volumes:
            - ./db/mysql:/var/lib/mysql
```

Donde:

- `MYSQL_DATABASE`. Define la base de datos con la que se trabajará.
- `MYSQL_ROOT_PASSWORD`. Se define la contraseña del usuario `root`.
- `MYSQL_USER`. Se define un usuario `root` con ámbito definida por `MYSQL_DATABASE`
- `MYSQL_PASSWORD`. Se define la contraseña para el usuario `MYSQL_USER`.

🔥 Finalmente, levantamos los servicios:
`docker compose --env-file src/config/.env up -d`

- `--env-file` se cargan las variables de entorno definidas.
- `-d` se levantan las instancia en modo silencioso

ℹ️ Para conectarse mediante clientes, el host permitido es mediante default del contenedor `0.0.0.0:3306`

Para los que no les funcionan al momento de hacer

```
docker-compose up -d sql
```

Si les da error , el nuevo servicio quedaria así (O bueno este me funciono)

```dockerfile
services:
	mysql:
    		image: mysql
   		restart: always
    		environment:
     	 		MYSQL_ROOT_PASSWORD: root
      			MYSQL_DATABASE: test_db
    		ports:
      			- "3307:3306"
    		volumes:
      			- ./mysql_data:/var/lib/mysql
```

Donde:
**MYSQL_ROOT_PASSWORD** Este es tu usuario y a la vez tu contraseña
**MYSQL_DATABASE** El nombre de la base de datos

Despues corren el PHPMYADMIN y funciona normal
Este es la estrategia que me funciono a mí. 💻💻💻

Instlar `mysql`

```sh
npm i --save mysql2
```

# 4. Migraciones

## ¿Qué son las migraciones? Migraciones en Sequelize ORM

Podemos definir una abstracción ***OOP\***, abstrayendo la clase `Sequelize` podemos recolectar desde variables de entorno de la siguiente forma:
.

> ✨ `.env` Definición de variables de entorno.

```
# postgres and docker
PGDATABASE=
PGHOST=
PGPORT=
PGUSER=
PGPASSWORD=
```

> ✨ `config.js`. Archivo de configuración de entorno.

```js
 require('dotenv').config({ path: './src/config/.env' });
// code ...
/**
 * @description variables of database server
 */
export const database = {
    dbName: process.env.PGDATABASE,
    dbHost: process.env.PGHOST || 'localhost',
    dbPort: process.env.PGPORT || '5432',
    dbUser: process.env.PGUSER,
    dbPassword: process.env.PGPASSWORD,
};
```

> ✨ `Sequelize.js`. Definición de librería driver.

```js
import { Sequelize as SequelizeClient } from 'sequelize';
import { database } from '../config/config';

const USER = encodeURIComponent(database.dbUser);
const PASSWORD = encodeURIComponent(database.dbPassword);
const URI = `postgres://${USER}:${PASSWORD}@${database.dbHost}:${database.dbPort}/${database.dbName}`;

export default class Sequelize {
 /**
     * @description migration builder object
     */
    static migration() {
        return {
            development: {
                username: database.dbUser,
                password: database.dbPassword,
                database: database.dbName,
                host: database.dbHost,
                dialect: 'postgres',
            },
        };
    }
}
```

> ✨`config.js`. Archivo de configuración de entornos.

```js
import Sequelize from './../libs/sequelize';

module.exports = Sequelize.migration;
```

> ✨`.sequelizerc`. Archivos de configuración `sequelize`.

```js
require('@babel/register');

module.exports = {
    config: './src/mocks/config.js',
    'migrations-path': './src/mocks/migrations',
};
```

## Configurando y corriendo migraciones con npm scripts

✨ Recomiendo que tengan un archivo `migration.template.js`:

```js
// schemas imports

module.exports = {
    up: async (queryInterface) => {
        // code
    },

    down: async (queryInterface) => {
        // code
    },
};
```

comandos de la cli de sequlize (:

Sequelize-cli Commands:

- sequelize-cli db:migrate Run pending migrations
- sequelize-cli db:migrate:schema:timestamps:add Update migration table to have timestamps
- sequelize-cli db:migrate:status List the status of all migrations
- sequelize-cli db:migrate:undo Reverts a migration
- sequelize-cli db:migrate:undo:all Revert all migrations ran
- sequelize-cli db:seed Run specified seeder
- sequelize-cli db:seed:undo Deletes data from the database
- sequelize-cli db:seed:all Run every seeder
- sequelize-cli db:seed:undo:all Deletes data from the database
- sequelize-cli db:create Create database specified by configuration
- sequelize-cli db:drop Drop database specified by configuration
- sequelize-cli init Initializes project
- sequelize-cli init:config Initializes configuration
- sequelize-cli init:migrations Initializes migrations
- sequelize-cli init:models Initializes models
- sequelize-cli init:seeders Initializes seeders
- sequelize-cli migration:generate Generates a new migration file
- [aliases: migration:create]
  sequelize-cli model:generate Generates a model and its migration
- [aliases: model:create]
  sequelize-cli seed:generate Generates a new seed file

## Modificando una entidad

### 💾 DB Generation

ℹ️ Repositorio: [Link](https://github.com/roremdev/thingst/tree/orm)
.

> ✨ `user.js`. Definición de esquema

```js
import { DataTypes, Sequelize as SequelizeClient } from 'sequelize';

/**
 * @description description of each field in the table
 * @typedef {Object} field definition
 * @property {boolean} allowNull - false=NOT NULL
 * @property {boolean} autoIncrement - each insert, increase the counter
 * @property {boolean} primaryKey - define is primary key
 * @property {boolean} type - expresion to match SQL type
 * @property {boolean} unique - difne as unique the field
 * @property {boolean} field - rename the field
 */
export default {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    email: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
    },
    password: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at',
        defaultValue: SequelizeClient.NOW,
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'updated_at',
        defaultValue: SequelizeClient.NOW,
    },
};
```

.

> ✨ `config.js`. Configuración de entornos

```js
import Sequelize from './../libs/sequelize';

module.exports = Sequelize.migration;
```

> ♻️ `.sequelizerc` Configuración del `sequelize`

```js
require('@babel/register');

module.exports = {
    config: './src/mocks/config.js',
    'migrations-path': './src/mocks/migrations',
    'seeders-path': './src/mocks/seeders',
};
```

> ♻️ `package.json`. Adición de scrips de generación y ejecución

```js
        "seeds:generate": "sequelize-cli seed:generate --name",
        "seeds:run": "sequelize-cli db:seed:all",
        "migrations:generate": "sequelize-cli migration:generate --name",
        "migrations:run": "sequelize-cli db:migrate",
        "db:generate": "npm run migrations:run && npm run seeds:run",
```

.

### Generación de scripts para `migration` y `seeds`

> ✨`create-user-table.js`

```js
import userSchema from '../models/user';

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable('users', userSchema);
    },

    down: async (queryInterface) => {
        await queryInterface.drop('users');
    },
};
```

> ✨ `users.js`

.
💡Para la generación de Seeds, puede ocupar un generador de mocks como [Mockaroo](https://mockaroo.com/)

```js
module.exports = {
    up: (queryInterface) => {
        return queryInterface.bulkInsert('users', [
            {
                email: 'ttrime0@zimbio.com',
                password: 'dIkttoW58AZl',
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                email: 'hbrychan1@globo.com',
                password: 'LWGHo4Sf',
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                email: 'kkeaveny2@unesco.org',
                password: '8iKZ14bBA',
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                email: 'ppuve3@goo.ne.jp',
                password: 'GLY4O1',
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                email: 'dwinspeare4@bigcartel.com',
                password: 'dWYttZ4',
                created_at: new Date(),
                updated_at: new Date(),
            },
        ]);
    },
    down: (queryInterface) => {
        return queryInterface.bulkDelete('users', null, {});
    },
};
```

Para generar el archivo add-role.js hay que correr el comando

```sh
npm run migrations:generate add-role
```

# 5. Relaciones

## Relaciones uno a uno

![Screen Shot 2021-10-25 at 13.25.34.png](https://static.platzi.com/media/user_upload/Screen%20Shot%202021-10-25%20at%2013.25.34-0aa425c8-d2c0-4cba-ae01-f214e9604c14.jpg)

## Resolviendo las relaciones uno a uno

Evitar repetir codigo en los schemas se puede importar los schemas del user y agregarlos al validador del customer

const joi = require(‘joi’);

```js
const { createUserSchema, updateUserSchema } = require('./user.schema');

const createCustomerSchema = joi.object({
  firstName: firstName.required(),
  lastName: lastName.required(),
  phone: phone.required(),
  user: createUserSchema
});

const updateCustomerSchema = joi.object({
  firstname: firstName,
  lastname: lastName,
  phone: phone,
  user: updateUserSchema
});
```

de esta forma si en un futuro se agrega un validador al user schema no habia que hacer nada en el customer schema

## Relaciones uno a muchos

Product y Category por si no quieren escribir 😄

category.model.js

```javascript
const { Model, DataTypes, Sequelize } = require('sequelize');

const CATEGORY_TABLE = 'categories';

const CategorySchema = {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER,
	},
	name: {
		type: DataTypes.STRING,
		unique: true,
		allowNull: false,
	},
	image: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	createdAt: {
		allowNull: false,
		type: DataTypes.DATE,
		field: 'created_at',
		defaultValue: Sequelize.NOW,
	},
};

class Category extends Model {
	static associate(models) {
		this.hasMany(models.Product, {
			as: 'products',
			foreignKey: 'categoryId',
		});
	}

	static config(sequelize) {
		return {
			sequelize,
			tableName: CATEGORY_TABLE,
			modelName: 'Category',
			timestamps: false,
		};
	}
}

module.exports = { Category, CategorySchema, CATEGORY_TABLE };
```

product.model.js

```javascript
const { Model, DataTypes, Sequelize } = require('sequelize');

const { CATEGORY_TABLE } = require('./category.model');

const PRODUCT_TABLE = 'products';

const ProductSchema = {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER,
	},
	name: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	image: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	description: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	price: {
		type: DataTypes.INTEGER,
		allowNull: false,
	},
	createdAt: {
		allowNull: false,
		type: DataTypes.DATE,
		field: 'created_at',
		defaultValue: Sequelize.NOW,
	},
	categoryId: {
		field: 'category_id',
		allowNull: false,
		type: DataTypes.INTEGER,
		references: {
			model: CATEGORY_TABLE,
			key: 'id',
		},
		onUpdate: 'CASCADE',
		onDelete: 'SET NULL',
	},
};

class Product extends Model {
	static associate(models) {
		this.belongsTo(models.Category, { as: 'category' });
	}

	static config(sequelize) {
		return {
			sequelize,
			tableName: PRODUCT_TABLE,
			modelName: 'Product',
			timestamps: false,
		};
	}
}

module.exports = { Product, ProductSchema, PRODUCT_TABLE };
```

## Resolviendo relaciones uno a muchos

- Señalamos las relaciones
  - Los schemas de la DB y Joi deben coincidir en la foreign key
  - Podemos agregar la relación en los servicios

```javascript
const { models }= require('./../libs/sequelize');

 async findOne(id) {
    const category = await models.Category.findByPk(id, {
      include: ['products']
    });
    return category;
  }
```

# 6. Consultas

## Órdenes de compra

código de order.model, orders.router, order.schema y order.service:
`order.model`

```js
const ORDER_TABLE = 'orders';

const OrderSchema = {
	id: {
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
		type: DataTypes.INTEGER,
	},
	customerId: {
		field: 'customer_id',
		allowNull: false,
		type: DataTypes.INTEGER,
		References: {
			model: CUSTOMER_TABLE,
			key: 'id',
		},
		onUpdate: 'CASCADE',
		onDelete: 'SET NULL',
	},
	createdAt: {
		allowNull: false,
		type: DataTypes.DATE,
		field: 'created_at',
		defaultValue: Sequelize.NOW,
	},
};

class Order extends Model {
	static associate(models) {
		this.belongsTo(models.Customer, {
			as: 'customer',
		});
	}

	static config(sequelize) {
		return {
			sequelize,
			tableName: ORDER_TABLE,
			modelName: 'Order',
			timestamps: false,
		};
	}
}

module.exports = { Order, OrderSchema, ORDER_TABLE };
```

orders.router

```js
const express = require('express');

const OrderService = require('../services/order.service');
const validatorHandler = require('../middlewares/validator.handler');
const {
	getOrderSchema,
	createOrderSchema,
} = require('../schemas/order.schema');

const router = express.Router();
const service = new OrderService();

router.get(
	'/:id',
	validatorHandler(getOrderSchema, 'params'),
	async (req, res, next) => {
		try {
			const { id } = req.params;
			const order = await service.findOne(id);
			res.json(order);
		} catch (error) {
			next(error);
		}
	}
);

router.post(
	'/',
	validatorHandler(createOrderSchema, 'body'),
	async (req, res, next) => {
		try {
			const body = req.body;
			const newOrder = await service.create(body);
			res.status(201).json({ newOrder });
		} catch (error) {
			next(error);
		}
	}
);

module.exports = router;
```

order.service

```js
const boom = require('@hapi/boom');

const { models } = require('./../libs/sequelize');

class OrderService {
	constructor() {
	}

	async create(data) {
		const newOrder = await models.Order.create(data);
		return newOrder;
	}

	async find() {
		return [];
	}

	async findOne(id) {
		const order = await models.Order.findByPk(id, {
			include: [
				{
					association: 'customer',
					include: ['user'],
				},
			],
		});
		return order;
	}

	async update(id, changes) {
		return {
			id,
			changes,
		};
	}

	async delete(id) {
		return { id };
	}
}

module.exports = OrderService;
```

order.schema

```js
const Joi = require('joi');

const id = Joi.number().integer()
const customerId = Joi.number().integer();


const getOrderSchema = Joi.object({
	id: id.required(),
})

const createOrderSchema = Joi.object({
	customerId: customerId.required(),
});

module.exports = {
	getOrderSchema,
	createOrderSchema
}
```

## Relaciones muchos a muchos

🧠 `order-product.model.js`

```js
const { Model, DataTypes, Sequelize } = require('sequelize');

const { ORDER_TABLE } = require('./order.model');
const { PRODUCT_TABLE } = require('./product.model');

const ORDER_PRODUCT_TABLE = 'orders_products';

const OrderProductSchema =  {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW,
  },
  amount: {
    allowNull: false,
    type: DataTypes.INTEGER
  },
  orderId: {
    field: 'order_id',
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: ORDER_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  productId: {
    field: 'product_id',
    allowNull: false,
    type: DataTypes.INTEGER,
    references: {
      model: PRODUCT_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  }
}

class OrderProduct extends Model {

  static associate(models) {
    //
  }

  static config(sequelize) {
    return {
      sequelize,
      tableName: ORDER_PRODUCT_TABLE,
      modelName: 'OrderProduct',
      timestamps: false
    }
  }
}

module.exports = { OrderProduct, OrderProductSchema, ORDER_PRODUCT_TABLE };
```

## Resolviendo relaciones muchos a muchos

En **orders.router.js**:

```javascript
router.get('/', async (req, res) => {
  try {
    const orders = await service.find()
    res.json(orders)
  } catch (error) {
    res.status(404).json({
      message: error.message
    })
  }
})
```

En **services/order.service.js**:

```javascript
  async find() {
    const orders = await models.Order.findAll({
      include: [
        {
          association: 'customer',
          include: ['user']
        },
        'items'
      ]
    })
    return orders
  }
```

## Paginación

**Code**

- Por defecto el valor de `limit` sea 2
- El `offset` empiece por defecto en 0
- De esta forma si se hace un GET a la ruta se mostrarán solo los dos primeros items
- Además se podría paginar unicamente enviando el query `offset` mientras se mantiene el `limit` por defecto, o viceversa.

```javascript
async find({ limit = 2, offset = 0 }) {
  const options = {
    include: ['category'],
    limit,
    offset,
  };

  const products = await models.Product.findAll(options);

  return products;
}   	
```

## Filtrando precios con operadores

operador que puede funcionar para este momento y es el operador `Op.between`:

```javascript
options.where.price = {
  [Op.between]: [price_min, price_max],
};
```

Fuente: [Variable | Sequelize](https://sequelize.org/master/variable/index.html#static-variable-Op)

------


Además, en la documentación de Joi encontré una manera de validar que el valor que se envíe en `price_max` sea mayor a `price_min` ya que por defecto si se envía estos datos, se retornara un array vacío y probablemente sea mejor manera el error:

```js
const queryProductSchema = Joi.object({
  limit,
  offset,
  price,
  price_min,
  price_max: price_max.greater(Joi.ref('price_min')),
})
  .with('price_min', 'price_max')
  .with('price_max', 'price_min');
```

![img](https://imgur.com/eMz5ynw.png)

------

Y por último, `with()` es una condicional donde el primer parámetro demanda la presencia de los parámetros siguientes para ser válida. Este segundo parámetro puede ser un string o un array.
Fuente: [API Reference | joi.dev](https://joi.dev/api/?v=17.4.2#objectwithkey-peers-options)

Si intentas realizar un query con limit y offset, o solo con price, veras que tendras el error de price_max is required, para poder utilizar las demas queries solo tienes que agregar `required()` en la opción `is` de price_max

```js
price_max: price_max.when('price_min', {
    is: Joi.number().integer().required(),
    then: Joi.required(),
  }),
```

También puedes simplificar un poco la opción `is`

```js
const price_min = Joi.number().integer();

price_max: price_max.when('price_min', {
    is: price_min.required(),
    then: Joi.required(),
  }),
```

# 7. Despliegue

## Deploy en Heroku

### add-role

Con respecto a la migracion de add-role, esta falla porque tenemos el userSchema ya actualizado con el campo role. Este campo ya fue agregado en la primer migracion que fue exitosa.

> Iniciar los servicios ni la pagina me aceptaba conectarse, revisando lo que le faltaba era la dependencia de `sequelize`.
> donde revise el error fue con el comando: `heroku logs --tail`

## Consideraciones al hacer migraciones

`Migraciones`:

```javascript
'use strict';
const { DataTypes, Sequelize } = require('sequelize');
const { USER_TABLE, UserSchema } = require('../models/userModel')
const { CUSTOMER_TABLE, CustomerSchema } = require('../models/customerModel')
const { CATEGORY_TABLE, CategorySchema } = require('../models/categoryModel')
const { PRODUCT_TABLE, ProductSchema } = require('../models/productModel')
const { ORDER_TABLE } = require('../models/orderModel')
const { OrderProductSchema, ORDER_PRODUCT_TABLE } = require('../models/order-productModel')


module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable(USER_TABLE, UserSchema);
    await queryInterface.createTable(CUSTOMER_TABLE, CustomerSchema);
    await queryInterface.createTable(CATEGORY_TABLE, CategorySchema);
    await queryInterface.createTable(PRODUCT_TABLE, ProductSchema);
    await queryInterface.createTable(ORDER_TABLE, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      customerId: {
        field: 'customer_id',
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          model: CUSTOMER_TABLE,
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
          },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at',
        defaultValue: Sequelize.NOW,
        },
    });
    await queryInterface.createTable(ORDER_PRODUCT_TABLE, OrderProductSchema);

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(USER_TABLE)
    await queryInterface.dropTable(CUSTOMER_TABLE)
    await queryInterface.dropTable(CATEGORY_TABLE)
    await queryInterface.dropTable(PRODUCT_TABLE)
    await queryInterface.dropTable(ORDER_TABLE)
    await queryInterface.dropTable(ORDER_PRODUCT_TABLE)
  }
};
```

# 8. Próximos pasos

## Toma con el Curso de Backend con Node.js


Un gran curso estuvo muy bueno algo complejo pero aprendí demasiados